<h1 class="color-verde">CATEGORIAS</h1>
<p class="color-azul">Veja abaixo as categorias cadastradas. Aqui você tem a possibilidade de criar, alterar ou excluir categorias.</p>


<br>
<div class="row">
	<div class="col-xs-12">
		<!-- BOTÃO ADICIONAR -->
		<button class="btn btn-default" type="button" onClick="showHide('bg-add', 'show')">  
			<span class="glyphicon glyphicon-plus color-verde" aria-hidden="true"></span> 
			Adicionar
		</button>

		<!-- INPUT BUSCA -->
		<!-- <div class="form-group margin-campo-busca pull-right visible-md-block visible-lg-block">
		    <input type="text" class="form-control" onKeyup="buscaKeyboard(this, 'td-nome-categoria')" placeholder="Buscar...">
		</div> -->	
		
		<br><br>

		<!-- INPUT BUSCA MOBILE -->
		<!-- <div class="form-group visible-xs visible-sm">
			<br>
		    <input type="text" class="form-control" onKeyup="buscaKeyboard(this, 'td-nome-categoria')" placeholder="Buscar...">
		</div> -->		
	</div>	

</div>


<div id="bg-add" class="row padding-add-cont bg-branco">
	<form id="Categorias" action="javascript:void(0);">
		<div class="col-xs-12 col-md-8">
			<div class="form-group">
				<input type="hidden" name="action" id="action" value="add">
				<input type="hidden" name="id" id="id">
			    <input type="text" name="nome-categoria" id="nome-categoria" class="form-control required" placeholder="Escreva aqui..." class="pull-left">
			</div>	
		</div>	
		<div class="col-xs-12 col-md-2">
			<button class="btn btn-default btn-block bg-verde color-branco btn-ok" type="button" 
			onClick="add('Categorias', 'Categorias')"> 
			 <span class="glyphicon glyphicon-ok color-branco" 
				aria-hidden="true"></span> Salvar</button>	
		</div>
		<div class="col-xs-12 col-md-2">
			<button class="btn btn-default btn-block bg-vermelho color-branco" type="button" 
			onClick="showHide('bg-add', 'hide')">  
				<span class="glyphicon glyphicon-remove color-branco" aria-hidden="true"></span> 
				Cancelar
			</button>
		</div>
	</form>
</div>

<div id="alerts2"></div>




<div class="row">
	<div class="col-xs-12">
		<div class="table-responsive bg-branco">
		  	<table class="table table-hover">
		  		<thead>
		    	<tr id="head">
				  	<th>Título</th>
				  	<th>Ações</th>
				</tr>
				</<thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<br>
		<div id="alert-return-table"></div>
	</div>
</div>


<!-- MODAL REMOVER -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalRemover" aria-labelledby="myModalLabel">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
      			<input type="hidden" name="id-remove" id="id-remove">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h3 class="modal-title">Atenção!</h3>
      		</div>
      		<div class="modal-body">
        		<p>Se você excluir essa categoria não será possível recupera-lo. Tem certeza disso?</p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        		<button type="button" class="btn btn-primary bg-verde" onClick="remover('Categorias', this)">Sim</button>
      		</div>
    	</div>
  	</div>
</div>