<?php require('topo-padrao.php'); ?>

<!-- BACKGRUPS -->
<div class="row show-for-large">
	<br>
	<div class="columns large-12">	
		<h6 class="color-verde-claro titillium_regular font12"><a href="index.php" class="hover-brancons">Página Inicial</a>/ <strong class="color-verde-escuro titillium_regular font12">A Maqpesa</strong> </h6>
    </div>
</div>



<!-- SECTION PRODUTOS -->
<section class="empresa">
	
	<div class="row">
		<br>
		<div class="column">
			<h1 class="color-verde-claro text-center show-for-large titilliumsemibold font52">A MAQPESA</h1>
			<h1 class="color-verde-claro text-center hide-for-large titilliumsemibold font42">A MAQPESA</h1>
			<h4 class="color-laranja2 text-center titilliumsemibold">"Proporcionando o desenvolvimento de novas <br> tecnologias e Máquinas Rodoviárias"</h4>
		</div>
	</div>
	
	<br>
	
	<!-- SOBRE A EMPRESA -->
	<div class="row">
		<div class="column">	
			<p class="font-18 color-cinza-forte">
				A Maqpesa é uma empresa que atua no ramo de fabricação e manutenção de Britadores, Rolo Compactador, Cone, Triturador de Galho, Carretinha para rebocar rolo compactador. <br><Br>
				
				Contando com a experiência de profissionais altamente qualificados, e o investimento constante em estruturação, desenvolvemos competência técnica ampliando nossos horizontes, abrindo novos mercados e tornando a empresa referência em<b> FABRICAÇÃO DE BRITADORES, ROLO COMPACTADOR REBOCÁVEL </b> etc... <br><Br>
				
				Os britadores que fabricamos podem ser usados na reciclagem de construção civil tendo maior aproveitamento dos resíduos e temos modelo de britador que produz pedra para calçamento e cascalhamento. <br><Br>

				Nossos modelos de britador atende a necessidade dos clientes na produção de brita para cascalho de estradas com vários tamanhos, dando qualidade nas estradas com baixo custo de produção, manutenção, tempo e mão de obra.
				<br><br> 
				<b>Destacam se entre nossos clientes as prefeituras, empresas de terraplanagem, pavimentação e mineradoras.


			</p>
		</div>
	</div>
<br><br>
</section>


<section class="valores">
	
	<!-- DESKTOP -->
	<div class="container-full bg-img-missao-desktop show-for-large">
		<br><br>
		<div class="row">
			<div class="column">
				<h3 class="color-verde-claro show-for-large titillium_bdbold text-left"><i>QUALIDADE E GARANTIA</i></h3>
				<hr class="linha-valores"><br><br>
				<p class="color-branco titillium_regular show-for-large">Uma nova marca para sua produtividade crescer</p>

			</div>

		</div>
		
		<div class="row">
			
			<div class="column large-6 ">
	    		<p class="color-branco titillium_regular"><strong class="titillium_bdbold">GARANTIA E QUALIDADE: </strong>Garantimos ao máximo a qualidade de nossos produtos e serviços em todos os equipamentos fabricado e reformados verificado um a um seu funcionamento e resistência.</p>


	    		<p class="color-branco titillium_regular"><strong class="titillium_bdbold">PEÇAS: </strong> Temos peças de reposição com qualidade para nossos equipamentos e outras marcas.</p>
	  		</div>

		    <div class="column large-6">
		    	<p class="color-branco titillium_regular"><strong class="titillium_bdbold">MANUTENÇÃO: </strong> Prestamos serviços de reforma e manutenção em todos os modelos de britadores e rolo compactador.</p>
		  	</div>

		</div>

	</div>







	<!-- MOBILE -->
	<div class="container-full bg-img-missao-mobile hide-for-large">
		<br><br>
		<div class="row">
			<div class="column">
				<h1 class="color-verde-escuro hide-for-large titillium_bdbold text-center"><i>SONHO GRANDE E VALORES</i></h1>
				<p class="color-branco titillium_regular hide-for-large text-center">Uma nova marca para sua produtividade crescer</p>
			</div>

		</div>
		
		<br><br>

		<div class="row">
			
			<div class="column">
	    		<p class="color-branco titillium_regular text-center"><strong class="titillium_bdbold">Missão:</strong> 
	    		<br>
	    		Desenvolver soluções tecnológicas inovadoras que facilitem a vida das pessoas e melhorem a qualidade e a eficiência de empresas de todos os portes.</p>


	    		<hr class="linha-branco">
	  		</div>

	  		<div class="column">
	  			<p class="color-branco titillium_regular text-center"><strong class="titillium_bdbold">Visão:</strong><br>
	    		 Ser reconhecida na região noroeste do RS como empresa inovadora e tecnológica.</p>
	    		 <hr class="linha-branco">
	  		</div>

		    <div class="column">
		    	<p class="color-branco titillium_regular text-center"><strong class="titillium_bdbold">Valores:</strong><br>
		    	 Ser reconhecida na região noroeste do RS como empresa inovadora e tecnológica.</p>
		    </div>

		</div>
	</div>

</section>








<?php require('rodape.php'); ?>