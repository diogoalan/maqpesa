<?php

class Categorias{
	/**
	Atributos da classe 
	**/

	private $id;
	private $nome;

	/**
	Funções get e set da classe
	**/
	function setId($id){
		$this->id = $id;
	}

	function getId(){
		return $this->id;
	}

	function setNome($nome){
		$this->nome = $nome;
	}

	function getNome(){
		return $this->nome;
	}



}
