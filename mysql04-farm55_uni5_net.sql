-- phpMyAdmin SQL Dump
-- version 4.3.7
-- http://www.phpmyadmin.net
--
-- Host: mysql04-farm55.uni5.net
-- Tempo de geração: 15/07/2019 às 17:44
-- Versão do servidor: 5.5.43-log
-- Versão do PHP: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `imakindustrial`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_categorias`
--

CREATE TABLE IF NOT EXISTS `tb_categorias` (
  `categoria_id` int(11) NOT NULL,
  `categoria_nome` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_categorias`
--

INSERT INTO `tb_categorias` (`categoria_id`, `categoria_nome`) VALUES
(2, 'Agricultura'),
(3, 'NR-12 Segurança '),
(4, 'Energia');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logins` (
  `login_id` int(11) NOT NULL,
  `login_email` varchar(255) NOT NULL,
  `login_password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_logins`
--

INSERT INTO `tb_logins` (`login_id`, `login_email`, `login_password`) VALUES
(1, 'imak', '21e473cdbb3f5ac9db265673a23ba529');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_noticias`
--

CREATE TABLE IF NOT EXISTS `tb_noticias` (
  `noticia_id` int(11) NOT NULL,
  `noticia_status` tinyint(11) NOT NULL,
  `noticia_titulo` varchar(255) NOT NULL,
  `noticia_subtitulo` varchar(255) NOT NULL,
  `noticia_descricao` text NOT NULL,
  `noticia_video` varchar(255) NOT NULL,
  `noticia_data` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_noticias`
--

INSERT INTO `tb_noticias` (`noticia_id`, `noticia_status`, `noticia_titulo`, `noticia_subtitulo`, `noticia_descricao`, `noticia_video`, `noticia_data`) VALUES
(33, 1, 'IMAK busca inovações na Alemanha', 'Rogério Richter participou, no mês de novembro, de duas grandes feiras em Hanover, na Alemanha', 'Um ano para buscar experiência, tecnologia e inovação. Este foi um dos objetivos estratégicos da IMAK Industrial para 2016: se preparar para crescer. Neste ano, duas viagens técnicas para a Alemanha asseguraram conhecimento técnico e inovação para 2017.\n\nNo mês de novembro, o gerente comercial da IMAK, Rogério Richter, participou de duas grandes feiras em Hanover, na Alemanha. O objetivo da viagem foi absorver conhecimento e buscar parcerias para o desenvolvimento de novas máquinas e implementos agrícolas, que possam modernizar a agricultura e a pecuária da nossa região.\n\nOs eventos aconteceram no Centro de Eventos e Convenções de Hanover, com mais de 20 pavilhões tomados de expositores de todas as partes do mundo. Na EuroTier, evento focado para inovações tecnológicas em máquinas e implementos para o setor agropecuário, foram feitos contatos com parceiros da Alemanha, Itália e Turquia.\n\nDa Alemanha, destaque para a automação. Máquinas que são responsáveis por processos completos dentro das instalações de uma propriedade rural, como alimentar o gado, por exemplo. Os equipamentos coletam a ração, feno ou silagem, pesa, dosa, distribui o alimento para os animais e faz a limpeza das instalações de forma automatizada. Da Itália e Turquia, destacam-se as máquinas responsáveis pela logística interna das propriedades.\n\n“Estas parcerias nos permitem inovar com maior velocidade, porém, lembramos que o mais importante é o desenvolvimento da nossa região”, destaca Rogério. Ele explica que o termo ‘tropicalização de tecnologias’ valoriza esta premissa, onde a tecnologia importada é embarcada em um produto 60% brasileiro, onde a estrutura dos equipamentos são produzidas de acordo com a realidade geográfica e do clima local. “Os parceiros fornecem os projetos e componentes especiais para as máquinas, produzidas aqui”, revela. É a união da tradição da IMAK, com a pesquisa europeia.\n\nNa Energy Decentral, destaque para a energia renovável: biogás. Nesta área contatos com duas empresas alemãs, que produzem equipamentos que transformam resíduos líquidos de propriedades rurais em energia. E este tema também foi destaque na primeira viagem da IMAK para a Alemanha, no primeiro semestre desse ano, na cidade de Munique. Na IFAT, Rogério fez contatos com empresas que produzem energia a partir de resíduos urbanos. “Sistemas de coleta, recuperação e geração de energia a partir de resíduos urbanos”, explica.\n\nPara aumentar a produtividade no campo é preciso facilitar as condições de trabalho para os produtores e melhorar o conforto para os animais. E este é o objetivo da IMAK para 2017: aumentar a produtividade agropecuária regional, a partir de produtos inovadores para o campo.', '', '2016-12-05'),
(34, 1, 'Confraternização marca final de ano da IMAK', 'Momento de integração também contou com a presença dos colaboradores das empresas Tecnika, TKM e K3', 'Fim de ano é tempo de reflexão e agradecimento. E também de confraternizar. Por isso, a direção da IMAK promoveu um almoço com os colaboradores e seus familiares, a fim de comemorar as conquistas deste ano e celebrar 2017.\n\nOs colaboradores da IMAK se reuniram no sábado (10), na sede da Etnia Alemã de Santa Rosa, para um almoço de final de ano da empresa. Junto da direção e familiares, a equipe recebeu saudação do Diretor, Mogar Sincak, que destacou as ações e o trabalho desempenhado durante todo o ano. O grande momento de integração também contou com a presença dos colaboradores das empresas Tecnika, TKM e K3.\n\nE como o Natal está se aproximando, não poderia faltar a presença do Papai Noel. Após servido o almoço, o Bom Velhinho apareceu de surpresa, para a alegria das crianças. Ele distribuiu doces e presentes para os filhos dos colaboradores. Cada colaborador também recebeu um cooler personalizado.', '', '2016-12-12'),
(35, 1, 'IMAK divulga calendário de participação em Feiras', 'O primeiro evento, Horizontina em Exposição, ocorrerá no mês de março', 'A IMAK Industrial divulgou nesta semana o calendário de participação em feiras para o primeiro semestre de 2017. E, o calendário aponta para um ano intenso. A empresa estará presente em uma série de eventos, em diversos municípios do Rio Grande do Sul.\n\nO objetivo da empresa é estar cada vez mais próximo do produtor rural e avalia que as Feiras são um local de troca de experiências e de divulgar tecnologia e inovação para o meio. Rogério Richter, gerente comercial da IMAK, diz que as Feiras serão também um local de apresentar ao produtor novos produtos. “Vamos apresentar nestas feiras novos produtos, na área de máquinas e implementos agrícolas, e também nossa nova área de atuação: energia, através do biogás”, revela.\n\nE, para dar início às atividades, entre os dias 3 a 5 de março, a empresa participará da feira Horizontina em Exposição, no Parque de Exposições João de Oliveira Borges, em Horizontina, em parceria com o Lojão Agrícola. Nos meses de abril e maio, a IMAK estará presente em quatro grandes feiras: a primeira será no munícipio de Três Passos, a 14ª Feicap, que  ocorre entre os dias 18 e 23 de abril. Em seguida, a Fenamilho, em Santo Ângelo, e a Expofeira, em Três de Maio, que serão realizadas na mesma data, entre os dias 29 de abril e 7 de maio. E, em São Paulo das Missões, a IMAK estará presente na ExpoFesp, nos dias 12, 13 e 14 de maio.\n\nPara o segundo semestre está prevista a participação em feiras nas cidades de Santo Augusto, Catuípe, Ijuí, Bossoroca, São Luiz Gonzaga, Cândido Godói, Santa Rosa, Alecrim, Campina das Missões, Porto Lucena e Boa Vista do Buricá.\n', '', '2017-01-05'),
(36, 1, 'Biogás: energia limpa e renovável', 'Vantagens para o meio ambiente e a saúde, pela baixa emissão de gases poluentes', 'Para o desenvolvimento sustentável de países em crescimento é necessário a busca e incentivo de tecnologias, que utilizem fontes renováveis de energia. A partir da decomposição de matéria orgânica por bactérias, o biogás traz esta característica, como fonte de energia alternativa de grande potencial na atualidade. \n\nComposto por grande parcela de gás metano (CH4), o biogás quando é lançado na atmosfera apresenta a poluição 21 vezes superior ao dióxido de carbono (CO2), no que se refere ao efeito estufa. A sua utilização na geração de energia leva a uma redução do potencial de poluição ambiental. \n\nA cadeia do biogás permite a agregação de valor ao produtor pela geração, principalmente do biogás, que tem a possibilidade de uso e substituição de outras fontes de calor e de energia dentro da sua propriedade rural. Possibilita também benefícios para o produtor, por exemplo, a substituição de fontes de gás que vem do petróleo, utilizando o biogás como fonte de energia dentro da propriedade rural, também para sistemas de aquecimento e de secagem de grãos e de geração de energia elétrica. O próprio uso do biofertilizante, nas atividades agrícolas, quando bem manejado pode reduzir o uso de fertilizantes minerais. \n\nÉ sempre interessante ressaltar a importância do correto manejo dos biodigestores e dos dejetos para evitar os impactos ambientais. Os efluentes têm um grande impacto ambiental, e podem contaminar os rios e lençóis freáticos quando não são corretamente manejados. ', '', '2017-03-16'),
(37, 1, 'Esterco tratado vira fertilizante e evita contaminação de nascentes de água', 'Esterqueira recomendada pela Emater pode evitar contaminação e produzir fertilizante a partir do esterco', 'Esterco de vaca é um assunto que muita gente acha desagradável, mas o pior mesmo é não falar disso porque, ainda hoje, tem fazenda e sítio que despejam todo o esterco do rebanho na natureza, poluindo o solo e a água. No Paraná, uma tecnologia simples está ajudando criadores a lidar com esse problema.\n\nTomazina, no norte do Paraná, é um lugar cercado de montanha que produzem cenários dignos de cartão postal. Muitos cursos d’água que formam a bacia do rio nascem nas área mais altas do município, como o bairro da Barra Mansa.\n\nEssa região do município concentra a maior bacia leiteira de Tomazina. Em uma área de pouco mais de 120 hectares, existem perto de 1300 vacas, que produzem por dia 16 mil litros de leite.  São cerca de 12 litros por vaca. Mas tem algo que elas produzem muito mais: resíduo. Uma vaca de leite produz por dia mais de 45 quilos de esterco e urina. Se o animal fica no pasto, o material vai sendo espalhado conforme ele anda, se degrada aos poucos e acaba virando adubo.\n\nMas quando as vacas estão confinadas, fezes e urina se acumulam no chão da instalação, como explica o agrônomo da Empresa de Assistência Técnica e Extensão Rural (Emater), Alfredo Alemão: “Nesse momento em que elas estão, é o momento em que elas acabam defecando mais. Porque estão se alimentando, já ativa o intestino e elas acabam defecando mais aqui. Assim como também é o momento da ordenha. Ela acaba sendo estimulada a defecar. Então, nesse momento acumula mais”.\n\nPara manter a higiene e evitar doenças, o lugar tem que ser lavado, pelo menos, uma vez por dia. A água misturada com urina e com o próprio esterco cria o chorume. E é aí que começam os problemas. Se o chorume que sai do curral vai direto para o ambiente, pode contaminar o solo e, especialmente, a água.\n\nEsterco é matéria orgânica que, na água, rouba o oxigênio dela para sua decomposição. Roubando oxigênio, ele mata a vida que existe naquela água, ou seja, mata peixes. O chorume que fica depositado diretamente sobre o solo sem nenhuma proteção e não é escorrido, infiltra. Aí chorume infiltrado vai atingir o lençol freático. Se estiver muito próximo a uma lâmina d’água, significa que o lençol freático é raso. Então, isso atinge rapidamente o lençol freático e vai contaminar não só as águas de lá, como qualquer água de nascente ou outra coisa que tiver sendo abastecida por esse lençol.\n\nFonte: Globo Rural.\n\nVeja a reportagem completa, aqui: https://goo.gl/QzGD3w', '', '2017-08-31'),
(38, 1, 'IMAK divulga calendário de Feiras do segundo semestre', 'O primeiro evento do segundo semestre será a Expo São Luiz, em São Luiz Gonzaga', 'A IMAK Industrial divulgou, nesta semana, o calendário de participação em feiras no segundo semestre de 2017. A empresa estará presente em uma série de eventos, em diversos municípios do Rio Grande do Sul. O objetivo é estar cada vez mais próximo do produtor rural. As feiras são um local de troca de experiências e de divulgar a tecnologia e inovação.  \n\nPara dar início às atividades do segundo semestre, entre os dias 27 de setembro e 1º de outubro, a empresa participará da feira Expo São Luiz, em São Luiz Gonzaga. No mês de outubro, a IMAK estará presente em três grandes feiras: a primeira será no munícipio de Cândido Godói, a 10ª Expocandi, que ocorre entre os dias 06 e 08 de outubro. Em seguida, a Hortigranjeiros, em Santa Rosa, de 11 a 15 de outubro. No mesmo período, estará presenta na Expoijuí/Fenadi, na cidade de Ijuí, que ocorre de 10 a 22 de outubro. Para finalizar a presença em feiras neste ano, a IMAK estará no munícipio de Senador Salgado Filho, na Exposafi, de 7 a 10 de dezembro. ', '', '2017-09-14'),
(39, 1, 'IMAK apresenta lançamentos na Fenasoja 2018', 'Empresa completa 5 anos fornecendo equipamentos para produtores que alimentam o Mundo', 'A Fenasoja, uma das mais importantes feiras do Rio Grande do Sul, que acontece de 27 de abril a 6 de maio, busca mostrar o que há de mais moderno e inovador para aumentar a produtividade no agronegócio. Nesta edição, a IMAK Industrial está presente na feira lançando o Distribuidor de Fertilizantes e Sementes.\n\nO Distribuidor possui acionamento mecânico e hidráulico, proporciona um trabalho preciso e eficaz de manejo de sementes e fertilizantes. O modelo simples, com capacidade para 800 litros, foi desenvolvido pensando no pequeno produtor rural. O modelo dupla tem capacidade de 1.100 a 2.000 litros. \n \nA IMAK, que atua há cinco anos em Santa Rosa, tem como pilar estar sempre próxima de seus clientes para compreender quais as reais necessidades do mercado. Além disso, a busca constante nos mercados internos e internacional de tecnologia, que aceleram o desenvolvimento de novos produtos que refletem na eficiência do trabalho e proporcionam um aumento na lucratividade em benefício do produtor rural.', '', '2018-04-30'),
(40, 1, 'Novas alternativas para adquirir equipamentos agrícolas', 'Carreta graneleira da IMAK é um dos produtos que podem ser financiados via BNDES', 'Com foco nas propriedades, a Imak Industrial traz novas alternativas para o homem do campo. Agora é possível adquirir, através dos financiamentos do BNDES Finame ou do Pronaf Mais Alimentos, máquinas e equipamentos agrícolas. Estas linhas de crédito do BNDES são destinadas a agricultores e produtores rurais familiares. \n\nA novidade busca proporcionar ao produtor mais facilidades e possibilidades, para que possa investir em soluções tecnológicas. O distribuidor de fertilizantes, a carreta graneleira e o vagão forrageiro são alguns exemplos dos equipamentos que podem ser financiados. A verificação da lista completa de produtos financiáveis pode ser feita no site do BNDES. \n\nPara solicitar o financiamento é necessário apresentar a Declaração de Aptidão ao Pronaf (DAP) válida e que cumpram os requisitos para enquadramento. Podem ser adquiridos bens que estejam diretamente relacionados com a implantação, ampliação ou modernização da estrutura das atividades de produção, de armazenagem, de transporte ou de serviços agropecuários. Para mais informações sobre estas novas opções de aquisição de produtos, fale com a equipe comercial da IMAK, através do fone 55 9 9948-7343.', '', '2018-05-22'),
(41, 1, 'IMAK completa 5 anos', 'Neste ano, empresa inaugurou nova planta industrial', 'A IMAK completa em 2018, 5 anos de atividades voltadas ao setor agropecuário. A indústria de máquinas e implementos agrícolas, com sede em Santa Rosa, na região Noroeste do Rio Grande do Sul, comercializa produtos em quatro estados do país, proporcionando o desenvolvimento de novas tecnologias e sistemas agrícolas.     \n\nA empresa surgiu em 2013, com o compromisso de desenvolver e aplicar tecnologia para a produção agropecuária. A proximidade da IMAK com seus clientes proporcionou o conhecimento sobre as reais necessidades do mercado, e fez com que a empresa pudesse crescer. A produção iniciou em um pavilhão localizado em um bairro de Santa Rosa. A produção era voltada a pequenos equipamentos, como perfurador de solo, guincho traseiro, roçadeiras, homogeneizador de dejetos, raspo transportador e mini trator. \n\nNo começo de 2018, a empresa inaugurou uma nova planta industrial. O novo espaço, amplo e com uma linha de produção organizada, permitiu a ampliação do portfólio de produtos, inclusive, com a produção de equipamentos de maior porte. Novos produtos foram lançados no mercado, como o vagão forrageiro, carreta forrageira, carreta graneleira, guincho bag e distribuidor de sementes e fertilizantes.\n\nA busca constante nos mercados internos e internacional de tecnologia e a participação em feiras agropecuárias, aceleram o desenvolvimento de novos produtos e garante uma linha variada de implementos agrícolas com conceitos de qualidade de alto nível.  O gerente da IMAK, Rogério Richter, diz que a empresa busca desenvolver soluções tecnológicas inovadoras que facilitem o trabalho das pessoas, melhorando a qualidade, a eficiência e a produtividade dos seus clientes, sempre com segurança operacional. \n\n“Buscamos disponibilizar ao cliente aquilo que ele realmente necessita, um produto robusto, que atenda suas expectativas e possa usar nas diversas situações que precise no dia a dia no campo”, destaca.\n\nA IMAK, com planta localizada na avenida Buricá, no bairro Cruzeiro, une experiência e tecnologia para produzir equipamentos de qualidade diferenciada, com a eficiência e custo-benefício que busca o produtor rural.', 'https://www.youtube.com/embed/8B8E60HEIa4', '2018-06-05'),
(42, 1, 'Marca registrada reafirma credibilidade e inovação', 'A marca registrada foi conquistada neste mês, e consiste em um tipo de propriedade intelectual', 'No ano em que comemora cinco anos de atuação, a IMAK anuncia o registro de sua marca, junto ao Instituto Nacional da Propriedade Industrial (INPI). A marca registrada foi conquistada neste mês, e consiste em um tipo de propriedade intelectual. O registo tem como função permitir que o consumidor possa identificar a origem de um produto ou serviço, possibilitando-lhe distinguir este produto ou serviço de outros similares existentes no mercado.\n\nA marca IMAK Industrial apresenta um conceito moderno de negócio, ligado diretamente a tecnologia e inovação. O objetivo de trazer tecnologia para as propriedades rurais, cada vez mais próximo e em um processo interativo com o produtor, tem como compromisso ofertar soluções para o campo.\n\nA indústria de máquinas e implementos agrícolas, com sede em Santa Rosa, na região Noroeste do Rio Grande do Sul, comercializa produtos em quatro estados do país, proporcionando o desenvolvimento de novas tecnologias e sistemas agrícolas. A empresa surgiu em 2013, com o compromisso de desenvolver e aplicar tecnologia para a produção agropecuária.', '', '2018-08-23'),
(43, 1, 'Imak qualifica processo de logística', 'A mais recente aquisição da empresa foi um caminhão com sistema de carregamento de última geração', 'No ano em que completa cinco anos de atividades voltadas ao setor agropecuário, a IMAK Industrial investe em nova estrutura para a logística. Esse é um dos serviços mais importantes da empresa, pois garante a integridade durante o transporte dos produtos até a entrega ao cliente.\n\nA mais recente aquisição da empresa foi um caminhão com sistema de carregamento de última geração. O investimento tem como objetivo qualificar o processo de logística, aumentando a agilidade e a segurança na etapa da entrega. O caminhão possui capacidade para transportar todos os equipamentos fabricados pela empresa, inclusive os de grande porte, como a carreta graneleira. \n\nO gerente da IMAK, Rogério Richter, evidencia que a empresa procura sempre desenvolver soluções que garantam eficiência e inovação.  “Estamos atendendo fortemente o Rio Grande do Sul, Santa Catarina e Paraná, por isso buscamos alternativas que possam aperfeiçoar o setor de logística”, destaca.', '', '2018-09-27'),
(44, 1, 'Chuvas aliviam produtores de soja e de milho ', 'As precipitações dos últimos períodos também amenizaram o déficit hídrico nas lavouras de soja, em especial no Norte do RS', 'Após apresentar sintomas de estresse hídrico em algumas lavouras, o milho foi beneficiado pelas precipitações que ocorrem em diversas regiões do Rio Grande do Sul. De acordo com um informativo divulgado pela Emater/RS-Ascar, as chuvas ocorridas desenvolveram a normalidade à cultura e trouxeram alívio ao quadro preocupante. \nAs precipitações dos últimos períodos também amenizaram o déficit hídrico nas lavouras de soja, em especial no Norte do RS. Já em municípios das regiões da Fronteira Oeste e Campanha, a chuva prejudicou os trabalhos de finalização do plantio de algumas áreas e o desenvolvimento das áreas já plantadas.\nEm alguns municípios das regiões da Fronteira Noroeste e Missões, foram identificados diversos focos de ferrugem asiática, sendo antecipado o controle com fungicidas, o que vai acarretar um aumento de aplicações nesta safra que se inicia.\nAs lavouras de milho implantadas no Estado alcançam 95% da área total, estimada em aproximadamente 740 mil hectares. Atualmente, 27% das áreas estão em desenvolvimento vegetativo, 18% em floração, 45% em enchimento de grãos, 9% das lavouras estão maduras e por colher e 1% da área foi colhida.', '', '2019-01-10'),
(45, 1, 'Safra de milho deve crescer em 2019', 'Destaque na segunda safra, o milho deve crescer na produção agrícola.', 'Destaque na segunda safra, o milho deve crescer na produção agrícola, neste novo ano, conforme o Instituto Brasileiro de Geografia e Estatística (IBGE). A safra de soja, pdeverá chegar a 117,7 milhões de toneladas, queda de 0,2% perante a 2018. A safra de milho deverá somar 86,9 milhões de tonaleadas, sendo 25,7 milhões de toneladas na primeira safra. Um crescimento de 9,9% em relação a 2018.\nNo geral, a safra agrícola de 2019 deverá totalizar 231,1 milhões de toneladas, uma alta de 1,7% em relação à estimativa de 2018. Ou seja, ainda é possível que a produção seja recorde, superando a supersafra de 2017. Para este resultado ser positivo, o clima é deve ser favorável, pois é um dos fatores mais importantes.', '', '2019-01-23'),
(46, 1, 'Equipe da IMAK divulga calendário de participação em Feiras', 'IMAK estará presente em uma série de eventos, em diversos municípios do Rio Grande do Sul', 'A IMAK Industrial divulgou nesta semana o calendário de participação em feiras para o primeiro semestre de 2019. O calendário aponta para um ano intenso. A empresa estará presente em uma série de eventos, em diversos municípios do Rio Grande do Sul. O objetivo da empresa é estar cada vez mais próximo do produtor rural. A Feira é um local de troca de experiências e de divulgar tecnologia e inovação para o meio.\n\nPara dar início as atividades, neste mês de março, ela participará da Expodireto, em Não Me Toque, entre os dias 11 e 15. No mês de abril, a IMAK estará presente em duas feiras: primeiro, na Expoagro, em Santo Cristo, de 11 a 14. Em seguida, na Fenamilho, em Santo Ângelo, de 27 de abril a 05 de maio. A IMAK também estará presente na Feicap, de 30 de abril a 05 de maio, em Três Passos.', '', '2019-03-05'),
(47, 1, 'Colheita de soja alcança 75% da área do Brasil ', 'Os trabalhos estão focados no RS, SC e Matopiba ', 'A colheita da safra 2018/2019 de soja chegou na última semana, a 75% da área no Brasil, de acordo com a AgRural. O número supera os 71% de um ano. Na média dos últimos cinco anos, os trabalhos de campo atingiam 70%. A partir de agora, os trabalhos estão focados e centrados no Rio Grande do Sul, Santa Catarina, Maranhão, Tocantins, Piauí e Bahia.\nO tempo seco, da semana passada, permitiu que as colheitadeiras avançassem mais rapidamente no Sul do país. O Estado do Paraná colhei 81% das áreas e Santa Catarina 49%. O Rio Grande do Sul superou as expectativas, cujo porcentual colhido saltou de 7% para 25%.\n', '', '2019-04-02'),
(48, 1, 'Dicas para a Safra de Trigo', 'É chegada a hora de o produtor tomar decisões em relação à cultura', 'Com o período da semeadura do trigo, é a chegada a hora de o produtor tomar decisões em relação à cultura. Confira algumas dicas essenciais: \n-Faça adubação de acordo com análise do sol e a produtividade desejada. \n-Execute a semeadura com boa condição de solo, evitando solo com umidade excessiva. Isso garante a boa distribuição de semente. \n-Ao colher o trigo, se clima estiver chuvoso e nublado, é recomendando a colheita com maior umidade do grão, objetivando evitar a perda de qualidade.', '', '2019-06-12'),
(49, 1, 'Trigo: pesquisa deve caracterizar formas de manejo para reduzir custos na produção', 'A produção de trigo, no Rio Grande do Sul, oscila de 1,7 a 2 milhões de toneladas', 'A produção de trigo, no Rio Grande do Sul, oscila de 1,7 a 2 milhões de toneladas, enquanto o consumo se mantem em um milhão de toneladas. Além da distância dos centros consumidores, a instabilidade climática nem sempre resulta em qualidade satisfatória para o cenário brasileiro. Para viabilizar a liquidez do trigo gaúcho, a Embrapa Trigo e a Federação das Cooperativas Agropecuárias do Estado do Rio Grande do Sul (Fecoagro/RS) estão trabalhando no desenvolvimento e validação de um sistema de produção de trigo destinado à exportação, visando menor risco e maior retorno econômico possível ao produtor. A base do projeto é a redução de custos no trigo para tornar o produto mais competitivo no mercado externo.\nDe acordo com o pesquisador da Embrapa Trigo, João Leonardo Pires, o sucesso com a cultura do trigo depende de planejamento. “Como a lavoura de trigo tem um nível de incerteza maior do que as culturas de verão, o planejamento é fundamental para aliar rendimento à rentabilidade”.\nSegundo ele, o trigo exige um mínimo de tecnologia para ser produzido, mas práticas promotoras e protetoras da lavoura precisam ser avaliadas de acordo com o retorno econômico que proporcionam. Produtividade nem sempre se traduz em lucro no trigo, enquanto que rendimentos acima da média geralmente são resultado do alto investimento em insumos. \nEntre os problemas apontados pelo projeto, está a calendarização de aplicações nas lavouras, o que tem tornado, muitas vezes, o controle de pragas e doenças uma receita única, contrária às boas práticas agrícolas e onerando o custo da lavoura. Somente os fungicidas representam de 8 a 12% do custo total no trigo. Da mesma forma é o uso de fertilizantes, que representam outros 33% do custo da lavoura, e nem sempre conta com critério agronômico para definir o investimento. Em alguns locais avaliados pela Embrapa Trigo, a densidade de semeadura variou de 200 a 500 sementes/m², grande diferença nos custos de produção que ao final resultou no mesmo rendimento.\n\nInformações da Embrapa.', '', '2019-06-30');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos` (
  `produto_id` int(11) NOT NULL,
  `produto_status` tinyint(11) NOT NULL,
  `produto_nome` varchar(255) NOT NULL,
  `produto_descricao` text NOT NULL,
  `produto_video` varchar(255) NOT NULL,
  `produto_opcional` varchar(255) NOT NULL,
  `produto_especificacoes` varchar(80) NOT NULL,
  `categoria_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`produto_id`, `produto_status`, `produto_nome`, `produto_descricao`, `produto_video`, `produto_opcional`, `produto_especificacoes`, `categoria_id`) VALUES
(104, 1, 'RT - Raspo Transportador', 'Projetado para  trabalho pesado, o Raspo Transportador apresenta qualidade excepcional e versatilidade, que proporciona alta performance  e durabilidade. O RT possui o exclusivo  sistema de desengate do sistema basculante, que possibilita o descarregamento de elevadas cargas com o mínimo de esforço físico, proporcionando economia de mão de obra e otimização de tarefas.\n \nIdeal para a movimentação de materiais como terra, entulho, matéria orgânica, silagem e fertilizante. O acoplamento permite sua aplicação em todos os tipos de tratores equipados com sistema de três pontos. Equipamento simples, robusto e de fácil operação.', '', '', '6c03b736fff4acc0eb3f5c5b009977c2.png', 2),
(106, 1, 'GT - Guincho Traseiro', 'A movimentação, carregamento e descarregamento de máquinas, equipamentos e produtos, que em outros tempos era um trabalho árduo e cansativo, hoje pode ser fácil, rápido e econômico, com a utilização do Guincho Traseiro. Construído em perfil tubular de aço carbono, a robustez aliada ao material de alta qualidade garante ao equipamento alto desempenho e segurança na movimentação de cargas. O acoplamento permite sua aplicação em todos os tipos de tratores equipados com sistema de três pontos. Equipamento simples, robusto e de fácil operação.', '', '', 'ed9f034631a13d654bbe921cab24e9e5.png', 2),
(107, 1, 'HE 7000 - Homogeneizador de Esterco', 'Projetado para homogeneizar materiais líquidos e pastosos, em camadas superficiais e profundas dos reservatórios de resíduos orgânicos. Facilita o carregamento dos dejetos para a preparação de fertilizante orgânico. A operação de homogeneizar possibilita a diluição de micro e macro nutrientes existentes na compostagem de dejetos orgânicos. Para uma total homogeneização dos materiais, é importante observar o tempo de operação, variando conforme dimensões do reservatório e estado do material a ser homogeneizado. O equipamento reduz esforços físicos, economiza mão de obra e otimiza tarefas, pois acoplado ao trator de forma simples e rápida, através do terceiro ponto e dos braços do levante hidráulico.', 'https://www.youtube.com/embed/BYJwfzSrRX0', '', '4b5f96398d3e2da494bdc7e8016f2276.png', 2),
(108, 1, 'Empilhadeira Traseira', 'Produtividade significa mover mais carga em menos tempo, consequentemente com menor custo. A Empilhadeira Traseira para tratores é aplicada no manuseio das mais variadas cargas, tornando-a extremamente versátil. Este equipamento é constituído de torre com um estágio, garfos móveis com regulagem manual, adaptado ao terceiro ponto do trator de forma simples, rápida e fácil. Uma empilhadeira versátil concebida para utilização em qualquer ambiente, ideal para uma ampla variedade de aplicações, tais como: depósitos, armazéns, indústrias metalúrgicas, transportadoras, remoções industriais, madeireiras e empresas do segmento da construção civil.', 'https://www.youtube.com/embed/ROU3Q5kpVew', '', '08eedf4a231ccb8849eab400a604bed9.png', 2),
(109, 1, 'Garra de Feno', 'Produtividade significa mover mais produtos em menos tempo, consequentemente com menor custo. A Garra de Feno traseira para tratores é aplicada no manuseio de fardos de feno e bolas de pré-secado. Este equipamento é constituído de torre com um estágio, adaptado ao terceiro ponto do trator de forma simples, rápida e fácil. Foi projetada para trabalho pesado, movimentando maior quantidade de feno por hora, proporcionando alta produtividade. De fácil operação e excelente conforto para o operador, permite máximo aproveitamento da jornada de trabalho. Apresenta qualidade excepcional aliada a versatilidade, proporcionando alto desempenho e durabilidade.', '', '', 'edeb8b6f99c2ac751e4fa6ee0510e919.png', 2),
(110, 1, 'GV 500 - Mini Guincho', 'Produtividade e lucratividade significa movimentar mais produtos em menos tempo e com menos esforço, consequentemente com menor custo. O Mini Guincho é aplicado no manuseio das mais diversas cargas, tornando-o extremamente versátil. Este equipamento é constituído de sistema de giro e alongador de haste, hidráulicos e sistema de patola manual. De fácil manuseio e torna o trabalho pesado mais ágil e seguro. É um equipamento versátil concebido para utilização em qualquer ambiente, ideal para uma ampla variedade de aplicações, tais como: indústria de recapagem de pneus,  depósitos, armazéns, indústrias metalúrgicas, transportadoras, remoções industriais, madeireiras e empresas do segmento da construção civil.', '', '', 'b9a783bc202712d987b197727b376a9e.png', 2),
(111, 1, 'GF 1500 - Garra de Feno', 'Produtividade significa mover mais produtos  em menos tempo, consequentemente, com menor custo. A Garra de Feno traseira para tratores é aplicada no manuseio de fardos de feno e bolas de pré-secado. Este equipamento é constituído de torre com um estágio, adaptado ao terceiro ponto do trator de forma  simples, rápida e fácil.\n\nA Garra para Feno (GF 1500) foi projetada para trabalho pesado, movimentando maior quantidade de feno por hora, proporcionando alta produtividade. De fácil operação e excelente conforto para o operador, permite máximo aproveitamento da jornada de trabalho.\nA Garra de Feno apresenta qualidade excepcional aliada a versatilidade proporcionando alto desempenho e durabilidade. É um equipamento confiável e robusto.\n', '', '', 'b08dc5041e114e163e323175ab0aa509.png', 2),
(116, 1, 'RTH - Raspo Transportador Hidráulico', 'Projetado para  trabalho pesado, movendo maior volume de material por hora e proporcionando a máxima produtividade. O Raspo Transportador Hidráulico, apresenta qualidade excepcional e versatilidade, que proporciona alta performance e durabilidade . O RT H é equipado com um cilindro hidráulico de elevado desempenho e robustez, que permite ótima capacidade de carga, baixo índice de manutenção, fácil operação e grande conforto. Ideal para a movimentação de materiais  como: terra, entulho, matéria orgânica, silagem, fertilizante.\n\nTambém pode ser utilizado como lâmina em trabalhos que exigem a remoção de materiais e nivelamento de terrenos. Com um excelente conforto para o operador e fácil operação, o Raspo Transportador Hidráulico permite o máximo aproveitamento da jornada de trabalho.', 'https://www.youtube.com/embed/zs7RsnWqy2E', '', '0101dfadfce0ff3d0f2d48023302aad4.png', 2),
(117, 1, 'RD 1500 - Mini Trator', 'A escassez de mão de obra para as propriedades rurais e a necessidade clara de aumento da produtividade, levou a IMAK Industrial a projetar e fabricar o Mini Trator para as operações críticas da propriedade, como a limpeza dos estábulos, oferecendo o melhor em confiabilidade e desempenho. \nEquipamento rápido e versátil, com design moderno e arrojado, que agregam avançados conceitos da engenharia, como o sistema de transmissão e tração totalmente hidráulico. Recomendado para limpeza de estábulos de gado leiteiro ou de corte, confinados, também utilizado para reorganização da silagem na área de trato do gado. ', 'https://www.youtube.com/embed/t07kNC1h3Xk', '', 'e4fc6aea6bac29f09c499742522ddcef.png', 2),
(119, 1, 'RC - Roçadeira IMAK', 'Projetada para o trabalho pesado, a roçadeira da IMAK proporciona máxima produtividade aliada a alta qualidade. É a garantia de performance e durabilidade no campo. ', '', '', '68d0af54c3171bc5bbc9da4ff653c459.jpg', 2),
(122, 1, 'DF - Distribuidor de Fertilizantes e Sementes', 'O Distribuidor de Fertilizantes e Sementes da IMAK tem versões com acionamento mecânico e hidráulico. Ele oferece ao produtor um trabalho preciso e eficaz de manejo de sementes e fertilizantes. ', '', '', '3080b6cccaaaecfb36dfa1e78bf19191.jpg', 2),
(123, 1, 'Guincho Bag', 'Os guinchos da IMAK possuem modelos com capacidade de carga de 1,5 mil a 2 mil quilos. Garanta a praticidade e inovação da sua propriedade.', '', '', '996906b3c697be392367913f8f1db904.jpg', 2),
(124, 1, 'PF - Perfurador de Solo', 'Com diferentes modelos de brocas, o perfurador de solo da IMAK atinge profundidades de 900mm. É a força e resistência que o campo precisa!', '', '', '99de875ec345305da3b07cbb6190296b.png', 2),
(125, 1, 'CG - Carreta Graneleira', 'A Carreta Graneleira da IMAK tem uma estrutura robusta e confiável, garantindo baixo custo de manutenção.', 'https://www.youtube.com/embed/rBgsK5FA19c', '', 'd9d80b84dcec1dc8fe951226187454e0.jpg', 2),
(126, 1, 'CF - Carreta Forrageira', 'Disponível em versões com capacidades que variam de 6 a 8 mil quilos, a Carreta Forrageira da IMAK oferece ao produtor rural alta performance, com uma estrutura robusta e confiável. ', '', '', '', 2),
(127, 1, 'VF - Vagão Forrageiro', 'Disponível em versões com capacidades que variam de 6 a 10 mil quilos, o Vagão Forrageiro da IMAK oferece ao produtor rural alta performance, com uma estrutura robusta e confiável.\n\n', '', '', '', 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_uploads`
--

CREATE TABLE IF NOT EXISTS `tb_uploads` (
  `upload_id` int(11) NOT NULL,
  `upload_img` varchar(100) NOT NULL,
  `upload_capa` varchar(45) NOT NULL,
  `produto_id` int(11) DEFAULT NULL,
  `noticia_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=810 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_uploads`
--

INSERT INTO `tb_uploads` (`upload_id`, `upload_img`, `upload_capa`, `produto_id`, `noticia_id`) VALUES
(648, '9d221827c6149c9401166eb7a713040e.png', '0', 109, NULL),
(651, '0c938c0c3baad77bf3ffa83f285ff215.jpg', '1', NULL, 33),
(655, 'ccd1e566b233685418c57c1faa6d51e7.jpg', '1', NULL, 34),
(659, 'c43fc1435d8e6613bf1550ad5168a495.jpg', '0', 108, NULL),
(679, '73332fd1f1e817c53f30045cbe497e87.jpg', '1', NULL, 35),
(680, 'ece8f9378cecd904a1a41bbeb7e3c568.jpg', '0', 110, NULL),
(681, '7d42d35186afb9c26ca770ffcf5d4119.jpg', '0', 110, NULL),
(682, 'd8f550fb2138b0b2d26033887bf5babd.png', '0', 110, NULL),
(684, 'e608d3218dbc9f203e619d71109772ef.jpg', '0', 111, NULL),
(700, 'd5a64b7d8b2824c1d7eaccd6a02825a3.jpg', '1', NULL, 36),
(723, 'd176e32ac1e91cf4d28371919db06f79.jpg', '1', NULL, 37),
(724, '0037ac2c42c0b2fb5037e0ba618bda63.jpg', '1', NULL, 38),
(736, '7a20dc8514a02385795cc9899ae237e2.jpg', '1', NULL, 39),
(740, '049a73426b003b5aa8c9ddf46857e63c.jpg', '1', NULL, 40),
(743, '10231206defa64f0c444158948c42394.png', '1', 116, NULL),
(744, '698559dbca7f3a5cc9fd703ac8cb200d.png', '1', 119, NULL),
(745, '0f4b35143109fd4d4d3ff4d10f6eeb94.png', '1', 104, NULL),
(746, 'dcb799bc61a4f5c42c58e467ee9b3b8b.png', '1', 106, NULL),
(773, '2b472ac6c940d266acafbeb86dc6c79c.png', '1', NULL, 41),
(774, 'c6d87e44bccacc35d5f5d56a0047ca95.jpg', '1', 124, NULL),
(775, 'a4144a3a00a198b1d103651dc270b162.jpg', '1', 123, NULL),
(776, 'a8ef3c7096a052d56f824d6f8929f464.jpg', '1', NULL, 42),
(777, '380b3d4a0e6b6a2585f4fbbe11f63a7d.jpg', '1', NULL, 43),
(778, '5060a76271c60111b7796680cbbd01b8.jpg', '1', NULL, 44),
(779, '69fda072038587b2b47fd585b1a0689d.png', '1', NULL, 45),
(780, '64f13a6f80ddff8ba45058c3dbe17476.png', '1', 107, NULL),
(782, '31c02dc53133802288a76049181848cd.jpg', '1', NULL, 46),
(783, '3ede769858504dc7cd25fdcf6dfe51a2.png', '1', 122, NULL),
(784, '9cb31bb3f50f12a40c54981b40af2a4b.jpg', '0', 122, NULL),
(785, '400058f6944997440cd2e936de4f8029.jpg', '0', 122, NULL),
(788, 'eabaec09f2f0159a53360412853b2d63.jpg', '1', 126, NULL),
(789, 'b5bd184a8b8210d4e2c9851ebef5b033.jpg', '0', 126, NULL),
(790, '5e122e46144fa68273babb26ddd33a9a.jpg', '1', 127, NULL),
(791, '8db9a219d4f4d4cd3e2bea7ca876c9d7.png', '0', 127, NULL),
(792, '20e0d652ec7e76e173f8684b6801f7e6.png', '1', NULL, 47),
(804, 'e4bc4d26e0a50b6c5fb97993709b129c.jpg', '1', 117, NULL),
(805, '1fa249c87d1de2a1c6a4a31a9e560977.jpg', '0', 117, NULL),
(806, '2495461f3e553ebdf2ed1b90de03d8cb.jpg', '1', 125, NULL),
(807, '5294a3901f30e05d78b3ea99a57286ee.jpg', '1', NULL, 48),
(808, '64b35e3dc92ce1a9c6fbdc8d24eac10a.jpg', '1', NULL, 49);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_videos`
--

CREATE TABLE IF NOT EXISTS `tb_videos` (
  `video_id` int(11) NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `produto_id` int(11) DEFAULT NULL,
  `noticia_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `tb_categorias`
--
ALTER TABLE `tb_categorias`
  ADD PRIMARY KEY (`categoria_id`);

--
-- Índices de tabela `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`login_id`);

--
-- Índices de tabela `tb_noticias`
--
ALTER TABLE `tb_noticias`
  ADD PRIMARY KEY (`noticia_id`);

--
-- Índices de tabela `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`produto_id`), ADD KEY `fk_tb_produtos_tb_categorias1_idx` (`categoria_id`);

--
-- Índices de tabela `tb_uploads`
--
ALTER TABLE `tb_uploads`
  ADD PRIMARY KEY (`upload_id`), ADD KEY `fk_tb_uploads_tb_noticias_idx` (`noticia_id`), ADD KEY `fk_tb_uploads_tb_produtos1_idx` (`produto_id`);

--
-- Índices de tabela `tb_videos`
--
ALTER TABLE `tb_videos`
  ADD PRIMARY KEY (`video_id`), ADD KEY `fk_tb_videos_tb_produtos1_idx` (`produto_id`), ADD KEY `fk_tb_videos_tb_noticias1_idx` (`noticia_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `tb_categorias`
--
ALTER TABLE `tb_categorias`
  MODIFY `categoria_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `login_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `tb_noticias`
--
ALTER TABLE `tb_noticias`
  MODIFY `noticia_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT de tabela `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `produto_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT de tabela `tb_uploads`
--
ALTER TABLE `tb_uploads`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=810;
--
-- AUTO_INCREMENT de tabela `tb_videos`
--
ALTER TABLE `tb_videos`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `tb_produtos`
--
ALTER TABLE `tb_produtos`
ADD CONSTRAINT `fk_tb_produtos_tb_categorias1` FOREIGN KEY (`categoria_id`) REFERENCES `tb_categorias` (`categoria_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `tb_uploads`
--
ALTER TABLE `tb_uploads`
ADD CONSTRAINT `fk_tb_uploads_tb_noticias` FOREIGN KEY (`noticia_id`) REFERENCES `tb_noticias` (`noticia_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_tb_uploads_tb_produtos1` FOREIGN KEY (`produto_id`) REFERENCES `tb_produtos` (`produto_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `tb_videos`
--
ALTER TABLE `tb_videos`
ADD CONSTRAINT `fk_tb_videos_tb_noticias1` FOREIGN KEY (`noticia_id`) REFERENCES `tb_noticias` (`noticia_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_tb_videos_tb_produtos1` FOREIGN KEY (`produto_id`) REFERENCES `tb_produtos` (`produto_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
