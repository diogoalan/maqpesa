function Produtos(){
	
	this.opcionais = [];

}

Produtos.prototype = {

	newOpcional: function(){
		
		console.log(this.opcionais);

		if($('#opcionais').val().charAt(0) == " "){
			arrayObjects['Validacoes'].alerts('alert alert-danger', 'Campo opcionais em branco!');
			return;
		}

		if(this.opcionais.indexOf($('#opcionais').val()) != -1){
			arrayObjects['Validacoes'].alerts('alert alert-danger', 'Já existe este opcional cadastrado!');
			return;
		}

		if($('#opcionais').val().length != 0){

			this.opcionais.push($('#opcionais').val());
			
			$('.visualizar-opcionais').append(
				'<div class="opcionais-bloco">\n\
					<div class="pull-left">\n\
						<span class="glyphicon glyphicon-remove tamanho-x" style="cursor:pointer;" aria-hidden="true" '+ 
						'onClick="arrayObjects[\'Produtos\'].removeOpcionais(this,\''+ $('#opcionais').val() + '\')"></span>\n\
					</div>\n\
					<h5 class="color-azul margin-text-opcional"> '+ $('#opcionais').val() +' </h5>\n\
				</div>');

			$('#opcionais').val('');

		}else{
			arrayObjects['Validacoes'].alerts('alert alert-danger', 'Campo opcionais em branco!');
			return;
		}

	},


	removeOpcionais: function(elemento, opcional){
		
		var indice = this.opcionais.indexOf(opcional);

		this.opcionais.splice(indice,1);
		console.log(this.opcionais);

		$(elemento).closest('.opcionais-bloco').remove();
	},


	add: function(dados){

		$('#action').val('add');

		if($('#idImg').val() == '1'){
			if(capa == null){
				capa = array[0].name;
			}
		}

		$('#alert-return-table').empty();

		var data = {
			'action' 	          : 'add',
			'nome'                : $('#nome').val(),
			'select-categoria'    : $('select[name="select-categoria"] :selected').val(),
			'descricao'           : $('#descricao').val(),
			'img'                 : array,
			'estado'              : $('input[name="estado"]:checked').val(),
			'video'       		  : $('#video-produto').val(),
			'opcionais'			  : this.opcionais.toString(),
			'img-especificacoes'  : (!$('#idImgEspecificacoes').val() ? '' : $('#idImgEspecificacoes').val()),
			'capa'                : capa
		}

		if(arrayObjects['Validacoes'].isYoutubeVideo(data.video) == true){
			data.video = data.video.replace('watch?v=', 'embed/');
			data.video = data.video.replace('v/', 'embed/');	
			data.video = data.video.replace('https://youtu.be/', 'https://www.youtube.com/embed/');
		}else{
			arrayObjects['Validacoes'].alerts('alert alert-danger', 'Deve-se colocar um link de video correto!');
			return;
		}

		if($('select[name="select-categoria"] :selected').val() == 0){
			$('#select-categoria').css('border', '1px solid #d9534f');
			$('body').animate({scrollTop: $('#select-categoria').height()}, 'slow');
			return;
		} else {
			$('#select-categoria').css('border', '1px solid #CCC');
		}

		if($('#idImg').attr('value') != 1){
			arrayObjects['Validacoes'].alerts('alert alert-danger', 'Deve-se selecionar pelo menos uma imagem para a Galeria!');
			return;
		}

		var callback = function(json){

			// console.log(json);
			
			var altura = $('#conteudo').height();
			$('body').animate({scrollTop: altura}, 'slow');

			arrayObjects['Validacoes'].alerts('alert alert-success', 'Produto cadastrado com sucesso!\n\
				<img src="assets/img/loading-verde.svg" style="width:50px">');


			setTimeout(function(){
				modifyScreen('produtos', 'Produtos');
			}, 3000);

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/ProdutoController.php', 'post', null, data, callback);

	},

	update: function(dados){

		if($('#idImg').val() == '1'){
			if(capa == null){
				capa = array[0].name;
			}
		}

		// if($('#idImgEspecificacoes').val() == '1'){
		// 	if(capa == null){
		// 		capa = array[0].name;
		// 	}
		// }

		$('#action').val('update');

		var data = {
			'action' 	          : 'update',
			'id'				  : $('#id').val(),
			'nome'                : $('#nome').val(),
			'select-categoria'    : $('select[name="select-categoria"] :selected').val(),
			'descricao'           : $('#descricao').val(),
			'img'                 : array,
			'estado'              : $('input[name="estado"]:checked').val(),
			'video'		          : $('#video-produto').val(),
			'opcionais'			  : this.opcionais.toString(),
			'img-especificacoes'  : $('#idImgEspecificacoes').val(),
			'capa'                : capa
		}

		if(arrayObjects['Validacoes'].isYoutubeVideo(data.video) == true){
			data.video = data.video.replace('watch?v=', 'embed/');
			data.video = data.video.replace('v/', 'embed/');	
			data.video = data.video.replace('https://youtu.be/', 'https://www.youtube.com/embed/');
		}else{
			arrayObjects['Validacoes'].alerts('alert alert-danger', 'Deve-se colocar um link de video correto!');
			return;
		}


		if($('select[name="select-categoria"] :selected').val() == 0){
			$('#select-categoria').css('border', '1px solid #d9534f');
			$('#select-categoria').animate({scrollTop: 110}, 'slow');
			return;
		} else {
			$('#select-categoria').css('border', '1px solid #CCC');
		}

		if($('#idImg').attr('value') != 1){
			arrayObjects['Validacoes'].alerts('alert alert-danger', 'Deve-se selecionar pelo menos uma imagem para a Galeria!');
			return;
		}

		var callback = function(json){

			console.log(json);
			
			var altura = $('#conteudo').height();
			$('body').animate({scrollTop: altura}, 'slow');

			arrayObjects['Validacoes'].alerts('alert alert-success', 'Produto alterado com sucesso!\n\
				<img src="assets/img/loading-verde.svg" style="width:50px">');

			setTimeout(function(){
				modifyScreen('produtos', 'Produtos');
			}, 3000);
			console.log('teste');
		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/ProdutoController.php', 'post', null, data, callback);
		console.log('teste');
	},

	showUpdate: function(id){

		var categoria = null;

		var dados = {
			'action' : 'returnInfoId',
			'id'     : $(id).parents().parents().attr('id')
		}

		var dados2 = {
			'action' : 'returnImg',
			'id'     : $(id).parents().parents().attr('id')
		}

		var callback = function(json){
			$('#conteudo').html(json);
			$('#id').val($(id).parents().parents().attr('id'));

			// $('.agendamento').hide();
			arrayObjects['Produtos'].listarCategoria();

			var callback2 = function(dados){
				
				console.log(dados);

				$('#loading').show();

				$('#nome').val(dados[0].name);
				$('#descricao').val(dados[0].descricao);
				$('#video-produto').val(dados[0].video);
				// $('#img-especificacoes').val();
				categoria = dados[0].categoria;
				
				
				// Monta a string que retornou do servidor DB; // Transforma a string do servidor para um array  novamente para mostar para o usuário.
				console.log(dados[0].opcionais);
				
				if(dados[0].opcionais != null){
					
					arrayObjects['Produtos'].opcionais = dados[0].opcionais.split(",");

					for(var i in arrayObjects['Produtos'].opcionais){

					$('.visualizar-opcionais').append(
						'<div class="opcionais-bloco">\n\
							<div class="pull-left">\n\
								<span class="glyphicon glyphicon-remove tamanho-x" style="cursor:pointer;" aria-hidden="true" '+ 
								'onClick="arrayObjects[\'Produtos\'].removeOpcionais(this,\''+arrayObjects['Produtos'].opcionais[i]+'\')"></span>\n\
							</div>\n\
							<h5 class="color-azul margin-text-opcional"> '+ arrayObjects['Produtos'].opcionais[i] + ' </h5>\n\
						</div>');

						$('#opcionais').val('');
					}	
				}

				console.log(arrayObjects['Produtos'].opcionais);
	
				console.log(dados);
				if(dados[0]['img-especificacoes'] == null){
					return;
				}else{
					// MOSTRA IMG ESPECIFICACOES
					$('#previewEspecificacoes').html('\n\
						<div id="'+json[0].name+'" class="hover-img">'+	
							'<input type="hidden" id="idImgEspecificacoes" value="'+ dados[0]['img-especificacoes']+'">'+					
							'<img src="../adm/assets/upload/'+dados[0]['img-especificacoes']+'" alt="Imagem">\n\
							<div class="retina">\n\
								<div class="acoes-img direita">\n\
									<span class="glyphicon glyphicon-trash color-vermelho tam-icone" aria-hidden="true"'+
									'onClick="arrayObjects[\'Produtos\'].removeImgEspecificacoes(this)"></span>\n\
								</div>\n\
							</div>\n\
						</div>\n\
					');			
				}
			}

			var callback3 = function(data){
				// console.log(data);
				for(var i in data){

					if(data[i].capa == 1){
						capa = data[i].img;
					}

					var dados = {
						'name' : data[i].img
					}

					array.push(dados);

					if(data[i].capa == '1'){
						var img = '<img src="../adm/assets/upload/'+data[i].img+'" alt="Imagem" class="foto-capa">';
					} else {
						var img = '<img src="../adm/assets/upload/'+data[i].img+'" alt="Imagem">';
					}

					$('#preview').append('\n\
						<div id="'+data[i].img+'" class="hover-img">'+	
							'<input type="hidden" id="idImg" value="1">'+					
							''+img+'\n\
							<div class="retina">\n\
								<div class="acoes-img esquerda">\n\
									<span class="glyphicon glyphicon-bookmark color-azul-c tam-icone" aria-hidden="true"'+ 
									'onClick="arrayObjects[\'Produtos\'].capaImg(this)"></span>\n\
								</div>\n\
								<div class="acoes-img direita">\n\
									<span class="glyphicon glyphicon-trash color-vermelho tam-icone" aria-hidden="true"'+
									'onClick="arrayObjects[\'Produtos\'].removeImg(this)"></span>\n\
								</div>\n\
							</div>\n\
						</div>\n\
					');
				}
				

				$('.btn-ok').attr('onClick', 'update(\'Produtos\', \'form-produto\')');

				$('option').each(function(index, el){
					if($(this).parents().attr('id') == 'select-categoria'){
						if($(this).attr('value') == categoria){
							$(this).attr('selected', true);
						}
					}

				});

				if($('select[id="select-categoria"] :selected').val() == 0){
					setTimeout(function(){
						$('option').each(function(index, el){
							if($(this).parents().attr('id') == 'select-categoria'){
								if($(this).attr('value') == categoria){
									$(this).attr('selected', true);
								}
							}
						});
					}, 1000);
				}
			}
		
			arrayObjects['Requisicoes'].ajax('mvc/controller/ProdutoController.php', 'post', 'json', dados, callback2);
			arrayObjects['Requisicoes'].ajax('mvc/controller/ProdutoController.php', 'post', 'json', dados2, callback3);
		}

		arrayObjects['Requisicoes'].ajax('cadastro-produto.php', 'post', null, null, callback);
	},

	listar: function(){

		var dados = {
			'action' : 'listar'
		}

		var callback = function(json){
			
			if(json.length > 0){

				for(var i in json){

					if(json[i].status == '1'){
						var status = '<input style="margin-top:20px" id="status" type="checkbox"'+
						'onClick="arrayObjects[\'Produtos\'].actionTable(\'status\', this)" checked>';
					} else {
						var status = '<input style="margin-top:20px" id="status" type="checkbox"'+
						'onClick="arrayObjects[\'Produtos\'].actionTable(\'status\', this)">';
					}

					$('tbody').append('\n\
					<tr id="'+json[i].id+'">\n\
					  	<td class="" id="td-nome-produto"><img style="width:60px; padding-right:10px" src="../adm/assets/upload/'+json[i].img+'">\n\
					  	<a class="color-azul" href="#">'+json[i].name+'</a></td>\n\
					  	\n\
					  	<td class="">'+status+'</td>\n\
					  	<td class="">\n\
					  	<a class="btn btn-default" href="#" role="button" style="margin-top:10px" onClick="load(\'Produtos\', \'update\', this)">\n\
					  	<span class="glyphicon glyphicon-edit color-azul-c" aria-hidden="true"></span> Editar</a>\n\
					  	<a class="btn btn-default" href="#" role="button" style="margin-top:10px"'+ 
					  	'onClick="showDialog(\'modalRemover\', \'show\', '+json[i].id+')">\n\
					  	<span class="glyphicon glyphicon-trash color-vermelho" aria-hidden="true"></span> Excluir</a></td>\n\
					</tr>\n\
					');

				}

			} else {
				$('#alert-return-table').html('<p class="alert alert-info" style="text-align: center">Não existe nenhum produto cadastrado! :(</p>');
			}

			capa = null;
			array = [];

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/ProdutoController.php', 'post', 'json', dados, callback);

	},

	actionTable: function(action, value){

		var aux = null;

		if(action == 'status'){

			if($(value).prop('checked') == true){
				
				$(value).parents().parents().find('#destaque').attr('disabled', false);
				aux = 1;

			} else {

				$(value).parents().parents().find('#destaque').attr('disabled', true);
				aux = 0;

			}

		}

		var dados = {
			'action'   : 'updateTable',
			'function' : action,
			'value'    : aux,
			'id'       : $(value).parents().parents().attr('id')
		}

		var callback = function(json){
			// console.log(json);
			arrayObjects['Validacoes'].alerts('alert alert-success', 'Produto atualizado com sucesso! :)');
		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/ProdutoController.php', 'post', null, dados, callback);

	},

	listarCategoria: function(){

		var dados = {
			'action' : 'categorias'
		}

		var callback = function(json){

			if(json.length > 0){
				for(var i in json){
					$('#select-categoria').append('<option value="'+json[i].id+'">'+json[i].name+'</option>');
				}
			}

			$('#action').val('add');

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/ProdutoController.php', 'post', 'json', dados, callback);

	},

	uploadImgEspecificacoes: function(value){
		console.log('testeee1')
		$('#form-img-especificacoes').ajaxForm({

			url: 'mvc/controller/UploadEspecificacoesController.php',
			type: 'post',
			data: $('#img-especificacoes').serialize(),
			dataType: 'json',
			beforeSend: function(){
				$('#previewEspecificacoes').append('<img id="campo-gif" value="1" style="width:80px; height:80px" src="assets/img/loading-verde.svg" />');
			},
			success: function(json){
				console.log(json);

				setTimeout(function(){

					$('#campo-gif').remove();
				
					if(json.length > 0){
					
					
							$('#previewEspecificacoes').html('\n\
								<div id="'+json[0].name+'" class="hover-img">'+	
									'<input type="hidden" id="idImgEspecificacoes" value="'+ json[0].name +'">'+					
									'<img src="../adm/assets/upload/'+json[0].name+'" alt="Imagem">\n\
									<div class="retina">\n\
										<div class="acoes-img direita">\n\
											<span class="glyphicon glyphicon-trash color-vermelho tam-icone" aria-hidden="true"'+
											'onClick="arrayObjects[\'Produtos\'].removeImgEspecificacoes(this)"></span>\n\
										</div>\n\
									</div>\n\
								</div>\n\
							');	
						
					}
					
					console.log('testeeeee');

					$('#loading').hide();

				}, 2000);

			}

		}).submit();	
	},


	uploadImg: function(value){

		$('#form-produto').ajaxForm({

			url: 'mvc/controller/UploadController.php',
			type: 'post',
			data: $('#form-produto').serialize(),
			dataType: 'json',
			beforeSend: function(){
				$('#preview').append('<img id="campo-gif" value="1" style="width:80px; height:80px" src="assets/img/loading-verde.svg" />');
			},
			success: function(json){

				setTimeout(function(){

					$('#campo-gif').remove();
				
					if(json.length > 0){

						for(var i in json){

							var dados = {
								'name' : json[i].name
							}

							array.push(dados);

							$('#preview').append('\n\
								<div id="'+json[i].name+'" class="hover-img">'+	
									'<input type="hidden" id="idImg" value="1">'+					
									'<img src="../adm/assets/upload/'+json[i].name+'" alt="Imagem">\n\
									<div class="retina">\n\
										<div class="acoes-img esquerda">\n\
										<span class="glyphicon glyphicon-bookmark color-azul-c tam-icone" aria-hidden="true"'+ 
										'onClick="arrayObjects[\'Produtos\'].capaImg(this)"></span>\n\
										</div>\n\
										<div class="acoes-img direita">\n\
											<span class="glyphicon glyphicon-trash color-vermelho tam-icone" aria-hidden="true"'+
											'onClick="arrayObjects[\'Produtos\'].removeImg(this)"></span>\n\
										</div>\n\
									</div>\n\
								</div>\n\
							');	
						}

					}

					$('#loading').hide();

				}, 2000);

			}

		}).submit();

		$('#upload').val('');

	},

	capaImg: function(value){

		$('img').removeClass('foto-capa');

		capa = $(value).parent().parent().parent().attr('id');
		$(value).parent().parent().parent().find('img').addClass('foto-capa');

		var altura = $('#conteudo').height();
		$('body').animate({scrollTop: altura}, 'slow');

		arrayObjects['Validacoes'].alerts('alert alert-success', 'Capa definida com sucesso! :)');

	},

	removeImg: function(value){

		var aux = $(value).parent().parent().parent().attr('id');

		for(var i = 0; i < array.length; i++){
			if(array[i].name == aux){

				if(array[i].name == capa)
					capa = null;

				array.splice(i, 1);
				$(value).parent().parent().parent().remove();	
			}
		} 

	},

	removeImgEspecificacoes: function(){
		
		$('#previewEspecificacoes').html('');
		
	},

	remove: function(id){

		var dados = {
			'id' 	 : id,
			'action' : 'remove'
		}
		// console.log(dados);
		var callback = function(json){

			showDialog('modalRemover', 'hide');
			$('#id').val('');

			if(json == 'erro'){
				arrayObjects['Validacoes'].alerts('alert alert-danger', 'Produto vinculado com outros registros. Verifique! :(');
				return;
			}

			$('#' + id).remove();

			arrayObjects['Validacoes'].alerts('alert alert-success', 'Produto removido com sucesso! :)');

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/ProdutoController.php', 'post', null, dados, callback);

	}

}