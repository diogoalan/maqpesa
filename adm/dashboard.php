<div class="row">
	<div class="col-sm-6 col-md-5 col-md-offset-1 padding-informacoes">
		<div class="pull-left icones-dashboard bg-verde">
			<span class="glyphicon glyphicon-shopping-cart color-branco tamanho-icone" aria-hidden="true"></span>
		</div>
		<h1 class="numeros-dashboard color-azul" id="produtos-cont"><strong></strong></h1>
		<p class="descritivo-numeros-dashboard color-cinza">PRODUTOS CADASTRADOS</p>	
	</div>
	<div class="col-sm-6 col-md-5 col-md-offset-1 padding-informacoes">
		<div class="pull-left icones-dashboard bg-amarelo">
			<span class="glyphicon glyphicon-th color-branco tamanho-icone" aria-hidden="true"></span>
		</div>
		<h1 class="numeros-dashboard color-azul" id="categorias-cont"><strong></strong></h1>
		<p class="descritivo-numeros-dashboard color-cinza">CATEGORIAS CADASTRADAS</p> 	
	</div>
</div>

<div class="row">
	<div class="col-sm-6 col-md-5 col-md-offset-1 padding-informacoes">
		<div class="pull-left icones-dashboard bg-azul-c">
			<span class="glyphicon glyphicon-th-list color-branco tamanho-icone" aria-hidden="true"></span>
		</div>
		<h1 class="numeros-dashboard color-azul" id="noticias-cont"><strong></strong></h1>
		<p class="descritivo-numeros-dashboard color-cinza">NOTÍCIAS CADASTRADAS</p>		
	</div>
	
	<!-- <div class="col-sm-6 col-md-5 col-md-offset-1 padding-informacoes">
		<div class="pull-left icones-dashboard bg-vermelho">
			<span class="glyphicon glyphicon-usd color-branco tamanho-icone" aria-hidden="true"></span>
		</div>
		<h1 class="numeros-dashboard color-azul" id="orcamentos-cont"><strong></strong></h1>
		<p class="descritivo-numeros-dashboard color-cinza">ORÇAMENTOS RECEBIDOS</p>	 	
	</div> -->	
</div>
