<?php 


class Logins {

	/**
	Atributos da classe 
	**/
	private $id;
	private $email;
	private $password;


	/**
	Funções get e set da classe
	**/
	function setId($id){
		$this->id = $id;
	}

	function getId(){
		return $this->id;
	}

	function setEmail($email){
		$this->email = $email;
	}

	function getEmail(){
		return $this->email;
	}

	function setPassword($password){
		$this->password = $password;
	}

	function getPassword(){
		return $this->password;
	}

}