<?php 

class Noticias{

	private $id;
	private $status;
	private $titulo;
	private $subtitulo;
	private $descricao;
	private $video;
	// private $img;
	private $data;	


	function setId($id){
		$this->id = $id;
	}

	function getId(){
		return $this->id;
	}

	function setStatus($status){
		$this->status = $status;
	}

	function getStatus(){
		return $this->status;
	}

	function setTitulo($titulo){
		$this->titulo = $titulo;
	}

	function getTitulo(){
		return $this->titulo;
	}

	function setSubtitulo($subtitulo){
		$this->subtitulo = $subtitulo;
	}

	function getSubtitulo(){
		return $this->subtitulo;
	}

	function setDescricao($descricao){
		$this->descricao = $descricao;
	}

	function getDescricao(){
		return $this->descricao;
	}

	function setVideo($video){
		$this->video = $video;
	}

	function getVideo(){
		return $this->video;
	}

	// function setImg($img){
	// 	$this->img = $img;
	// }

	// function getImg(){
	// 	return $this->img;
	// }

	function setData($data){
		$this->data = $data;
	}

	function getData(){
		return $this->data;
	}



}