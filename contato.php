<?php require('topo-padrao.php'); ?>

<div id="conteudo">

<!-- BACKGRUPS -->
<div class="row show-for-large">
	<br>
	<div class="columns large-12">	
		<h6 class="color-verde-claro titillium_regular font12"><a href="index.php" class="hover-brancons">Página Inicial</a>/ <strong class="color-verde-escuro titillium_regular font12">Contato</strong> </h6>
    </div>
</div>


<!-- SECTION CONTATO -->
<section class="contato">
	
	<br><br>
	<div class="row">
		<div class="column">	
			<h1 class="color-verde-claro text-center show-for-large titilliumsemibold font52">FALE CONOSCO</h1>
			<h1 class="color-verde-claro text-center hide-for-large titilliumsemibold font42">FALE CONOSCO</h1>
			<!-- <h4 class="color-laranja text-center titilliumsemibold">Utilize os campos abaixo para entrar em contato. <br> Ficaremos muito felizes em lhe ajudar</h4> -->
		</div>
	</div>




	<!-- TESTE-->
	<br><br><br><br>



	<!-- LOCALIZAÇÂO -->
	<div class="row">
		<div class="large-6 columns">

			<div class="info text-center">
				<img class="float-center" src="assets/img/ic3-contato.png">
				<h6 class="color-cinza-forte titillium_regular">55 <strong>3511-3490</strong> - 55 <strong>98448 0860</strong></h6>
			</div>	
			<br class="hide-for-large">		
		
			<br><br>
		
			<div class="info text-center">
				<img class="float-center" src="assets/img/ic1-contato.png">
				<h6 class="color-cinza-forte titillium_regular">maqpesa.ind@gmail.com</h6>
			</div>
			<br class="hide-for-large">			
		
			<br><br>

			<div class="info text-center">
				<img class="float-center" src="assets/img/ic2-contato.png">
				<h6 class="color-cinza-forte titillium_regular">Rodovia BR 472, 1200 - KM 7, Guia Lopes - Santa Rosa - RS - Brasil</h6>
			</div>
		
	
		</div>


				<!-- FORMULARIO CONTATO -->
		<div class="large-6 columns form-contato">
			<form id="form" action="javascript:void(0)" method="post" accept-charset="utf-8" data-abide novalidate>
				
				<label class=""><span class="hide-for-large color-verde-claro font18 margin-label"><strong>Nome</strong></span>
			        <input type="text" name="nome" id="nome" class="inputs-formularios-contato required" placeholder="Nome">
			    </label>
				<br>
			    <label class=""><span class="hide-for-large color-verde-claro font18 margin-label"><strong>E-mail</strong></span>
			        <input type="email" name="email" id="email" class="inputs-formularios-contato required" placeholder="E-mail">
			    </label>
				<br>
			    <label class=""><span class="hide-for-large color-verde-claro font18 margin-label"><strong>Telefone</strong></span>
			        <input type="tel" name="telefone" id="telefone" class="inputs-formularios-contato corrige-tamanh-tel required" placeholder="Telefone">
			    </label>

			    <div class="form-contato">
			   		
			   		<label class=""><span class="hide-for-large color-verde-claro font18 margin-label"><strong>Mensagem</strong></span>
			        <textarea name="msg" id="msg" class="inputs-formularios-contato required" placeholder="Mensagem"></textarea>
			    	</label>

			    	<br>
					<p class="campos-obrigatorios float-right">Todos os campos são obrigatórios*</p>
					<div style="clear: both"></div>

			    	<button type="submit" class="btn-enviar-contato float-right btn-ok titillium_bdbold" onClick="arrayObjects['Site'].contato('form')">ENVIAR</button>
			    </div>
			</form>
		</div>
	</div>
	
	<br><br><br>


	

	<div id="alerts"></div>
</div>


	<br>


<section class="maps">
	<div class="container-full show-for-large">
		<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3527.5371227548394!2d-54.51638918537057!3d-27.854766239856456!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1srodovia+br+472+guia+lopes+santa+rosa!5e0!3m2!1spt-BR!2sbr!4v1566570156046!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	<br>
</section>



<?php require('rodape.php'); ?>

</div>

<script>
$('#telefone').mask('(99)9999-9999?9');
</script>