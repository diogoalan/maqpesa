<h1 class="color-verde">CADASTRO DE PRODUTOS</h1>
<p class="color-azul">Preencha abaixo os campos para adicionar um novo produto.</p>


<br>
<form id="form-produto" action="javascript:void(0);">
	<div class="row">
		<div class="col-xs-12">
			<input type="hidden" id="action" name="action" value="add">
			<input type="hidden" id="id" name="id">
			<p class="color-azul"><strong>Estado do produto</strong></p>
			<div class="radio">
			 	<label>
			    	<input type="radio" id="estado-ativo" name="estado" checked value="1"> Ativo
			  	</label>
					
			  	<label class="margin-left-radio">
			    	<input type="radio" id="estado-inativo" name="estado" value="0"> Inativo
			  	</label>
			</div>		
		</div>

		<br>

		<div class="col-xs-12">
			<br>
			<div class="form-group">
		    	<label class="color-azul">Nome do produto</label>
		    	<input type="text" id="nome" name="nome" class="form-control required">
		  	</div>	
		</div>

		

		<div class="col-xs-12">
			<br>
			<label class="color-azul">Categoria</label>
			<select id="select-categoria" name="select-categoria" class="form-control" onChange="load('Produtos', 'select', this)">
			  <option value="0">Selecione uma categoria</option>
			</select>
		</div>	

		
		<!-- DESCRICAO PRODUTO -->
		<div class="col-xs-12">
			<br>
			<div class="form-group">
		    	<label class="color-azul">Descrição</label>
		    	<textarea class="form-control required" name="descricao" id="descricao" rows="6"></textarea>
		  	</div>	
		</div>


		<!-- UPLOADS IMG DO PRODUTO -->
		<div class="col-xs-12">
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<div class="input-file text-center">
						<input type="file" name="upload[]" id="upload" multiple onChange="load('Produtos', 'upload', this)">
						<p class="color-branco texto-upload-img"><strong>Clique aqui para adicionar imagens do produto</strong></p>	
					</div>
				</div>
				<div class="col-xs-12 col-md-8 bg-branco padding10">
					<div id="preview" class="">
					</div>
				</div>
			</div>
		</div>

		

		<!-- VIDEO DO PRODUTO -->
		<div class="col-xs-12">
			<br><br>
			<div class="form-group">
		    	<label class="color-azul">Video do Produto</label>
		    	<input type="text" id="video-produto" name="video-produto" class="form-control" placeholder="https://youtu.be/48B8rQdxQcc">
		  	</div>	
		</div>


		<!-- OPCIONAIS -->
		<div class="col-xs-12">
		<br>
			<div class="row">
				<div class="col-xs-12 col-md-10 form-group">
			    	<label class="color-azul">Opcionais</label>
			    	<input type="text" id="opcionais" name="opcionais" class="form-control">
			  	</div>
			
			  	<div class="col-xs-12 col-md-2 form-group">
				  	<br>
				  	<button class="btn btn-default altura-add-opcionais" id="opcionais" type="button" onClick="arrayObjects['Produtos'].newOpcional('opcionais', 'form-produto');">  
							<span class="glyphicon glyphicon-plus color-verde" aria-hidden="true"></span> Adicionar
					</button>
				</div>
			  	<div class="col-xs-12 visualizar-opcionais">
		  			<!-- <div class="pull-left">
						<span class="glyphicon glyphicon-remove tamanho-x" aria-hidden="true"></span>
					</div>
					<h5 class="color-azul margin-text-opcional">Opcional 1</h5> -->
			  	</div>
			</div>	
		</div>

</form>



		<!-- UPLOAD IMG ESPECIFICAÇÔES -->
		<div class="col-xs-12">
			<br><br>
			<label class="color-azul">Imagem Especificações</label>
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<div class="input-file2 text-center form-group">
						<form id="form-img-especificacoes" action="javascript:void(0);">
							<input type="file" name="img-especificacoes" id="img-especificacoes" onChange="load('Produtos', 'img-especificacoes', this)">
							<p class="color-branco texto-upload-img2"><strong>Clique aqui para adicionar a imagem das especificações</strong></p>
						</form>
					</div>
				</div>
				<div class="col-xs-12 col-md-8 bg-branco padding10">
					<div id="previewEspecificacoes" class="">
					</div>
				</div>
			</div>
		</div>

		



		<!-- BOTOES SALVAR E CANCELAR -->
		<div class="col-xs-6">
			<br><br>
			<button type="button" class="btn btn-lg bg-verde color-branco btn-ok" onClick="add('Produtos', 'form-produto')">
				<span class="glyphicon glyphicon-ok color-branco" aria-hidden="true"></span> Salvar</button>
			<br><br><br><br>
		</div>
		<div class="col-xs-6">
			<br><br><br>
			<button type="button" class="btn btn-lg bg-vermelho color-branco pull-right" onClick="modifyScreen('produtos', 'Produtos')">
				<span class="glyphicon glyphicon-remove color-branco" aria-hidden="true"></span> Cancelar
			</button>
			<br><br><br><br>
		</div>



	  
</div>

<div id="alerts2"></div>
<!-- <script>

$('#form-produto').on('keypress', function(e){
	if(e.keyCode != 13) return;

	enterOpcioanis('opcionais', 'newOpcional');

});


</script> -->