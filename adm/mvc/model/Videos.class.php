<?php 

class Videos{
	
	private $id;
	private $link;
	private $produtoId;
	private $noticiaId;


	function setId($id){
		$this->id = $id;
	}

	function getId(){
		return $this->id;
	}

	function setLink($link){
		$this->link = $link;
	}

	function getLink(){
		return $this->link;
	}

	function setProdutoId($produtoId){
		$this->produtoId = $produtoId;
	}

	function getProdutoId(){
		return $this->produtoId;
	}

	function setNoticiaId($noticiaId){
		$this->noticiaId = $noticiaId;
	}

	function getNoticiaId(){
		return $this->noticiaId;
	}




}