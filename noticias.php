<?php require('topo-padrao.php'); ?>

<div class="row text-center loading">
   	<img src="assets/img/loading-verde.svg" />
</div>

<div id="conteudo">

<!-- BACKGRUPS -->
<div class="row show-for-large">
	<br>
	<div class="columns large-12">	
		<h6 class="color-verde-claro titillium_regular font12"><a href="index.php" class="hover-brancons">Página Inicial</a>/ <a href="noticias.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">Notícias</strong></a>

		<!-- <h6 class="color-verde-claro titillium_regular font12"><a href="index.php" class="hover-brancons">Página Inicial</a>/ <a href="noticias.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">Notícias</strong></a>/ <a href="index.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">Nome_Noticia</strong></a> </h6> -->
    </div>
    <br>
</div>


<!-- SECTION PRODUTOS -->
<section class="noticias">
	<br><br>
	<div class="row">
		<div class="column">	
			<h1 class="color-verde-claro text-center show-for-large titilliumsemibold font52">NOTÍCIAS</h1>
			<h1 class="color-verde-claro text-center hide-for-large titilliumsemibold font42">NOTÍCIAS</h1>
			<h4 class="color-laranja2 text-center titilliumsemibold">Seja bem vindo ao portal de notícias da Maqpesa <br> Fique por dentro das novidades!</h4>
		</div>
	</div>
	


	<br><br><br>



	<!-- Noticia Destaque DESKTOP -->
	<div id="noticiaDestaqueDesktop" class="row show-for-large">
		<!-- <div class="large-4 columns">		
			
			<div class="img-noticia-destaque">
				<a href="noticias-interno.php"><img class="float-center" src="assets/img/img-noticias-pagina-inicial.png"></a>
			</div>
			
		</div>
			
		<div class="large-8 columns">		
			<div class="box-noticia">
				<h6 class="color-laranja titillium_regular font14"><i>10/08/16</i></h6>

				<h4 class="titillium_bdbold"><a class="color-verde-claro" href="noticias-interno.php"> Navegue também pelas nossas notícias</a></h4>

				<p class="color-cinza-forte titillium_regular font12">Navegue também pelas nossas notícias, e fique por dentro das novidadesNavegue também pelas nossas notícias notícias</p>

				<br>

				<a class="show-for-large" href="noticias-interno.php"><div class="veja-mais-noticias">VEJA MAIS</div></a>

				<a class="hide-for-large" href="noticias-interno.php"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>

			</div>
		</div> -->	
	</div>


	<!-- Noticia Destaque MOBILE -->
	<div id="noticiaDestaqueMobile" class="row hide-for-large">
		<!-- <div class="column">		
			<a href="noticias-interno.php"><img class="float-center" src="assets/img/img-noticias-pagina-inicial.png"></a>
			<br>
			<div class="box-noticia float-center">
				<h6 class="color-laranja titillium_regular font14 text-center"><i>10/08/16</i></h6>
				<h5 class="titillium_bdbold text-center"><a class="color-verde-claro" href="noticias-interno.php"> Navegue também pelas nossas notícias, e fique por blablablalbla blabla bla bla</a></h5>
				
				<p class="color-cinza-forte titillium_regular font12">Navegue também pelas nossas notícias, e fique por dentro das novidadesNavegue também pelas nossas notícias notícias</p>

				<br>

				<a class="show-for-large" href="noticias-interno.php"><div class="veja-mais-noticias">VEJA MAIS</div></a>

				<a class="hide-for-large" href="noticias-interno.php"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>

				<br>
			</div>
		</div> -->
	</div>

<br><br>

	<!-- LINHA SEPARAÇÂO -->
	<div class="row">
		<div class="column">
			<hr class="linha-noticia show-for-large">
			<hr style="width:100%;" class="hide-for-large">
		</div>
	</div>



	<br><br>

	
	<!-- LISTA NOTICIAS -->
	<div id="all-noticias" class="row small-up-1 medium-up-2 large-up-3">
		<br>
<!-- 		<div class="column margin-noticias">
			<div class="box-shadow-noticias-pagina float-center">
				<div class="img-noticia">
					<a href="noticias-interno.php"><img src="assets/img/img-noticias-pagina-inicial.png" class="float-center" alt=""></a>
				</div>

				<div class="box-noticia">	
					<p class="font12 color-laranja titillium_regular">20/12/1994</p>

					<h6 class="titillium_bdbold"><a class="color-verde-claro" href="#"> Navegue também pelas nossas notícias, e fique por</a></h6>

					<p class="color-cinza-forte titillium_regular font12">Navegue também pelas nossas notícias, e fique por dentro das novidadesNavegue também pelas nossas notícias notícias</p>

					<br>

					<a href="noticias-interno.php"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>

					<br>

				</div>
			</div>
		</div>


		<div class="column margin-noticias">
			<div class="box-shadow-noticias-pagina float-center">
				<div class="img-noticia">
					<a href="noticias-interno.php"><img src="assets/img/img-noticias-pagina-inicial.png" class="float-center" alt=""></a>
				</div>

				<div class="box-noticia">	
					<p class="font12 color-laranja titillium_regular">20/12/1994</p>

					<h6 class="titillium_bdbold"><a class="color-verde-claro" href="#"> Navegue também pelas nossas notícias, e fique por</a></h6>

					<p class="color-cinza-forte titillium_regular font12">Navegue também pelas nossas notícias, e fique por dentro das novidadesNavegue também pelas nossas notícias notícias</p>

					<br>

					<a href="noticias-interno.php"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>

					<br>

				</div>
			</div>
		</div>


		<div class="column margin-noticias">
			<div class="box-shadow-noticias-pagina float-center">
				<div class="img-noticia">
					<a href="noticias-interno.php"><img src="assets/img/img-noticias-pagina-inicial.png" class="float-center" alt=""></a>
				</div>

				<div class="box-noticia">	
					<p class="font12 color-laranja titillium_regular">20/12/1994</p>

					<h6 class="titillium_bdbold"><a class="color-verde-claro" href="#"> Navegue também pelas nossas notícias, e fique por</a></h6>

					<p class="color-cinza-forte titillium_regular font12">Navegue também pelas nossas notícias, e fique por dentro das novidadesNavegue também pelas nossas notícias notícias</p>

					<br>

					<a href="noticias-interno.php"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>

					<br>

				</div>
			</div>
		</div> -->
		
	</div>

	
	<br><br><br>

</section>

<?php require('rodape.php'); ?>

<!-- <script>
arrayObjects['Site'].ListarNoticia();
</script> -->

</div>