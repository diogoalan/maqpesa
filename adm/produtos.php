<h1 class="color-verde">PRODUTOS</h1>
<p class="color-azul">Veja abaixo os produtos cadastrados. Aqui você tem a possibilidade de criar, alterar ou excluir produtos.</p>


<br>
<div class="row">
	<div class="col-sm-12 col-md-6">
		<!-- BOTÃO ADICIONAR -->
		<button class="btn btn-default" type="button" onClick="showHide('add-produtos', 'show')">  
			<span class="glyphicon glyphicon-plus color-verde" aria-hidden="true"></span> Adicionar
		</button>


		<input type="hidden" name="id" id="id">


		<!-- BOTÃO FILTRO MOBILE -->
		<!-- <div class="dropdown pull-right visible-xs visible-sm">
		  	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
		    Fitro
		    	<span class="caret"></span>
		  	</button>
		  	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		  		<li><a href="#" onClick="buscaBtn('Todos')">Todos</a></li>
		    	<li><a href="#" onClick="buscaBtn('Ativos')">Ativos</a></li>
		    	<li><a href="#" onClick="buscaBtn('Inativos')">Inativos</a></li>
		    	<li><a href="#" onClick="buscaBtn('Destaques')">Destaques</a></li>
		  	</ul>
		</div> -->
	</div>

	<div class="col-sm-12 col-md-6 ">

		<!-- BOTÃO FILTRO -->
		<!-- <div class="dropdown pull-right visible-md-block visible-lg-block">
		  	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
		    Fitro
		    	<span class="caret"></span>
		  	</button>
		  	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		  		<li><a href="#" onClick="buscaBtn('Todos')">Todos</a></li>
		    	<li><a href="#" onClick="buscaBtn('Ativos')">Ativos</a></li>
		    	<li><a href="#" onClick="buscaBtn('Inativos')">Inativos</a></li>
		    	<li><a href="#" onClick="buscaBtn('Destaques')">Destaques</a></li>
		  	</ul>
		</div> -->



		<!-- INPUT BUSCA -->
<!-- 		<div class="form-group margin-campo-busca pull-right visible-md-block visible-lg-block">
		    <input type="text" class="form-control" onKeyup="buscaKeyboard(this, 'td-nome-produto')" placeholder="Buscar...">
		</div>	 -->
		
		<br>

		<!-- INPUT BUSCA MOBILE -->
<!-- 		<div class="form-group visible-xs visible-sm">
		    <input type="text" class="form-control" onKeyup="buscaKeyboard(this, 'td-nome-produto')" placeholder="Buscar...">
		</div>	 -->	
	</div>			
</div>

<div id="alerts2"></div>

<br>

<div class="row">
	<div class="col-xs-12">
		<div class="table-responsive bg-branco">
		  	<table class="table table-hover">
		  		<thead>
			    	<tr id="head">
					  	<th>Título</th>
					  	<th>Ativo</th>
					  	<th>Ações</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<br>
		<div id="alert-return-table"></div>
	</div>
</div>

<!-- MODAL REMOVER -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalRemover" aria-labelledby="myModalLabel">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
      			<input type="hidden" name="id-remove" id="id-remove">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h3 class="modal-title">Atenção!</h3>
      		</div>
      		<div class="modal-body">
        		<p>Se você excluir esse produto não será possível recupera-lo. Tem certeza disso?</p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        		<button type="button" class="btn btn-primary bg-verde" onClick="remover('Produtos', this)">Sim</button>
      		</div>
    	</div>
  	</div>
</div>