<?php 
session_start();
if($_SESSION['login'] != 'true'){?>
	<script>
		window.location.href="../adm";
	</script>
<?php } ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="format-detection" content="telephone=no">
	<title>Painel de Controle</title>
	<link type="image/ico" rel="icon" href="assets/img/favicon.ico">
	<link rel="stylesheet" href="assets/css/normalize.css">
	<link rel="stylesheet" href="assets/css/paradeiser.css">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/daterangepicker.css">
</head>
<body class="bg-azul">



<div class="container-fluid altura-coluna">
	<div class="altura-mobile hidden-md hidden-lg"></div>
	<!-- MENU MOBILE -->
	<nav id="paradeiser-dropdown" class="paradeiser headroom headroom--not-top headroom--pinned hidden-md hidden-lg">
	    <a onClick="controllScreen('dashboard')" href="#">
	        <span class="glyphicon glyphicon-home font20 color-verde" aria-hidden="true"> </span>
	        <span>Início</span>
	    </a>
	    <a onClick="controllScreen('produtos')" href="#">
	        <span class="glyphicon glyphicon-shopping-cart font20 color-verde" aria-hidden="true"></span>
	        <span>Produtos</span>
	    </a>
	    <a onClick="controllScreen('categorias')" href="#">
	        <span class="glyphicon glyphicon-th font20 color-verde" aria-hidden="true"></span>
	        <span>Categorias</span>
	    </a>
	    <a onClick="controllScreen('noticias')" href="#">
	        <span class="glyphicon glyphicon-th-list font20 color-verde" aria-hidden="true"></span>
	        <span>Notícias</span>
	    </a>
	</nav>
	



	<!-- MENU LATERAL DESKTOP -->
	<div class="row altura-coluna">
		<div class="col-md-3 bg-azul altura-coluna hidden-sm hidden-xs">
			<br>
			<img src="assets/img/logo.png" alt="Elede" class="center-block">

			<br><br>
			<p class="color-azul">Navegue pelo sistema com as opções abaixo:</p>
			<br><br>

			<a onClick="controllScreen('dashboard')" href="#">
				<div id="dashboard" class="row menu-ativo">
					<p class="font20">
					 	<span class="glyphicon glyphicon-home font20 margin-icone-menu" aria-hidden="true"></span>Início
					</p>
				</div>
			</a>
			
			<a onClick="controllScreen('produtos')" href="#">
				<div id="produtos" class="row menu">
					<p class="font20">
						 <span class="glyphicon glyphicon-shopping-cart font20 margin-icone-menu" aria-hidden="true"></span>Produtos
					</p>
				</div>
			</a>


			<a onClick="controllScreen('categorias')" href="#">
				<div id="categorias" class="row menu">
					<p class="font20">
						<span class="glyphicon glyphicon-th font20 margin-icone-menu" aria-hidden="true"></span>Categorias
					</p>
				</div>
			</a>


			<a onClick="controllScreen('noticias')" href="#">
				<div id="noticias" class="row menu">
					<p class="font20">
					 	<span class="glyphicon glyphicon-th-list font20 margin-icone-menu" aria-hidden="true"></span>Notícias
					</p>
				</div>
			</a>
		</div>
		






		<!-- CONTEUDO  -->
		<div class="col-md-9 bg-cinza altura-coluna2">
			<!-- BARRA TOPO -->
			<div class="row">	
				<div class="barra-topo">
					<div class="col-xs-9 col-md-8">
						<h4 class="color-azul corrige-alt-h4-barra pull-left">Painel de Controle 
							<span class="color-verde hidden-xs">Imak Industrial
						</span></h4>	
					</div>
						
					<div class="col-xs-3 col-md-4">
						<a href="" class="color-azul corrige-alt-sair-barra pull-right font18" data-toggle="modal" data-target="#modalSair">
							<span class="glyphicon glyphicon-log-out font20 color-verde" aria-hidden="true"></span> Sair
						</a>
					</div>
					
				</div>
			</div>
			

			<img id="loading" class="loader-principal" src="assets/img/loading-verde.svg">

			<br><br>
			<div id="conteudo">
			<?php 
			include("dashboard.php");
			?>
			</div>

			<br><br><br><br>
		</div>
	</div>
	

</div>








<!-- MODAL SAIR -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalSair" aria-labelledby="myModalLabel">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h3 class="modal-title">Atenção!</h3>
      		</div>
      		<div class="modal-body">
        		<p>Tem certeza que deseja sair do sistema?</p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        		<button type="button" class="btn btn-primary bg-verde" onClick="sair()">Sim</button>
      		</div>
    	</div>
  	</div>
</div>


<!-- MODAL RESPONDER ORÇAMENTO -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalResponder" aria-labelledby="myModalLabel">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h3 class="modal-title">Responder cliente</h3>
      		</div>
      		<div class="modal-body">
      			<form id="resposta" action="javascript:void(0)">
        			<textarea class="form-control required" name="msg-resposta" id="msg-resposta" rows="6"></textarea>
        		</form>
      		</div>
      		<div class="modal-footer">
        		<button type="button" id="modal-cancelar-resposta" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        		<button type="button" class="btn btn-primary bg-verde" onClick="add('Orcamentos', 'resposta')">Responder</button>
        		<br><br>
        		<div id="alerts"></div>
      		</div>
    	</div>
  	</div>
</div>

<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/headroom/0.7.0/headroom.min.js"></script>
<script type="text/javascript" src="assets/js/Requisicoes.js"></script>
<script type="text/javascript" src="assets/js/Validacoes.js"></script>
<script type="text/javascript" src="assets/js/Categorias.js"></script>
<script type="text/javascript" src="assets/js/Login.js"></script>
<script type="text/javascript" src="assets/js/Noticias.js"></script>
<script type="text/javascript" src="assets/js/Dashboard.js"></script>
<script type="text/javascript" src="assets/js/Produtos.js"></script>
<script type="text/javascript" src="assets/js/ajax-form.js"></script>
<script type="text/javascript" src="assets/js/moment.min.js"></script>
<script type="text/javascript" src="assets/js/daterangepicker.js"></script>
<script type="text/javascript" src="assets/js/Site.js"></script>
<script type="text/javascript" src="assets/js/Main.js"></script>

<script type="text/javascript">
	document.getElementById("paradeiser-dropdown").addEventListener("click", function(event){
	    event.preventDefault(); 
	});

	var myElement = document.querySelector(".paradeiser");
	var headroom  = new Headroom(myElement, {
	    tolerance : 5,
	});
	headroom.init();
</script>

</body>
</html>


