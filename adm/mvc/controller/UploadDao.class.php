<?php 

class UploadDao {


	private $dbc;

	public function __construct() {
        $this->dbc = new DbConnection();
        $this->dbc->createConnection();
    }

    public function insertUpload(Uploads $upload){

    	$sql = 'insert into tb_uploads (upload_img, upload_capa, produto_id, noticia_id) values (:img, :capa, :id, :noticia_id)';
    	$aux = $this->dbc->createConnection()->prepare($sql);
    	$aux->bindValue(':img', $upload->getImg());
    	$aux->bindValue(':capa', $upload->getCapa());
    	$aux->bindValue(':id', $upload->getProdutoId());
        $aux->bindValue(':noticia_id', $upload->getNoticiaId());
    	$aux->execute();

    }

    public function selectAllUpload(Uploads $upload){

        $sql = 'select * from tb_uploads where produto_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':id', $upload->getProdutoId());
        $aux->execute();
        return $aux->fetchAll();
    }

    public function selectAllUploadNoticia(Uploads $upload){

        $sql = 'select * from tb_uploads where noticia_id = :noticia_id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':noticia_id', $upload->getNoticiaId());
        $aux->execute();
        return $aux->fetchAll();
    }

    public function removeUpload(Uploads $upload){

        $sql = 'delete from tb_uploads where produto_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':id', $upload->getProdutoId());
        $aux->execute();

    }

    public function removeUploadNoticia(Uploads $upload){

        $sql = 'delete from tb_uploads where noticia_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':id', $upload->getNoticiaId());
        $aux->execute();

    }

}