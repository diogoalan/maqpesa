<h1 class="color-verde">CADASTRO DE NOTÍCIAS</h1>
<p class="color-azul">Preencha abaixo os campos para adicionar uma nova notícia.</p>


<br>
<form id="form-noticia" action="javascript:void(0);">
	<div class="row">
		<div class="col-xs-12">
			<input type="hidden" id="action" name="action" value="add">
			<input type="hidden" id="id" name="id">
			<p class="color-azul"><strong>Estado da notícia</strong></p>
			<div class="radio">
			 	<label>
			    	<input type="radio" id="estado-ativo" name="estado" checked value="1"> Ativo
			  	</label>
					
			  	<label class="margin-left-radio">
			    	<input type="radio" id="estado-inativo" name="estado" value="0"> Inativo
			  	</label>
			</div>		
		</div>

		<br>

		<div class="col-xs-12">
			<br>
			<div class="form-group">
				<label class="color-azul">Data da Notícia</label>
				<input type="text" name="birthdate" id="birthdate" class="form-control required" onmouseup="dataAlbum()">
			</div>
		</div>

		<br>

		<!-- TITULO DA NOTICA -->
		<div class="col-xs-12">
			<br>
			<div class="form-group">
		    	<label class="color-azul">Título da Notícia</label>
		    	<input type="text" id="titulo" name="titulo" class="form-control required">
		  	</div>	
		</div>

		<!-- SUB TITULO DA NOTICIA -->
		<div class="col-xs-12">
			<br>
			<div class="form-group">
		    	<label class="color-azul">Sub-título da Notícia</label>
		    	<input type="text" id="sub-titulo" name="sub-titulo" class="form-control required">
		  	</div>	
		</div>		

		
		<!-- TEXTO DA NOTICIA -->
		<div class="col-xs-12">
			<br>
			<div class="form-group">
		    	<label class="color-azul">Texto Notícia</label>
		    	<textarea class="form-control required" name="descricao" id="descricao" rows="6"></textarea>
		  	</div>	
		</div>


		<!-- VIDEO DA NOTÍCIA -->
		<div class="col-xs-12">
			<br><br>
			<div class="form-group">
		    	<label class="color-azul">Video da Notícia</label>
		    	<input type="text" id="video-noticia" name="video-noticia" class="form-control" placeholder="https://youtu.be/48B8rQdxQcc">
		  	</div>	
		</div>
		

		<div class="col-xs-12">
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<div class="input-file text-center">
						<input type="file" name="upload[]" id="upload" multiple onChange="load('Noticias', 'upload', this)">
						<p class="color-branco texto-upload-img"><strong>Clique aqui para adicionar imagens</strong></p>	
					</div>
				</div>
				<div class="col-xs-12 col-md-8 bg-branco padding10">
					<div id="preview" class="">
					</div>
				</div>
			</div>
		</div>


		<!-- BOTOES SALVAR E CANCELAR -->
		<div class="col-xs-6">
			<br><br><br>
			<button type="button" class="btn btn-lg bg-verde color-branco btn-ok" onClick="add('Noticias', 'form-noticia')">
				<span class="glyphicon glyphicon-ok color-branco" aria-hidden="true"></span> Salvar</button>
			<br><br><br><br>
		</div>
		<div class="col-xs-6">
			<br><br><br>
			<button type="button" class="btn btn-lg bg-vermelho color-branco pull-right" onClick="modifyScreen('noticias', 'Noticias')">
				<span class="glyphicon glyphicon-remove color-branco" aria-hidden="true"></span> Cancelar
			</button>
			<br><br><br><br>
		</div>
</form>	

</div>

<div id="alerts2"></div>
