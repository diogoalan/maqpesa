<?php 

class CategoriaDao {


	private $dbc;

	public function __construct() {
        $this->dbc = new DbConnection();
        $this->dbc->createConnection();
    }

    public function insertCategoria(Categorias $categoria){

        $sql = 'insert into tb_categorias (categoria_nome) values (:nome)';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':nome', $categoria->getNome());
        $aux->execute();
        
    }

    public function updateCategoria(Categorias $categoria){

        $sql = 'update tb_categorias set categoria_nome = :categoria where categoria_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':categoria', $categoria->getNome());
        $aux->bindValue(':id', $categoria->getId());
        $aux->execute();

    }

    public function lastId(){

        $sql2 = 'select categoria_id from tb_categorias order by categoria_id desc limit 1';
        $aux2 = $this->dbc->createConnection()->prepare($sql2);
        $aux2->execute();
        return $aux2->fetchAll();
        
    }

    public function selectAllCategoria(){

    	$sql = 'select * from tb_categorias';
    	$aux = $this->dbc->createConnection()->prepare($sql);
    	$aux->execute();
    	return $aux->fetchAll();

    }

    // public function verificaExistenciaSubcategoria(Categorias $categoria){

    //     $sql = 'select * from tb_categorias inner join tb_subcategorias on tb_categorias.categoria_id = tb_subcategorias.categoria_id 
    //     where tb_categorias.categoria_id = :id';
    //     $aux = $this->dbc->createConnection()->prepare($sql);
    //     $aux->bindValue(':id', $categoria->getId());
    //     $aux->execute();
    //     return $aux->fetchAll();

    // }

    public function verificaExistenciaProdutos(Categorias $categoria){

        $sql = 'select * from tb_categorias inner join tb_produtos on tb_categorias.categoria_id = tb_produtos.categoria_id 
        where tb_categorias.categoria_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':id', $categoria->getId());
        $aux->execute();
        return $aux->fetchAll();

    }

    public function removeCategoria(Categorias $categoria){
        $sql = 'delete from tb_categorias where categoria_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':id', $categoria->getId());
        $aux->execute();
    }

}