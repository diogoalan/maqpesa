<?php 

error_reporting(E_ERROR);

class NoticiaDao {

	private $dbc;

	public function __construct(){
		$this->dbc = new DbConnection();
		$this->dbc->createConnection();
	}

	public function insertNoticia(Noticias $noticia){
		
		$sql = 'insert into tb_noticias (noticia_status, noticia_titulo, noticia_subtitulo, noticia_descricao, noticia_video, noticia_data) 
		values(:status, :titulo, :subtitulo, :descricao, :video, :data)';
		$aux = $this->dbc->createConnection()->prepare($sql);
		$aux->bindValue(':status', $noticia->getStatus());
		$aux->bindValue(':titulo', $noticia->getTitulo());
		$aux->bindValue(':subtitulo', $noticia->getSubtitulo());
		$aux->bindValue(':descricao', $noticia->getDescricao());
		$aux->bindValue(':video', $noticia->getVideo());
		$aux->bindValue(':data', $noticia->getData());
		return $aux->execute();
	}

	public function updateNoticia(Noticias $noticia){
		
		$sql = 'update tb_noticias set noticia_status = :status, noticia_titulo = :titulo, noticia_subtitulo = :subtitulo, noticia_descricao = :descricao, noticia_video = :video, noticia_data = :data where noticia_id = :id';
		$aux = $this->dbc->createConnection()->prepare($sql);
		$aux->bindValue(':id', $noticia->getId());
		$aux->bindValue(':status', $noticia->getStatus());
		$aux->bindValue(':titulo', $noticia->getTitulo());
		$aux->bindValue(':subtitulo', $noticia->getSubtitulo());
		$aux->bindValue(':descricao', $noticia->getDescricao());
		$aux->bindValue(':video', $noticia->getVideo());
		$aux->bindValue(':data', $noticia->getData());
		$aux->execute();
	}

	public function updateStatus(Noticias $noticia){

        $sql = 'update tb_noticias set noticia_status = :status where noticia_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':id', $noticia->getId());
        $aux->bindValue(':status', $noticia->getStatus());
        $aux->execute();

    }

    public function removeNoticia(Noticias $noticia){

    	$sql = 'delete from tb_noticias where noticia_id = :id';
    	$aux = $this->dbc->createConnection()->prepare($sql);
    	$aux->bindValue(':id', $noticia->getId());
    	$aux->execute();
    }

    public function lastId(){

        $sql2 = 'select noticia_id from tb_noticias order by noticia_id desc limit 1';
        $aux2 = $this->dbc->createConnection()->prepare($sql2);
        $aux2->execute();
        return $aux2->fetchAll();
        
    }
    // aqui
    public function selectAllNoticias(){

    	// $sql = 'select * from tb_noticias order by tb_noticias.noticia_data desc';
    	$sql = 'select * from tb_noticias inner join tb_uploads on tb_noticias.noticia_id = tb_uploads.noticia_id where tb_uploads.upload_capa = 1 order by tb_noticias.noticia_id desc';
    	$aux = $this->dbc->createConnection()->prepare($sql);
    	$aux->execute();
        return $aux->fetchAll();
    }

    public function selectFromNoticias(){

        // $sql = 'select * from tb_noticias order by tb_noticias.noticia_data desc';
        $sql = 'select * from tb_noticias inner join tb_uploads on tb_noticias.noticia_id = tb_uploads.noticia_id where tb_uploads.upload_capa = 1 order by tb_noticias.noticia_data desc';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->execute();
        return $aux->fetchAll();
    }

    // public function selectFromNoticias(){
    //     $sql = 'select * from tb_noticias inner join tb_uploads on tb_noticias.noticia_id = tb_uploads.noticia_id order by tb_uploads.upload_capa, tb_noticias.noticia_data, tb_noticias.album_id asc';
    //     $retornoBanco = $this->dbc->createConnection()->prepare($sql);
    //     $retornoBanco->execute();
    //     return $retornoBanco->fetchAll();
    // }

    public function selectNoticiaId(Noticias $noticia){

        $sql = 'select * from tb_noticias where tb_noticias.noticia_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':id', $noticia->getId());
        $aux->execute();
        return $aux->fetchAll();

    }
}