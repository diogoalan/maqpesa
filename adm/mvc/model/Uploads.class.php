<?php

class Uploads {


	private $id;
	private $img;
	private $capa;
	private $produtoId;
	private $noticiaId;


	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	public function setImg($img){
		$this->img = $img;
	}

	public function getImg(){
		return $this->img;
	}

	public function setCapa($capa){
		$this->capa = $capa;
	}

	public function getCapa(){
		return $this->capa;
	}

	public function setProdutoId($produtoId){
		$this->produtoId = $produtoId;
	}

	public function getProdutoId(){
		return $this->produtoId;
	}

	public function setNoticiaId($noticiaId){
		$this->noticiaId = $noticiaId;
	}

	public function getNoticiaId(){
		return $this->noticiaId;
	}

}
