<?php 

error_reporting(E_ERROR);

require '../loadClass.php';

function add(){

	$produtoModel = new Produtos();
	$produtoModel->setStatus($_POST['estado']);
	$produtoModel->setNome($_POST['nome']);
	$produtoModel->setDescricao($_POST['descricao']);
	$produtoModel->setCategoriaId($_POST['select-categoria']);
	$produtoModel->setVideo($_POST['video']);
	$produtoModel->setOpcional($_POST['opcionais']);
	$produtoModel->setProdutoEspecificacoes($_POST['img-especificacoes']);

	// var_dump($_POST['img-especificacoes']);

	$produto = new ProdutoDao();
	$produto->insertProduto($produtoModel);
	$aux = $produto->lastId();

	foreach($aux as $c){
		$aux = $c['produto_id'];
	}

	$uploadModel = new Uploads();
	$upload = new UploadDao();

	for($i = 0; $i < count($_POST['img']); $i++){

		if($_POST['capa'] == $_POST['img'][$i]['name']){
			$capa = 1;
		} else {
			$capa = 0;
		}

		$uploadModel->setImg($_POST['img'][$i]['name']);
		$uploadModel->setCapa($capa);
		$uploadModel->setProdutoId($aux);
		$upload->insertUpload($uploadModel);
	}

	echo 'true';
}

function update(){

	$produtoModel = new Produtos();
	$produtoModel->setId($_POST['id']);
	$produtoModel->setStatus($_POST['estado']);
	$produtoModel->setNome($_POST['nome']);
	$produtoModel->setDescricao($_POST['descricao']);
	$produtoModel->setCategoriaId($_POST['select-categoria']);
	$produtoModel->setVideo($_POST['video']);
	$produtoModel->setOpcional($_POST['opcionais']);
	$produtoModel->setProdutoEspecificacoes($_POST['img-especificacoes']);


	$produto = new ProdutoDao();
	$produto->updateProduto($produtoModel);

	echo json_encode($produtoModel->getId());
	
	$uploadModel = new Uploads();
	$uploadModel->setProdutoId($_POST['id']);

	$upload = new UploadDao();
	$upload->removeUpload($uploadModel);

	for($i = 0; $i < count($_POST['img']); $i++){

		if($_POST['capa'] == $_POST['img'][$i]['name']){
			$capa = 1;
		} else {
			$capa = 0;
		}

		$uploadModel->setImg($_POST['img'][$i]['name']);
		$uploadModel->setCapa($capa);
		$uploadModel->setProdutoId($_POST['id']);
		$upload->insertUpload($uploadModel);
	}

}

function updateTable(){

	if($_POST['function'] == 'status'){

		$produtoModel = new Produtos();
		$produtoModel->setId($_POST['id']);
		$produtoModel->setStatus($_POST['value']);

		$produto = new ProdutoDao();
		$produto->updateStatus($produtoModel);
	}
}

function remove(){

	if(count($aux) > 0){
		echo 'erro';
	} else {
		$uploadModel = new Uploads();
		$uploadModel->setProdutoId($_POST['id']);

		$upload = new UploadDao();
		$upload2 = new UploadDao();
		$upload2 = $upload2->selectAllUpload($uploadModel);

		foreach ($upload2 as $c) {
			$pasta = '../../assets/upload/' . $c['upload_img'];
			unlink($pasta);
		}

		$upload->removeUpload($uploadModel);

		$produtoModel = new Produtos();
		$produtoModel->setId($_POST['id']);

		$produto = new ProdutoDao();
		$produto->removeProduto($produtoModel);

		echo 'true';
	}

}

function removeImgEspecificacoes(){

	if(count($aux) > 0){
		echo 'erro';
	} else {
		$produtoModel = new Produtos();
		$produtoModel->setProdutoId($_POST['id']);

		foreach ($upload2 as $c) {
			$pasta = '../../assets/upload/' . $c['img-especificacoes'];
			unlink($pasta);
		}

		$produtoModel = new Produtos();
		$produtoModel->setId($_POST['id']);

		echo 'true';
	}

}

function selectProdutoId(){

	$produtoModel = new Produtos();
	$produtoModel->setId($_POST['id']);

	$produtos = new ProdutoDao();
	$produtos = $produtos->selectProdutoId($produtoModel);
	$return = array();
	$array = array();

	foreach ($produtos as $c) {
		$array['id'] = $c['produto_id'];
		$array['status'] = $c['produto_status'];
		$array['name'] = $c['produto_nome'];
		$array['descricao'] = $c['produto_descricao'];
		$array['categoria'] = $c['categoria_id'];
		$array['video'] = $c['produto_video'];
		$array['opcionais'] = $c['produto_opcional'];
		$array['img-especificacoes'] = $c['produto_especificacoes'];
		array_push($return, $array);
	}

	$json = json_encode($return);
	
	echo $json;

}

function selectUploadId(){

	$uploadModel = new Uploads();
	$uploadModel->setProdutoId($_POST['id']);

	$upload = new UploadDao();
	$upload = $upload->selectAllUpload($uploadModel);

	$return = array();
	$array = array();

	foreach ($upload as $c) {
		$array['img'] = $c['upload_img'];
		$array['capa'] = $c['upload_capa'];
		array_push($return, $array);
	}

	$json = json_encode($return);
	
	echo $json;

}

function listar(){

	$produtos = new ProdutoDao();
	$produtos = $produtos->selectAllProdutos();

	$return = array();
	$array = array();

	foreach ($produtos as $c) {
		$array['id'] = $c['produto_id'];
		$array['status'] = $c['produto_status'];
		$array['categoria'] = $c['categoria_id'];
		$array['categoriaNome'] = $c['categoria_nome'];
		$array['name'] = $c['produto_nome'];
		$array['descricao'] = $c['produto_descricao'];
		$array['video'] = $c['produto_video'];
		$array['opcionais'] = $c['produto_opcional'];
		$array['img-especificacoes'] = $c['produto_especificacoes'];
		$array['img'] = $c['upload_img'];
		array_push($return, $array);
	}

	$json = json_encode($return);
	
	echo $json;

}

function listarCategoria(){

	$categorias = new CategoriaDao();
	$categorias = $categorias->selectAllCategoria();

	$return = array();
	$array = array();

	foreach ($categorias as $c) {
		$array['id'] = $c['categoria_id'];
		$array['name'] = $c['categoria_nome'];
		array_push($return, $array);
	}

	$json = json_encode($return);
	
	echo $json;

}


switch ($_POST['action']) {
	case 'add':
		add();
	break;

	case 'update':
		update();
	break;

	case 'updateTable':
		updateTable();
	break;

	case 'remove':
		remove();
	break;

	case 'returnInfoId':
		selectProdutoId();
	break;

	case 'returnImg':
		selectUploadId();
	break;

	case 'categorias':
		listarCategoria();
	break;	

	default:
		listar();
	break;
}