function Validacoes(){



}

Validacoes.prototype = {
	
	camposEmBranco: function(idForm){
		var dados = $('#'+idForm).serialize(),			
			inputNull = 0;

		dados = dados.split('&');

		for(var i in dados){
			dados[i]  = dados[i].replace(idForm, "").replace("%5B","").replace("%5D","").split('=');
		
			if(dados[i][1] != '' && dados[i][1] != null && dados[i][1] != undefined && dados[i][1].indexOf('+') != 0){
				$('#'+dados[i][0]).css('border', '1px solid #CCC');
			}else if($('#'+dados[i][0]).hasClass('required')){
				$('form').animate({scrollTop: $('#'+dados[i][0]).height()}, 'slow');
				$('#'+dados[i][0]).css('border', '1px solid #d9534f');
				inputNull++;	
			}
		}

		if(inputNull > 0){
			return false;
		}

		return true
	},

	isYoutubeVideo: function(video) {  		
  		var verificaVideo = video;
 		
  		var expressao = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
   		
  		console.log(expressao.test(verificaVideo));

  		if(!expressao.test(verificaVideo) && $('#video-produto').val().length > 0){
  			$('#video-produto').css('border', '1px solid #d9534f');
  			return false;
  		}else{
  			return true;
  		}
	},

	isYoutubeVideoNoticia: function(video) {  		
  		var verificaVideo = video;
 		
  		var expressao = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
   		
  		console.log(expressao.test(verificaVideo));

  		if(!expressao.test(verificaVideo) && $('#video-noticia').val().length > 0){
  			$('#video-noticia').css('border', '1px solid #d9534f');
  			return false;
  		}else{
  			return true;
  		}
	},


	// dadosJSON: function(formName){
	// 	var dados = decodeURIComponent( $('#'+formName).serialize() ),			
	// 		jsonDados = {};	
		
	// 	dados = dados.split('&');
		
	// 	for(var i in dados){
	// 		dados[i]  = dados[i].replace(formName, "").replace("[","").replace("]","").split('=');			
	// 		jsonDados[dados[i][0]] = this.replaceAll(dados[i][1], '+', ' ');			
	// 	}

	// 	return jsonDados;
	// },

	// replaceAll: function(string, find, replace) {
	//  	return string.split(find).join(' ').replace(/[\'\"]/g,'');
	// },

	email: function(email){

		var emailValue = $('#' + email).val();

		if(emailValue == null || emailValue.length == 0 || emailValue.indexOf(".") == -1 || emailValue.indexOf("@") == -1 
			|| emailValue.indexOf(" ") != -1){
			$('#' + email).css('border', '1px solid #d9534f');
			return false;
		}
		
		expressao = /^[a-zA-Z0-9][a-zA-Z0-9\._\-&!?=#]*/;
		confere = expressao.exec(emailValue);

		if(!confere){
			$('#' + email).css('border', '1px solid #d9534f');
			return false;
		}
		
		// expressao = /@(\w{2,}\.(\w{2,}\.)?[a-zA-Z]{2,3})$/;
		// confere = expressao.exec(emailValue);
		// if(!confere){
		// 	$('#' + email).css('border', '1px solid #d9534f');
		// 	return false;
		// }
		
		return true;
	},

	alerts: function(type, msg){
		$('#alerts').removeClass('alert-danger alert-success');
		$('#alerts').html('');

		$('#alerts').addClass(type);
		$('#alerts').html(msg);

		$('.box-login form button').css('margin-top', '0px');

		$('#alerts').css('display', 'block');	
		$('#alerts').css('text-align', 'center');	

		setTimeout(function(){
			$('.box-login form button').css('margin-top', '40px');
			$('#alerts').css('display', 'none');
		},4000);

		$('#alerts2').removeClass('alert-danger alert-success');
		$('#alerts2').html('');

		$('#alerts2').addClass(type);
		$('#alerts2').html(msg);

		$('.box-login form button').css('margin-top', '0px');

		$('#alerts2').css('display', 'block');		

		setTimeout(function(){
			$('.box-login form button').css('margin-top', '40px');
			$('#alerts2').css('display', 'none');
		},4000);
	},

	url: function(url){

		var nome = url;

		var com_acento = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ'; 
		var sem_acento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC'; 
		var nova = ''; 

		for(i=0; i  < nome.length; i++) { 
			if(nome[i] == ' ' || nome[i] == '/'){
				nova += '-';
			}
			else if(nome[i] == '+'){
				nova += 'mais';
			}
			else {
				if (com_acento.search(nome.substr(i,1)) >= 0) { 
				    nova += sem_acento.substr(com_acento.search(nome.substr(i,1)),1); 
				} 
				else { 
				    nova+=nome.substr(i,1); 
				} 
			}
		} 

		nome = nova;
		nome = nome.toLowerCase();

		return nome;

	}

}