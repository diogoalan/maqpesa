<?php require('topo-padrao.php'); ?>

<div class="row text-center loading">
    <img src="assets/img/loading-verde.svg"/>
</div>

<div id="conteudo">

<!-- BACKGRUPS -->
<div class="row show-for-large">
	<br>
	<div id="navegacaoProduto" class="columns large-12">	
		<!-- <h6 class="color-verde-claro titillium_regular font12"><a href="index.php" class="hover-brancons">Página Inicial</a>/ <a href="produtos.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">Produtos</strong></a> </h6> -->
    </div>
</div>






<!-- SECTION PRODUTOS -->
<section class="produtos">
	<br><br>
	<!-- NOME PRODUTO -->
	<div class="row">
		<div class="column">	
			<h3 id="nome-produto" class="color-laranja text-left show-for-large titillium_bdbold"></h3>
			<h1 id="nome-produto" class="color-laranja text-center hide-for-large titillium_bdbold"></h1>
		</div>
	</div>
	


	<br>

	<!-- DESCRICAO PRODUTO -->
	<div class="row">

		<div class="column large-6">
			<h4 class="color-verde-claro titillium_bdbold">Características Técnicas</h4>

			<p id="descricao" class="color-cinza-forte titillium_regular">
			</p>

		</div>


		<!-- FOTOS PRODUTOS DESKTOP -->
		<div class="small-12 medium-9 large-6 columns galeria-produto">
			<br>
			<div class="text-center float-center">
				<div id="banner" class="slider-pro ">
					<div id="upload" class="" data-width="100%" data-ratio="450/300" data-max-width="100%" data-allowfullscreen="true"  data-nav="thumbs" data-autoplay="true" data-fit="contain" data-arrows="true" data-swipe="true"  data-keyboard="true">
          			</div>
			    </div>
		    </div>
		</div>

</section>




	<!-- LINHA SEPARAÇÂO -->
	<div class="row">
		<div class="column">
			<hr class="show-for-large linha-verde">
			<hr style="width:100%;" class="linha-verde hide-for-large">
		</div>
	</div>

<!-- VIDEOS E CARACTERISITICAS DO PRODUTO -->
<section>
	
	<br><br>	

	<!-- VIDEO DO PRODUTO -->
	<div id="video-produto" class="row">
		<!-- <div class="column large-12">
			<h4 class="color-verde-claro text-center show-for-large titillium_bdbold">Vídeo deste produto</h4>
			<h3 class="color-verde-claro text-center hide-for-large titillium_bdbold">Vídeo deste produto</h3>
			
			<br><br>
			<div>
				<div class="video text-center show-for-large">
					<iframe width="515" height="315" src="https://www.youtube.com/embed/izTHAqsoG7o" frameborder="0" allowfullscreen></iframe>
				</div>

				<div class="video text-center hide-for-large">
					<iframe width="100%" height="315" src="https://www.youtube.com/embed/izTHAqsoG7o" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div> -->
	</div>


	<br>

	<!-- OPCIONAIS -->
	<div id="opcionais" class="row">
		<div class="column">	
			
			<h4 class="color-verde-claro text-center show-for-large titillium_bdbold">Opcionais</h4>
			<h3 class="color-verde-claro text-center hide-for-large titillium_bdbold">Opcionais</h3>

			<br>
			
			<!-- <p class="color-cinza-forte titillium_regular text-center">Equipamento robusto</p> -->

		</div>
	</div>

	<br><br>

	<!-- IMG ESPECIFICAÇÔES -->
	<div id="img-especificacoes" class="row">
		<!-- <div class="large-12 column">

			<h4 class="color-verde-claro text-center show-for-large titillium_bdbold">Imagem especificações</h4>
			<h3 class="color-verde-claro text-center hide-for-large titillium_bdbold">Imagem especificações</h3>	
			
			<div class="img-especificacoes">
				<img class="float-center" style="width:700px; height:350px;" src="assets/img/Google_map-6.png">
			</div>

		</div> -->
	</div>

<br><br><br>

</section>






<?php require('rodape.php'); ?>

<script>
arrayObjects['Site'].listaProdutoIndividual();
</script>

</div>