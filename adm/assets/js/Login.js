function Login(){


}

Login.prototype = {

	login: function(dados){

		var callback = function(json){

			if(json == 'false'){
				arrayObjects['Validacoes'].alerts('alert-danger', 'E-mail ou senha incorretos! :(');
			} else {

				$('.btn-ok').html('<img style="width: 30px" src="assets/img/loading-branco.svg">');
				arrayObjects['Validacoes'].alerts('alert-success', 'Login efetuado com sucesso! :)');
				setTimeout(function(){
					window.location.href = "estrutura.php";
				}, 2000);
				
			}
		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/LoginController.php', 'post', null, dados, callback); 

	},

	sair: function(){

		var dados = {
			'action' : 'sair'
		}

		var callback = function(json){

			window.location.href = "../adm/";
				
		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/LoginController.php', 'post', null, dados, callback);

	}

}