function Noticias(){

}

Noticias.prototype = {
	
	add: function (dados){

		$('#action').val('add');

		if($('#idImg').val() == '1'){
			if(capa == null){
				capa = array[0].name;
			}
		}

		$('#alert-return-table').empty();

		var dataPub = $('#birthdate').val();
		dataPub = dataPub.split('/');
		dataPub = dataPub[2] + '-' + dataPub[1] + '-' + dataPub[0];

		var data = {
			'action'			: 'add',
			'data'				: dataPub,
			'status'            : $('input[name="estado"]:checked').val(),
			'titulo'			: $('#titulo').val(),
			'subtitulo'			: $('#sub-titulo').val(),
			'descricao'			: $('#descricao').val(),
			'video'				: $('#video-noticia').val(),
			'img'				: array,
			'capa'				: capa
		}	

		if(arrayObjects['Validacoes'].isYoutubeVideoNoticia(data.video) == true){
			data.video = data.video.replace('watch?v=', 'embed/');
			data.video = data.video.replace('v/', 'embed/');	
			data.video = data.video.replace('https://youtu.be/', 'https://www.youtube.com/embed/');
		}else{
			arrayObjects['Validacoes'].alerts('alert alert-danger', 'Deve-se colocar um link de video correto!');
			return;
		}


		if($('#idImg').attr('value') != 1){
			arrayObjects['Validacoes'].alerts('alert alert-danger', 'Deve-se selecionar pelo menos uma imagem para a Notícia!');
			return;
		}

		var callback = function(json){

			console.log(json);

			var altura = $('#conteudo').height();
			$('body').animate({scrollTop: altura}, 'slow');

			arrayObjects['Validacoes'].alerts('alert alert-success', 'Notícia cadastrada com sucesso! \n\
				<img src="assets/img/loading-verde.svg" style="width:50px">');


			setTimeout(function(){
				modifyScreen('noticias', 'Noticias');
			}, 3000);

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/NoticiaController.php', 'post', null, data, callback);

	},


	update: function(dados){

		if($('#idImg').val() == '1'){
			if(capa == null){
				capa = array[0].name;
			}
		}

		var dataPub = $('#birthdate').val();
		dataPub = dataPub.split('/');
		dataPub = dataPub[2] + '-' + dataPub[1] + '-' + dataPub[0];

		$('#action').val('update');

		var data = {
			'action'			: 'update',
			'id'				: $('#id').val(),
			'data'				: dataPub,
			'status'            : $('input[name="estado"]:checked').val(),
			'titulo'			: $('#titulo').val(),
			'subtitulo'			: $('#sub-titulo').val(),
			'descricao'			: $('#descricao').val(),
			'video'				: $('#video-noticia').val(),
			'img'				: array,
			'capa'				: capa
		}

		if(arrayObjects['Validacoes'].isYoutubeVideoNoticia(data.video) == true){
			data.video = data.video.replace('watch?v=', 'embed/');
			data.video = data.video.replace('v/', 'embed/');	
			data.video = data.video.replace('https://youtu.be/', 'https://www.youtube.com/embed/');
		}else{
			arrayObjects['Validacoes'].alerts('alert alert-danger', 'Deve-se colocar um link de video correto!');
			return;
		}

		if($('#idImg').attr('value') != 1){
			arrayObjects['Validacoes'].alerts('alert alert-danger', 'Deve-se selecionar pelo menos uma imagem para a Notícia!');
			return;
		}

		var callback = function(json){

			console.log(json);

			var altura = $('#conteudo').height();
			$('body').animate({scrollTop: altura}, 'slow');

			arrayObjects['Validacoes'].alerts('alert alert-success', 'Notícia alterada com sucesso!\n\
				<img src="assets/img/loading-verde.svg" style="width:50px">');

			
			setTimeout(function(){
				modifyScreen('noticias', 'Noticias');
			}, 3000);

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/NoticiaController.php', 'post', null, data, callback);
	},


	showUpdate: function(id){

		var dados = {
			'action' : 'returnInfoId',
			'id'     : $(id).parents().parents().attr('id')
		}

		var dados2 = {
			'action' : 'returnImg',
			'id'     : $(id).parents().parents().attr('id')
		}
		

		var callback = function(json){

			$('#conteudo').html(json);
			$('#id').val($(id).parents().parents().attr('id'));
			
			var callback2 = function(dados){

				$('#loading').show();

					var dataPub = dados[0].data;
					dataPub = dataPub.split('-');
					dataPub = dataPub[2] + '/' + dataPub[1] + '/' + dataPub[0];
					
					$('#birthdate').val(dataPub);
					$('#titulo').val(dados[0].titulo);
					$('#sub-titulo').val(dados[0].subtitulo);
					$('#video-noticia').val(dados[0].video);
					$('#descricao').val(dados[0].descricao);
			}

			var callback3 = function(data){
				console.log(data);
				
				for(var i in data){

					if(data[i].capa == 1){
						capa = data[i].img;
					}

					var dados = {
						'name' : data[i].img
					}

					array.push(dados);

					if(data[i].capa == '1'){
						var img = '<img src="../adm/assets/upload/'+data[i].img+'" alt="Imagem" class="foto-capa">';
					} else {
						var img = '<img src="../adm/assets/upload/'+data[i].img+'" alt="Imagem">';
					}

					$('#preview').append('\n\
						<div id="'+data[i].img+'" class="hover-img">'+	
							'<input type="hidden" id="idImg" value="1">'+					
							''+img+'\n\
							<div class="retina">\n\
								<div class="acoes-img esquerda">\n\
									<span class="glyphicon glyphicon-bookmark color-azul-c tam-icone" aria-hidden="true"'+ 
									'onClick="arrayObjects[\'Produtos\'].capaImg(this)"></span>\n\
								</div>\n\
								<div class="acoes-img direita">\n\
									<span class="glyphicon glyphicon-trash color-vermelho tam-icone" aria-hidden="true"'+
									'onClick="arrayObjects[\'Produtos\'].removeImg(this)"></span>\n\
								</div>\n\
							</div>\n\
						</div>\n\
					');

				}

			}

			$('.btn-ok').attr('onClick', 'update(\'Noticias\', \'form-noticia\')');

			arrayObjects['Requisicoes'].ajax('mvc/controller/NoticiaController.php', 'post', 'post', dados, callback2);
			arrayObjects['Requisicoes'].ajax('mvc/controller/NoticiaController.php', 'post', 'post', dados2, callback3);
		}

		arrayObjects['Requisicoes'].ajax('cadastro-noticia.php', 'post', null, null, callback);
	},


	listar: function(){

		var dados = {
			'action' : 'listar'
		}

		var callback = function(json){
			
			if(json.length > 0){
				// console.log(json);

				for(var i in json){

					if(json[i].status == '1'){
						var status = '<input style="margin-top:20px" id="status" type="checkbox"'+
						'onClick="arrayObjects[\'Noticias\'].actionTable(\'status\', this)" checked>';
					} else {
						var status = '<input style="margin-top:20px" id="status" type="checkbox"'+
						'onClick="arrayObjects[\'Noticias\'].actionTable(\'status\', this)">';
					}

					$('tbody').append('\n\
					<tr id="'+json[i].id+'">\n\
					  	<td class="" id="td-nome-produto"><img style="width:60px; padding-right:10px" src="../adm/assets/upload/'+json[i].img+'">\n\
					  	<a class="color-azul" href="#">'+json[i].titulo+'</a></td>\n\
					  	\n\
					  	<td class="">'+status+'</td>\n\
					  	<td class="">\n\
					  	<a class="btn btn-default" href="#" role="button" style="margin-top:10px" onClick="load(\'Noticias\', \'update\', this)">\n\
					  	<span class="glyphicon glyphicon-edit color-azul-c" aria-hidden="true"></span> Editar</a>\n\
					  	<a class="btn btn-default" href="#" role="button" style="margin-top:10px"'+ 
					  	'onClick="showDialog(\'modalRemover\', \'show\', '+json[i].id+')">\n\
					  	<span class="glyphicon glyphicon-trash color-vermelho" aria-hidden="true"></span> Excluir</a></td>\n\
					</tr>\n\
					');

				}

			} else {
				$('#alert-return-table').html('<p class="alert alert-info" style="text-align: center">Não existe nenhuma notícia cadastrada! :(</p>');
			}

			capa = null;
			array = [];

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/NoticiaController.php', 'post', 'json', dados, callback);

	},


	actionTable: function(action, value){

		var aux = null;

		if(action == 'status'){

			if($(value).prop('checked') == true){
				
				$(value).parents().parents().find('#destaque').attr('disabled', false);
				aux = 1;

			} else {

				$(value).parents().parents().find('#destaque').attr('disabled', true);
				aux = 0;

			}

		}

		var dados = {
			'action'   : 'updateTable',
			'function' : action,
			'value'    : aux,
			'id'       : $(value).parents().parents().attr('id')
		}

		var callback = function(json){
			console.log(json);
			arrayObjects['Validacoes'].alerts('alert alert-success', 'Notícia atualizada com sucesso! :)');
		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/NoticiaController.php', 'post', null, dados, callback);
	},


	uploadImg: function(value){

		$('#form-noticia').ajaxForm({

			url: 'mvc/controller/UploadController.php',
			type: 'post',
			data: $('#form-noticia').serialize(),
			dataType: 'json',
			beforeSend: function(){
				$('#preview').append('<img id="campo-gif" value="1" style="width:80px; height:80px" src="assets/img/loading-verde.svg" />');
			},
			success: function(json){

				setTimeout(function(){

					$('#campo-gif').remove();
				
					if(json.length > 0){

						for(var i in json){

							var dados = {
								'name' : json[i].name
							}

							array.push(dados);

							$('#preview').append('\n\
								<div id="'+json[i].name+'" class="hover-img">'+	
									'<input type="hidden" id="idImg" value="1">'+					
									'<img src="../adm/assets/upload/'+json[i].name+'" alt="Imagem">\n\
									<div class="retina">\n\
										<div class="acoes-img esquerda">\n\
										<span class="glyphicon glyphicon-bookmark color-azul-c tam-icone" aria-hidden="true"'+ 
										'onClick="arrayObjects[\'Produtos\'].capaImg(this)"></span>\n\
										</div>\n\
										<div class="acoes-img direita">\n\
											<span class="glyphicon glyphicon-trash color-vermelho tam-icone" aria-hidden="true"'+
											'onClick="arrayObjects[\'Produtos\'].removeImg(this)"></span>\n\
										</div>\n\
									</div>\n\
								</div>\n\
							');
								
						}

					}

					$('#loading').hide();

				}, 2000);

			}

		}).submit();

		$('#upload').val('');

	},


	capaImg: function(value){

		$('img').removeClass('foto-capa');

		capa = $(value).parent().parent().parent().attr('id');
		$(value).parent().parent().parent().find('img').addClass('foto-capa');

		var altura = $('#conteudo').height();
		$('body').animate({scrollTop: altura}, 'slow');

		arrayObjects['Validacoes'].alerts('alert alert-success', 'Capa definida com sucesso! :)');

	},

	removeImg: function(value){

		var aux = $(value).parent().parent().parent().attr('id');

		for(var i = 0; i < array.length; i++){
			if(array[i].name == aux){
				array.splice(i, 1);
				$(value).parent().parent().parent().remove();	
			}
		} 

	},

	remove: function(id){

		var dados = {
			'id' 	 : id,
			'action' : 'remove'
		}
		console.log(id);
		var callback = function(json){
			console.log(json);
			showDialog('modalRemover', 'hide');
			$('#id').val('');

			if(json == 'erro'){
				arrayObjects['Validacoes'].alerts('alert alert-danger', 'Notícia vinculada com outros registros. Verifique! :(');
				return;
			}

			$('#' + id).remove();

			arrayObjects['Validacoes'].alerts('alert alert-success', 'Notícia removida com sucesso! :)');

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/NoticiaController.php', 'post', null, dados, callback);

	},

}