<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="format-detection" content="telephone=no">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>IMAK INDUSTRIAL - PAGINA EM CONSTRUÇÃO </title>
	<link type="image/ico" rel="icon" href="assets/imagens/favicon2.ico">
	<link rel="stylesheet" href="assets/css/normalize.css">
	<link rel="stylesheet" href="assets/css/foundation.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	

<!-- IMG TOPO -->
<div class="row">	
	<br>
	<div class="logo">
		<img  class="float-center" src="assets/img/logo-pagina-construção.png">
	</div>

	<hr class="float-center linha show-for-large" style="width:700px;">
	<hr class="float-center linha hide-for-large" style="width:100%;">

	<br>

	<h2 class="color-laranja text-center titillium_bdbold">SITE EM CONSTRUÇÃO</h2>
	<h5 class="color-cinza-forte text-center titillium_regular">Enquanto isso entre em contato com a gente!</h5>

</div>

<br><br>

<div class="row">
	<div class="large-4 columns">
		<div class="info text-center">
			<img class="float-center" src="assets/img/ic2-contato.png">
			<h6 class="color-cinza-forte titillium_regular">Rua dos Farrapos, 777 - Bairro Central Santa Rosa, RS - Brasil</h6>
		</div>		
	</div>

	<div class="large-4 columns">
		<div class="info text-center">
			<img class="float-center" src="assets/img/ic1-contato.png">
			<h6 class="color-cinza-forte titillium_regular">comercial@imakindustrial.com.br</h6>
		</div>		
	</div>

	<div class="large-4 columns">
		<div class="info text-center">
			<img class="float-center" src="assets/img/ic3-contato.png">
			<h6 class="color-cinza-forte titillium_regular">55 <strong>3511 4112</strong> - 55 <strong>9948 7343</strong></h6>
		</div>		
	</div>
</div>


<br><br>


<div class="row"><h5 class="color-laranja text-center titillium_bdbold">SIGA-NOS NAS REDES SOCIAIS:</h5></div>


<div class="row small-up-1 medium-up-1 large-up-2">
	
	<div class="column">
			
			<a href="https://www.facebook.com/imakindustrial/?fref=ts" target="blank"><img class="float-right show-for-large" src="assets/img/ic-facebook.png"></a>

			<a href="https://www.youtube.com/channel/UC4bDE5F1ILg425z9CY6KzSg" target="blank"><img class="float-center hide-for-large" src="assets/img/ic-youtube.png"></a>
	
	</div>

	<div class="column">

			<a href="https://www.youtube.com/channel/UC4bDE5F1ILg425z9CY6KzSg" target="blank"><img class="show-for-large" src="assets/img/ic-youtube.png"></a>

			<a href="https://www.youtube.com/channel/UC4bDE5F1ILg425z9CY6KzSg" target="blank"><img class=" float-center hide-for-large" src="assets/img/ic-youtube.png"></a>
		
	</div>
</div>


<br><br>

<!-- VIDEOS -->
<div class="row small-up-1 medium-up-2 large-up-3">
	
	<div class="column">
			
		<iframe width="100%" height="200" src="https://www.youtube.com/embed/48B8rQdxQcc" frameborder="0" allowfullscreen></iframe>
	
	</div>

	<div class="column">

		<iframe width="100%" height="200" src="https://www.youtube.com/embed/ROU3Q5kpVew" frameborder="0" allowfullscreen></iframe>
		
	</div>

		<div class="column">

			<iframe width="100%" height="200" src="https://www.youtube.com/embed/izTHAqsoG7o" frameborder="0" allowfullscreen></iframe>
		
	</div>

</div>

<br><br>



</body>
</html>
	
