<?php

class DbConnection {


	private $host;
	private $db;
	private $user;
	private $password;
	private $instance;

	public function createConnection(){

		try{
			if (!isset(self::$instance)) { 

				$host = 'localhost';
				$db = 'maqpesar_db';
				$user = 'maqpesar_root';
				$password = 'maqpesar_root';
				// site.maqpesa@2019
				
				$this->instance = new PDO('mysql:host='.$host.';dbname='.$db, $user, $password,
					array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				$this->instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
				$this->instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING); 
			}

			return $this->instance;

		} catch(Exception $e){
			echo $e->getMessage();
		}

	}

}

// $host = 'localhost';
// $db = 'db_imak';
// $user = 'root';
// $password = '';

// $host = 'mysql.elede.com.br';
// $db = 'elede05';
// $user = 'elede05';
// $password = 'ld2016';

// Configuração banco www.imakindustrial.com.br
// $host = 'mysql.imakindustrial.com.br';
// $db = 'imakindustrial';
// $user = 'imakindustrial';
// $password = 'radafUru2r';