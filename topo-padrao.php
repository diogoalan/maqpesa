<!DOCTYPE html>
<html>
<head>
	<meta name="theme-color" content="#E6B11F">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="format-detection" content="telephone=no">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>MAQPESA - Indústria de Máquinas Rodoviárias</title>
	<link type="image/ico" rel="icon" href="assets/img/fav-icon.png">
	<link rel="stylesheet" href="assets/css/normalize.css">
	<link rel="stylesheet" href="assets/css/foundation.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/animate.css">
	<link rel="stylesheet" href="assets/css/slider-pro.css">
	<link rel="stylesheet" href="assets/css/examples.css">
</head>
<body>

	<!-- URL AMIGAVEL -->
	<input type="hidden" id="urlSite" value="http://imak.elede.com.br">


<!-- IMG TOPO -->
<div class="container-full fundo-topo bg-branco">

	<!-- DESKTOP -->
	<div class="row" >
		<div class="column large-4 show-for-large">	
			<a href="index.php"><img class="float-left img-logo2" src="assets/img/logo999.png" width="300"></a>
		</div>


		<!-- MOBILE -->
		<div class="column hide-for-large">	
			<a href="index.php"><img class="float-left img-logo" src="assets/img/logomobilecurto.png" width="70" height="50"></a>

			<!-- MENU MOBILE -->
				<div id="myNav" class="overlay">
				  	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
				  	<div class="overlay-content">
					    <a href="index.php">PÁGINA INÍCIAL</a>
					    <a href="empresa.php">A MAQPESA</a>
					    <a href="produtos.php">PRODUTOS</a>
					    <a href="noticias.php">NOTÍCIAS</a>
					    <a href="contato.php">CONTATO</a>
				  	</div>
				</div>

				<span onclick="openNav()"><div class="menu-mobile float-right"></div></span>
		</div>

	
		<!-- MENU DESKTOP -->
		<div class="column large-8">
			<div class="show-for-large float-right">
  				<ul id="menuTopo" class="menu-desktop">

			        <a href="index.php"><li class="itens-menu2">PÁGINA INICIAL</li></a>
			        <a href="empresa.php"><li class="itens-menu2">A MAQPESA</li></a>
			        
			        	<!-- <a href="produtos.php"> -->
			        		<li class="itens-menu2">
			        			<div class="dropdown">	
				        			
				        			<a href="produtos.php" class="dropbtn">PRODUTOS</a>
				        			
				        			<!--<div id="all-categoria" class="dropdown-content">
								    	<a href="noticias.php">INDUSTRIAL</a>
								    	<a href="noticias.php">AGRICOLA</a>
								    	<a href="noticias.php">COMERCIAL</a> 
	  								</div>-->
	  							</div>
			        		</li>
			        	<!-- </a> -->
			        		
			        <a href="noticias.php"><li class="itens-menu2">NOTÍCIAS</li></a>
			        <a href="contato.php"><li class="itens-menu2">CONTATO</li></a>

     			</ul>
	  		</div>
		</div>
	</div>

</div>

<!-- LINHA LARANJA -->
<div class="bg-laranja" style="height: 4px;"></div>
