<?php 

error_reporting(E_ERROR);

require '../loadClass.php';

function add(){

	$noticiaModel = new Noticias();
	$noticiaModel->setStatus($_POST['status']);
	$noticiaModel->setTitulo($_POST['titulo']);
	$noticiaModel->setDescricao($_POST['descricao']);
	$noticiaModel->setSubtitulo($_POST['subtitulo']);
	$noticiaModel->setVideo($_POST['video']);
	$noticiaModel->setData($_POST['data']);

	var_dump($_POST);

	$noticia = new NoticiaDao();
	$noticia->insertNoticia($noticiaModel);
	$aux = $noticia->lastId();

	foreach($aux as $c){
		$aux = $c['noticia_id'];
	}

	$uploadModel = new Uploads();
	$upload = new UploadDao();

	for($i = 0; $i < count($_POST['img']); $i++){

		if($_POST['capa'] == $_POST['img'][$i]['name']){
			$capa = 1;
		} else {
			$capa = 0;
		}

		$uploadModel->setImg($_POST['img'][$i]['name']);
		$uploadModel->setCapa($capa);
		$uploadModel->setNoticiaId($aux);
		$upload->insertUpload($uploadModel);
	}
	echo 'true';
}

function update(){

	$noticiaModel = new Noticias();
	$noticiaModel->setId($_POST['id']);
	$noticiaModel->setStatus($_POST['status']);
	$noticiaModel->setTitulo($_POST['titulo']);
	$noticiaModel->setDescricao($_POST['descricao']);
	$noticiaModel->setSubtitulo($_POST['subtitulo']);
	$noticiaModel->setVideo($_POST['video']);
	$noticiaModel->setData($_POST['data']);

	var_dump($_POST);

	$noticia = new NoticiaDao();
	$noticia->updateNoticia($noticiaModel);

	

	// echo json_encode($noticiaModel->getId());

	

	$uploadModel = new Uploads();
	$uploadModel->setNoticiaId($_POST['id']);

	$upload = new UploadDao();
	$upload->removeUploadNoticia($uploadModel);

	for($i = 0; $i < count($_POST['img']); $i++){

		if($_POST['capa'] == $_POST['img'][$i]['name']){
			$capa = 1;
		} else {
			$capa = 0;
		}

		$uploadModel->setImg($_POST['img'][$i]['name']);
		$uploadModel->setCapa($capa);
		$uploadModel->setNoticiaId($_POST['id']);
		$upload->insertUpload($uploadModel);
	}

	echo 'true';

}

function updateTable(){

	if($_POST['function'] == 'status'){

		$noticiaModel = new Noticias();
		$noticiaModel->setId($_POST['id']);
		$noticiaModel->setStatus($_POST['value']);

		$noticia = new NoticiaDao();
		$noticia->updateStatus($noticiaModel);
	}

}

function remove(){


	if(count($aux) > 0){
		echo json_encode('false');
	} else {		
		
		$imagemModel = new Uploads();
		$imagemModel->setNoticiaId($_POST['id']);

		//Imagem Noticia		
		$upload = new UploadDao();
		$images = $upload->selectAllUploadNoticia($imagemModel);

		foreach ($images as $c) {
			$pasta = '../../assets/upload/' . $c['upload_img'];
			unlink($pasta);
		}

		$upload->removeUploadNoticia($imagemModel);

		//Noticia
		$noticiaModel = new Noticias();
		$noticiaModel->setId($_POST['id']);

		$noticia = new NoticiaDao();
		$noticia->removeNoticia($noticiaModel);


		echo json_encode('true');
	}

}

function selectNoticiaId(){

	$noticiaModel = new Noticias();
	$noticiaModel->setId($_POST['id']);
	
	$noticias = new NoticiaDao();
	$noticias = $noticias->selectNoticiaId($noticiaModel);
	$return = array();
	$array = array();

	foreach ($noticias as $c) {
		$array['id'] = $c['noticia_id'];
		$array['status'] = $c['noticia_status'];
		$array['titulo'] = $c['noticia_titulo'];
		$array['subtitulo'] = $c['noticia_subtitulo'];
		$array['descricao'] = $c['noticia_descricao'];
		$array['video'] = $c['noticia_video'];
		$array['data'] = $c['noticia_data'];
		array_push($return, $array);
	}

	$json = json_encode($return);
	
	echo $json;
}

// aqui
function selectUploadId(){

	$uploadModel = new Uploads();
	$uploadModel->setNoticiaId($_POST['id']);

	$upload = new UploadDao();
	$upload = $upload->selectAllUploadNoticia($uploadModel);
	
	$return = array();
	$array = array();

	foreach ($upload as $c) {
		$array['img'] = $c['upload_img'];
		$array['capa'] = $c['upload_capa'];
		array_push($return, $array);
	}

	$json = json_encode($return);
	
	echo $json;

}

function selectFromNoticias(){

	$noticia = new NoticiaDao();
	$noticia = $noticia->selectFromNoticias();
	$return = array();
	$array = array();

	foreach($noticia as $c){
		
		$array['id'] = $c['noticia_id'];
		$array['status'] = $c['noticia_status'];
		$array['titulo'] = $c['noticia_titulo'];
		$array['subtitulo'] = $c['noticia_subtitulo'];
		$array['descricao'] = $c['noticia_descricao'];
		$array['video'] = $c['noticia_video'];
		$array['data'] = $c['noticia_data'];
		$array['img'] = $c['upload_img'];
		$array['capa'] = $c['upload_capa'];
		array_push($return, $array);
	}

	$json = json_encode($return);
	echo $json;

}

function listar(){

	$noticias = new NoticiaDao();
	$noticias = $noticias->selectAllNoticias();

	$return = array();
	$array = array();

	foreach ($noticias as $c) {
		$array['id'] = $c['noticia_id'];
		$array['status'] = $c['noticia_status'];
		$array['titulo'] = $c['noticia_titulo'];
		$array['subtitulo'] = $c['noticia_subtitulo'];
		$array['descricao'] = $c['noticia_descricao'];
		$array['video'] = $c['noticia_video'];
		$array['data'] = $c['noticia_data'];
		$array['img'] = $c['upload_img'];
		array_push($return, $array);
	}

	$json = json_encode($return);
	
	echo $json;

}

switch ($_POST['action']) {
	case 'add':
		add();
	break;

	case 'update':
		update();
	break;

	case 'updateTable':
		updateTable();
	break;

	case 'remove':
		remove();
	break;

	case 'returnInfoId':
		selectNoticiaId();
	break;

	case 'returnImg':
		selectUploadId();
	break;

	case 'selectFromNoticias':
		selectFromNoticias();
	break;

	default:
		listar();
	break;
}