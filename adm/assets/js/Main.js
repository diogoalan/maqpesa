var arrayObjects = [];
var array = [];
var capa = null;
var actionClick = null;

startObj();

$(document).ready(function(e){
 
	$('#alerts').hide();

	$('#loading').hide();

	//Ações Enter no Sistema
	$('body').on('keypress',function(e){
		if(e.keyCode != 13) return;

		// CLICK ENTER LOGIN
		if($(this).find('#form-login').length >= 1){
			login('Login',  'form-login');		
			return;
		}

		// CADASTRO OPCIONAIS
		if($(this).find('#form-produto').length >= 1){
			arrayObjects['Produtos'].newOpcional('opcionais', 'form-produto');
			// console.log('usadashd');
			return;
		}

		if($(this).find('#bg-add').length == 1){

			actionClick = $('.btn-ok').attr('onclick');

			if(actionClick == "add('Categorias', 'Categorias')"){
				add('Categorias', 'Categorias');
				return;
			}

			if(actionClick == "update('Categorias', 'Categorias')"){
				update('Categorias', 'Categorias');
				return;
			}

		}
	});	

	var url = window.location.href.substr(window.location.href.lastIndexOf("/")+1);

	switch(url){

		case '':
			// $('#conteudo').hide();
			// arrayObjects['Site'].listarCategoria();
			arrayObjects['Site'].listarNoticia();
			arrayObjects['Site'].listarProduto('index.php');
		break;

		case 'index.php':
			// $('#conteudo').hide();
			arrayObjects['Site'].listarNoticia();
			arrayObjects['Site'].listarProduto('index.php');
		break;

		case 'produtos.php':
			// $('#conteudo').hide();
			// arrayObjects['Site'].listarCategoria();
			arrayObjects['Site'].listarProduto('produtos.php');
		break;

		case 'noticias.php':
			// $('#conteudo').hide();
			// arrayObjects['Site'].listarNoticiaIndividual();
			arrayObjects['Site'].listarNoticia('noticias.php');
		break;

		case 'estrutura.php#':
			arrayObjects['Dashboard'].listar();
			arrayObjects['Dashboard'].cont();
		break;

		case 'estrutura.php':
			arrayObjects['Dashboard'].listar();
			arrayObjects['Dashboard'].cont();
		break;

		default:

		if(url.indexOf('produtos.php?imak=') != -1)
			arrayObjects['Site'].listarProduto();

		// var url = window.location.href;
		// if(url.indexOf('produto/') != -1){
		// 	arrayObjects['Site'].listarCategoria();
			
		// 	arrayObjects['Site'].listaProdutoIndividual();
		// } else {
		// 	arrayObjects['Site'].listarCategoria();
			
		// 	arrayObjects['Site'].listarProduto('produtos');
		// }
		break;
	}

});
// function enterOpcioanis(){

// }
function startObj(){

	try{
		arrayObjects['Site'] = new Site();
		arrayObjects['Login'] = new Login();
		arrayObjects['Requisicoes'] = new Requisicoes();
		arrayObjects['Validacoes'] = new Validacoes();
		arrayObjects['Categorias'] = new Categorias();
		arrayObjects['Produtos'] = new Produtos();
		arrayObjects['Noticias'] = new Noticias();
		arrayObjects['Dashboard'] = new Dashboard();

	} catch(e){

		console.log('Obj Undefined - '+e);

	}

}

function login(obj, idForm){	

	if(!arrayObjects['Validacoes'].camposEmBranco(idForm)) {
		return;
	}

	arrayObjects[obj].login(formData(idForm));

}

function modifyClass(nameClass, newClass, action){


	if(action == 'remove'){
		$('.' + nameClass).removeClass(newClass);
		$('.' + nameClass).css('border', '1px solid #CCC');

	}
}

function showHide(id, action){

	if(id == 'add-produtos'){

		if(action == 'show'){

			var callback = function(json){
				$('#conteudo').html(json);

				arrayObjects['Produtos'].listarCategoria();
			}

			arrayObjects['Requisicoes'].ajax('cadastro-produto.php', 'post', null, null, callback);

		}

		if(action == 'hide'){

			var callback = function(json){
				$('#conteudo').html(json);
			}

			arrayObjects['Requisicoes'].ajax('produtos.php', 'post', null, null, callback);
			
		}

	}

	if(id == 'add-noticias'){

		if(action == 'show'){

			var callback = function(json){
				$('#conteudo').html(json);

				arrayObjects['Produtos'].listarCategoria();
			}

			arrayObjects['Requisicoes'].ajax('cadastro-noticia.php', 'post', null, null, callback);

		}

		if(action == 'hide'){

			var callback = function(json){
				$('#conteudo').html(json);
			}

			arrayObjects['Requisicoes'].ajax('produtos.php', 'post', null, null, callback);
			
		}

	}

	if(action == 'show'){

		$('#' + id).show();

	}

	if(action == 'hide'){

		$('#' + id).hide();

		$('form').each(function(index, el){
			$(this).find('input').val('');
		});
	}

}

function add(obj, idForm, e){

	if(arrayObjects['Validacoes'].camposEmBranco(idForm) == false){
		return;
	}

	if(obj == 'Site'){

		if(idForm == 'form-orcamento-mobile'){
			if(!arrayObjects['Validacoes'].email('email2')) {
				return
			}
		} else {
			if(!arrayObjects['Validacoes'].email('email')) {
				return
			}
		}
	}

	arrayObjects[obj].add(formData(idForm));

}


function update(obj, idForm){
	console.log(arrayObjects['Produtos'].opcionais);
	if(!arrayObjects['Validacoes'].camposEmBranco(idForm)){
		return;
	}

	arrayObjects[obj].update(formData(idForm));

}

function remover(obj, id){

	arrayObjects[obj].remove($('#id').val());

}

function showDialog(id, action, value){

	if(value > 0){
		$('#id').val(value);
	}

	if(action == 'show'){
		$('#' + id).modal('show');
	}

	if(action == 'hide'){
		$('#' + id).modal('hide');
	}

}

function formData(id){

	var dados = $('#' + id).serialize();
	return dados;

}

function removeAtivo(){
	$('div').each(function(index, el){
		if($(this).attr('class') == 'row menu-ativo'){
			$(this).removeClass('row menu-ativo');
			$(this).addClass('row menu');
		}
	});
}

function controllScreen(url){

	switch(url){
		case 'dashboard':
		removeAtivo();
		$('#dashboard').removeClass('row menu');
		$('#dashboard').addClass('row menu-ativo');
		modifyScreen(url, 'Dashboard');
		break;
		case 'produtos':
		removeAtivo();
		$('#produtos').removeClass('row menu');
		$('#produtos').addClass('row menu-ativo');
		modifyScreen(url, 'Produtos');
		break;
		case 'categorias':
		removeAtivo();
		$('#categorias').removeClass('row menu');
		$('#categorias').addClass('row menu-ativo');
		modifyScreen(url, 'Categorias');
		break;
		case 'noticias':
		removeAtivo();
		$('#noticias').removeClass('row menu');
		$('#noticias').addClass('row menu-ativo');
		modifyScreen(url, 'Noticias');
		break;
	}
}

function modifyScreen(urlReq, obj){

	var dados = null;

	var callback = function(json){

		$('#conteudo').html(json);
		$('#bg-add').hide();
		arrayObjects[obj].listar();
		arrayObjects['Dashboard'].cont();

	}

	var url = urlReq + '.php';

	arrayObjects['Requisicoes'].ajax(url, 'post', null, dados, callback);

}

function load(obj, action, value){

	if(action == 'update'){
		arrayObjects[obj].showUpdate(value);
	}

	// console.log(arrayObjects['Produtos'].opcionais);
	
	if(action == 'upload'){
		arrayObjects[obj].uploadImg(value);
	}

	if(action == 'img-especificacoes'){
		arrayObjects[obj].uploadImgEspecificacoes(value);
	}

	if(action == 'img-noticia'){
		arrayObjects[obj].uploadImgNoticia(value);
	}

}

function dataAlbum(){

    $('input[name="birthdate"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,	
		
		locale: {
            format: 'DD/MM/YYYY'
        }
    });
}

function sair(){

	arrayObjects['Login'].sair();

}

