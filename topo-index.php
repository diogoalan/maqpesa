<!DOCTYPE html>
<html>
<head>
	<meta name="theme-color" content="#E6B11F">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="format-detection" content="telephone=no">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>MAQPESA - Indústria de Máquinas Rodoviárias</title>
	<link type="image/ico" rel="icon" href="assets/img/fav-icon.png">
	<link rel="stylesheet" href="assets/css/normalize.css">
	<link rel="stylesheet" href="assets/css/foundation.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/animate.css">
	<link rel="stylesheet" href="assets/css/slider-pro.css">
	<link rel="stylesheet" href="assets/css/examples.css">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-62383843-13');
	</script>
</head>
<body>


<!-- IMG TOPO -->
<div class="container-full show-for-large">
	
	<div class="banner-principal" style="height: 550px;">
		
		<div class="row">
			<!-- LOGO -->
			<div class="column large-4">
				<a href="index.php"><img class="float-left img-logo2" src="assets/img/logo999.png" width="300"></a>
			</div>


			<!-- MENU DESKTOP -->
			<div class="column large-8">
					
	  			<div class="show-for-large float-right">
	  				
	  				<ul id="menuTopo" class="menu-desktop">

				        <a href="index.php"><li class="itens-menu">PÁGINA INICIAL</li></a>
				        <a href="empresa.php"><li class="itens-menu">A MAQPESA</li></a>
			        	<a href="produtos.php"><li class="itens-menu">PRODUTOS</li></a>
			        	

			        	<!-- <a href="produtos.php">
			        		<li class="itens-menu">
			        			<div class="dropdown">	
				        			
				        			<a href="produtos.php" class="dropbtn">PRODUTOS</a>
				        			
				        			<div id="all-categoria" class="dropdown-content">
								    	<a href="noticias.php">INDUSTRIAL</a>
								    	<a href="noticias.php">AGRICOLA</a>
								    	<a href="noticias.php">COMERCIAL</a> 
	  								</div>
	  							</div>
			        		</li>
			        	</a> -->
			        		
				        <a href="noticias.php"><li class="itens-menu">NOTÍCIAS</li></a>
				        <a href="contato.php"><li class="itens-menu">CONTATO</li></a>

     				</ul>

	  			</div>
	  			
			</div>

		</div>

		<br><br><br>

		<!-- SLOGAN E BOTAO DESKTOP -->
		<div class="show-for-large">
		  	<br><br><br><br>
			<div class="color-branco text-center">
				<h2 class="titilliumlight">IMPLEMENTANDO NOVAS TECNOLOGIAS, <br> AMPLIANDO HORIZONTES</h2>
			</div>
		</div>	

		<div class="show-for-large">
			<br><br>
			<a href="empresa.php"><div class="btn-veja-mais float-center titillium_bdbold">CONHEÇA A MAQPESA</div></a>	
		</div> 
	</div>
</div>	


<!-- MOBILE -->
<div class="container-full hide-for-large">
	
	<div class="banner-principal-mobile">
		<div class="row">
			<!-- LOGO -->
			<div class="column large-4">
				<a href="index.php"><img class="float-left img-logo" src="assets/img/logomobilecurto.png" width="70" height="50"></a>
	
				<!-- MENU MOBILE -->
				<div id="myNav" class="overlay">
				  	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
				  	<div class="overlay-content">
 						<a href="index.php">PÁGINA INÍCIAL</a>
					    <a href="empresa.php">A MAQPESA</a>
					    <a href="produtos.php">PRODUTOS</a>
					    <a href="noticias.php">NOTÍCIAS</a>
					    <a href="contato.php">CONTATO</a>
				  	</div>
				</div>

				<span onclick="openNav()"><div class="menu-mobile float-right"></div></span>

			</div>
			


			<!-- SLOGAN E BOTAO DESKTOP -->
			<div class="">
			 	<br><br><br><br><br><br>
				<div class="color-branco text-center show-for-medium">
					<h3 class="titillium_regular">IMPLEMENTANDO NOVAS TECNOLOGIAS, <br> AMPLIANDO HORIZONTES</h3>
				</div>

				<div class="color-branco text-center hide-for-medium">
					<h1 class="titillium_regular">IMPLEMENTANDO NOVAS TECNOLOGIAS, <br> AMPLIANDO HORIZONTES</h1>
				</div>
			</div>	

			<div class="">
			<br>
				<a href="empresa.php"><div class="btn-veja-mais float-center titillium_bdbold">CONHEÇA A MAQPESA</div></a>	
			</div> 

		</div>
	</div>

</div>	







<!-- COM BANNER -->






<div class="hide">
<div class="container-full">
	
	<div class="row show-for-large">
		<div class="row" style="heigth: 100px; position: absolute; width: 100%; z-index: 9999999;">
			<!-- LOGO -->
			<div class="column large-4">
				<a href="index.php"><img class="float-left" src="assets/img/logook.png" width="240px"></a>
			</div>

			<!-- MENU DESKTOP -->
			<div class="column large-8">
					
				<div class="show-for-large float-right">
					
					<ul id="menuTopo" class="menu-desktop">

						<a href="index.php"><li class="itens-menu">PÁGINA INICIAL</li></a>
						<a href="empresa.php"><li class="itens-menu">A MAQPESA</li></a>
					
						<!-- <a href="produtos.php"> -->
							<li class="itens-menu">
								<div class="dropdown">	
									
									<a href="produtos.php" class="dropbtn">PRODUTOS</a>
									
									<div id="all-categoria" class="dropdown-content">
										<!-- <a href="noticias.php">INDUSTRIAL</a>
										<a href="noticias.php">AGRICOLA</a>
										<a href="noticias.php">COMERCIAL</a> -->
									</div>
								</div>
							</li>
						<!-- </a> -->
							
						<a href="noticias.php"><li class="itens-menu">NOTÍCIAS</li></a>
						<a href="contato.php"><li class="itens-menu">CONTATO</li></a>

					</ul>

				</div>
				
			</div>
		</div>
	</div>

	<div class="row hide-for-large" style="background-color: #357353;">
		<div class="row" style="heigth: 100px; position: relative; width: 100%; z-index: 9999999; padding-left: 15px;">
			<!-- LOGO -->
			<div class="column large-4">
				<a href="index.php"><img class="float-left img-logo" src="assets/img/logo-topo.png"></a>

				<!-- MENU MOBILE -->
				<div id="myNav" class="overlay">
					<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
					<div class="overlay-content">
						<a href="index.php">PÁGINA INÍCIAL</a>
						<a href="empresa.php">A MAQPESA</a>
						<a href="produtos.php">PRODUTOS</a>
						<a href="noticias.php">NOTÍCIAS</a>
						<a href="contato.php">CONTATO</a>
					</div>
				</div>

				<span onclick="openNav()"><div class="menu-mobile float-right"></div></span>

			</div>
		</div>				
	</div>

	<div id="bannerimages">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>				
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>				
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="assets/img/banner-principal.png" alt="...">
					<div class="carousel-caption"></div>
				</div>
				<div class="item">
					<img src="assets/img/banner2.png" alt="...">
					<div class="carousel-caption"></div>
				</div>
				<div class="item">
					<img src="assets/img/banner3.png" alt="...">
					<div class="carousel-caption"></div>
				</div>			
			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><</span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true">></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
</div>

</div>