<?php 

class ProdutoDao {

	private $dbc;

	public function __construct() {
        $this->dbc = new DbConnection();
        $this->dbc->createConnection();
    }

    public function insertProduto(Produtos $produto){

    	$sql = 'insert into tb_produtos (produto_status, produto_nome, produto_descricao, categoria_id, produto_video, produto_opcional, produto_especificacoes) 
    	values (:status, :nome, :descricao, :categoria, :video, :opcional, :especificacoes)';
    	$aux = $this->dbc->createConnection()->prepare($sql);
    	$aux->bindValue(':status', $produto->getStatus());
    	$aux->bindValue(':nome', $produto->getNome());
    	$aux->bindValue(':descricao', $produto->getDescricao());
    	$aux->bindValue(':categoria', $produto->getCategoriaId());
    	$aux->bindValue(':video', $produto->getVideo());
        $aux->bindValue(':opcional', $produto->getOpcional());
        $aux->bindValue(':especificacoes', $produto->getProdutoEspecificacoes());
    	$aux->execute();
    }

    public function updateProduto(Produtos $produto){

        $sql = 'update tb_produtos set produto_status = :status, produto_nome = :nome, produto_descricao = :descricao, categoria_id = :categoria, produto_video = :video, produto_opcional = :opcional, produto_especificacoes = :especificacoes where produto_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':id', $produto->getId());
        $aux->bindValue(':status', $produto->getStatus());
        $aux->bindValue(':nome', $produto->getNome());
        $aux->bindValue(':descricao', $produto->getDescricao());
        $aux->bindValue(':categoria', $produto->getCategoriaId());
        $aux->bindValue(':video', $produto->getVideo());
        $aux->bindValue(':opcional', $produto->getOpcional());
        $aux->bindValue(':especificacoes', $produto->getProdutoEspecificacoes());
        $aux->execute();


    }

    public function updateStatus(Produtos $produto){

        $sql = 'update tb_produtos set produto_status = :status where produto_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':id', $produto->getId());
        $aux->bindValue(':status', $produto->getStatus());
        $aux->execute();

    }

    public function removeProduto(Produtos $produto){

        $sql = 'delete from tb_produtos where produto_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':id', $produto->getId());
        $aux->execute();

    }

    public function lastId(){

        $sql2 = 'select produto_id from tb_produtos order by produto_id desc limit 1';
        $aux2 = $this->dbc->createConnection()->prepare($sql2);
        $aux2->execute();
        return $aux2->fetchAll();
        
    }

    public function selectAllProdutos(){

        $sql = 'select * from tb_produtos inner join tb_uploads on tb_produtos.produto_id = tb_uploads.produto_id inner join tb_categorias on tb_categorias.categoria_id = tb_produtos.categoria_id where tb_uploads.upload_capa = 1 order by tb_produtos.produto_id desc';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->execute();
        return $aux->fetchAll();

    }

    public function selectProdutoId(Produtos $produto){

        $sql = 'select * from tb_produtos where tb_produtos.produto_id = :id';
        $aux = $this->dbc->createConnection()->prepare($sql);
        $aux->bindValue(':id', $produto->getId());
        $aux->execute();
        return $aux->fetchAll();

    }
}