<?php 

class LoginDao {

	private $dbc;

	public function __construct() {
        $this->dbc = new DbConnection();
        $this->dbc->createConnection();
    }

    public function selectLogin(){

    	$sql = 'select * from tb_logins';
    	$aux = $this->dbc->createConnection()->prepare($sql);
    	$aux->execute();
    	return $aux->fetchAll();

    }

}