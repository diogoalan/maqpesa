<?php

class Produtos{
	/**
	Atributos da classe 
	**/
	private $id;
	private $status;
	private $nome;
	private $descricao;
	private $video;
	private $opcional;
	private $produtoEspecificacoes;
	private $categoriaId;


	/**
	Funções get e set da classe
	**/

	function setId($id){
		$this->id = $id;
	}

	function getId(){
		return $this->id;
	}

	function setStatus($status){
		$this->status = $status;
	}

	function getStatus(){
		return $this->status;
	}

	function setNome($nome){
		$this->nome = $nome;
	}

	function getNome(){
		return $this->nome;
	}

	function setDescricao($descricao){
		$this->descricao = $descricao;
	}

	function getDescricao(){
		return $this->descricao;
	}

	function setVideo($video){
		$this->video = $video;
	}

	function getVideo(){
		return $this->video;
	}

	function setOpcional($opcional){
		$this->opcional = $opcional;
	}

	function getOpcional(){
		return $this->opcional;
	}

	function setProdutoEspecificacoes($produtoEspecificacoes){
		$this->produtoEspecificacoes = $produtoEspecificacoes;
	}

	function getProdutoEspecificacoes(){
		return $this->produtoEspecificacoes;
	}

	function setCategoriaId($categoriaId){
		$this->categoriaId = $categoriaId;
	}

	function getCategoriaId(){
		return $this->categoriaId;
	}

}
