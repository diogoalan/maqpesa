<?php require('topo-padrao.php'); ?>

<div class="row text-center loading">
   	<img src="assets/img/loading-verde.svg" />
</div>

<div id="conteudo">

<!-- BACKGRUPS -->
<div class="row show-for-large">
	<br>
	<div id="navegacaoNoticia" class="columns large-12">	
		<!-- <h6 class="color-verde-claro titillium_regular font12"><a href="index.php" class="hover-brancons">Página Inicial</a>/ <a href="noticias.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">Notícias</strong></a>/ <a href="index.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">Nome_Noticia</strong></a> </h6> -->
    </div>
    <br>
</div>




<!-- SECTION NOTICIAS INTERNO -->
<section class="noticias-interno">
	<div id="noticiaIndividual">	
		<br><br>
		<!-- TITULO SUBTITULO -->
		<!-- <div class="row">
			<div class="column large-12">
				<h3 class="color-laranja titillium_bdbold">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an</h3>
				<br>
				<h5 class="color-verde-escuro titillium_regular"> Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</h5>
			</div>
		</div>

		<br> -->

		<!-- TEXTO NOTICIA -->
		<!-- <div class="row">
			<div class="column large-12">
				<p class="titillium_regular color-cinza-forte">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				<br><br>
				Why do we use it?
				It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
				<br><br>
				Why do we use it?
				It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).

				</p>
			</div>
		</div>


		<br><br><br> -->

	</div>


	
		<!-- IMG DA NOTICIA -->
		<div id="imagensNoticia" class="row">
			<!-- <div class="column large-12">
				<div class="">
					<img class="float-center" style="width:700px; height:350px;" src="assets/img/2016-04-08_16-05-05_milho.jpg">
				</div>
			</div> -->
		</div>


		<!-- <br><br><br> -->


		<!-- VIDEO DA NOTICIA -->
	<div id="videoNoticia" class="row">
		<!-- <div class="column large-12">
			<div class="video text-center show-for-large">
				<iframe width="515" height="315" src="https://www.youtube.com/embed/izTHAqsoG7o" frameborder="0" allowfullscreen></iframe>
			</div>

			<div class="video text-center hide-for-large">
				<iframe width="100%" height="315" src="https://www.youtube.com/embed/izTHAqsoG7o" frameborder="0" allowfullscreen></iframe>
			</div>
		</div> -->
	</div>
	<br><br>
</section>





<!-- SECTION LEIA TAMBEM -->
<section class="leiaTambém">
	<br><br>
	<!-- LINHA SEPARAÇÂO -->
	<div class="row">
		<div class="column">
			<hr class="show-for-large linha-verde">
			<hr style="width:100%;" class="linha-noticia hide-for-large">
		</div>
	</div>
	
	<br>
	
	<div class="row">
		<div class="column">	
			<h3 class="color-laranja text-center show-for-large titillium_bdbold">LEIA TAMBÉM</h3>
			<h1 class="color-laranja text-center hide-for-large titillium_bdbold">LEIA TAMBÉM</h1>
			<br><br>
		</div>

	</div>
	
	
	<!-- Noticia LEIA MAIS DESKTOP -->
	<div id="noticiaDestaqueDesktop" class="row show-for-large">
		<!-- <div class="large-4 columns">		
			
			<div class="img-noticia-destaque">
				<a href="noticias-interno.php"><img class="float-center" src="assets/img/img-noticias-pagina-inicial.png"></a>
			</div>
			<br><br>
		</div>
			
		<div class="large-8 columns">		
			<div class="box-noticia">
				<h6 class="color-laranja titillium_regular font14"><i>10/08/16</i></h6>

				<h5 class="titillium_bdbold"><a class="color-verde-claro" href="noticias-interno.php"> Navegue também pelas nossas notícias</a></h5>

				<p class="color-cinza-forte titillium_regular font12">Navegue também pelas nossas notícias, e fique por dentro das novidadesNavegue também pelas nossas notícias notícias</p>

				<br>

				<a class="show-for-large" href="noticias-interno.php"><div class="veja-mais-noticias">VEJA MAIS</div></a>

				<a class="hide-for-large" href="noticias-interno.php"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>
			</div>

		</div> -->

	</div>




	<!-- Noticia LEIA MAIS MOBILE -->
	<div id="noticiaDestaqueMobile" class="row hide-for-large">
		<!-- <div class="column">		

			<a href="noticias-interno.php"><img class="float-center" src="assets/img/img-noticias-pagina-inicial.png"></a>
		
			<br>

			<div class="box-noticia float-center">
				<h6 class="color-laranja titillium_regular font14 text-center"><i>10/08/16</i></h6>
				<h5 class="titillium_bdbold text-center"><a class="color-verde-claro" href="noticias-interno.php"> Navegue também pelas nossas notícias, e fique por blablablalbla blabla bla bla</a></h5>
				
				<p class="color-cinza-forte titillium_regular font12">Navegue também pelas nossas notícias, e fique por dentro das novidadesNavegue também pelas nossas notícias notícias</p>

				<br>

				<a class="show-for-large" href="noticias-interno.php"><div class="veja-mais-noticias">VEJA MAIS</div></a>

				<a class="hide-for-large" href="noticias-interno.php"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>

			</div>
			<br><br>
		</div> -->
	</div>
	<br><br><br>
</section>

<?php require('rodape.php'); ?>

<script>
arrayObjects['Site'].listarNoticiaIndividual();
arrayObjects['Site'].listarNoticia();
</script>

</div>