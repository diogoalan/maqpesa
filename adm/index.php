<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="format-detection" content="telephone=no">
	<title>Painel de Controle</title>
	<link type="image/ico" rel="icon" href="assets/img/favicon.ico">
	<link rel="stylesheet" href="assets/css/normalize.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
</head>
<body>
	
	<div class="fundo-login">		
		<div class="container-form-login">
			
			<div class="logo-login"></div>
			<h3 class="text-center">Painel de Controle</h3>
			
			
			<br>

			<form id="form-login" action="javascript(0)">

				<div class="input-group input-group-lg">
				  	<span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
				  	<input type="text" name="email" id="email" class="form-control required" placeholder="Usuário" aria-describedby="sizing-addon1">
				</div>
				
				
				<br>

				
				<div class="input-group input-group-lg">
				  	<span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span></span>
				  	<input type="password" name="password" id="password" class="form-control required" placeholder="Senha" 
				  	aria-describedby="sizing-addon1">
				</div>

				<br>

				<div id="alerts" class="alert"></div>
				<button type="button" class="btn bg-verde color-branco btn-lg btn-block btn-ok" onClick="login('Login', 'form-login')">Entrar</button>

		</form>
			

		</div>
	</div>


	<script type="text/javascript" src="assets/js/jquery.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="assets/js/Requisicoes.js"></script>
	<script type="text/javascript" src="assets/js/Validacoes.js"></script>
	
	<script type="text/javascript" src="assets/js/Dashboard.js"></script>
	<script type="text/javascript" src="assets/js/Categorias.js"></script>
	<script type="text/javascript" src="assets/js/Login.js"></script>
	
	<script type="text/javascript" src="assets/js/Produtos.js"></script>
	<script type="text/javascript" src="assets/js/Site.js"></script>
	<script type="text/javascript" src="assets/js/Login.js"></script>
	<script type="text/javascript" src="assets/js/Main.js"></script>

</body>
</html>