<?php

error_reporting(E_ERROR);

require '../loadClass.php';

$categorias = new CategoriaDao();
$categorias = $categorias->selectAllCategoria();
$categoriasAux = count($categorias);

$produtos = new ProdutoDao();
$produtos = $produtos->selectAllProdutos();
$produtosAux = count($produtos);

$noticias = new NoticiaDao();
$noticias = $noticias->selectAllNoticias();
$noticiasAux = count($noticias);


$return = array();
$array = array();

$array['categoria'] = $categoriasAux;
$array['produto'] = $produtosAux;
$array['noticias'] = $noticiasAux;


array_push($return, $array);

$json = json_encode($return);
	
echo $json;