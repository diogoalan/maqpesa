function Requisicoes(){


}

Requisicoes.prototype = {


	ajax: function(urlReq, type, dataTypeReq, dados, callback){

		if(dataTypeReq == '' || dataTypeReq == undefined){
			var dataTypeReq = '';
		} else {
			dataTypeReq = 'json';
		}

		$.ajax({
			url: urlReq,
			type: type,
			dataType: dataTypeReq,
			data: dados,
			beforeSend: function(){
				$('#loading').show();
			},
			success: function(data){
				
				$('#loading').hide();	
				$('#alerts').show();

				callback(data);
			}
		})

	}

}