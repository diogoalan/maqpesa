function Site(){
	
	this.id = null;

	this.url = $('#urlSite').val();

}

Site.prototype = {

	listarCategoria: function(){
		
		var dados = {
			'action' : 'selectAllCategoria'
		}

		var callback = function(json){

			var contCategoria = 0;
			
			for(var i in json){
				
				// LISTA CATEGORIAS DESKTOP
				if(contCategoria == 0){
					$('#all-categoria').append('<a href="produtos.php?imak='+json[i].id+'">'+json[i].name+'</a>');
				}
			}
		}

		arrayObjects['Requisicoes'].ajax('adm/mvc/controller/CategoriaController.php', 'post', 'json', dados, callback);
	},

	listarProduto: function(){

		var dados = null;
		// var countProdutosIndex = 0;

		var callback = function(json){
	
			var html          	   = '',
				countProdutosIndex = 0,
				idCategoria   = window.location.href.substr(window.location.href.lastIndexOf("=")+1),
				quantidade = 1;
				htmlTeste = '';

			if(json.length > 0){
			
				for(var i in json){
					console.log(json);
					html = ''+
							'<div id="'+json[i].id+'" class="column margin-produtos">'+
								'<input type="hidden" id="verificaProduto" value="1">'+
								'<div class="border-produtos float-center">'+
									'<a href="produto-interno.php?imak='+json[i].id+'">'+
						   				'<div style="background:url(adm/assets/upload/'+json[i].img+')'+ 
						   				'no-repeat center; background-size: cover;" class="img-produto float-center"></div>'+
									'</a><br>'+
									'<h5 class="color-laranja text-center titillium_bdbold">'+
										'<a class="color-laranja" href="produto-interno.php?imak='+json[i].id+'">'+json[i].name+''+
										'</a></h5>'+
									'<p class="titillium_bdbold font16">'+
										'<a class="veja-mais" href="produto-interno.php?imak='+json[i].id+'">'+
										'<i>VEJA MAIS</i></a>'+
									'</p>'+
								'</div>'+
							'</div>';
						
					// SLAID INDEX  Logica Diogo!
					if(json[i].status == 1){
						if(countProdutosIndex == 0){							
								htmlTeste += '<div class="row small-up-1 medium-up-2 large-up-3 text-center mySlides fade">';
								$('#botoesSlaid').append('<span class="dot" onclick="currentSlide('+quantidade+')"></span>');
								quantidade++;
						}
						

						htmlTeste += html;

						countProdutosIndex++;

						if(countProdutosIndex == 3){
							countProdutosIndex = 0;
								
							htmlTeste += '</div>';
						}						
					}

					if(json[i].status == 1 && !isNaN(parseInt(idCategoria)) && json[i].categoria == idCategoria){
						$('#all-produtos').append(html);
						$('#nomeCategoria2').html(
							'<h1 class="color-verde-claro text-center show-for-large titilliumsemibold font52">PRODUTOS</h1>\n\
							<h1 class="color-verde-claro text-center hide-for-large titilliumsemibold font42">PRODUTOS</h1>\n\
							<h4 class="text-center color-laranja titilliumsemibold">'+json[i].categoriaNome+'</h4>\n\
						');
						$('#navegacaoProduto').html('<h6 class="color-verde-claro titillium_regular font12"><a href="index.php" class="hover-brancons">Página Inicial</a>/ <a href="index.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">Produtos</strong></a>/ <a href="index.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">'+json[i].categoriaNome+'</strong></a> </h6>');
					}

					if(json[i].status == 1 && isNaN(parseInt(idCategoria))){
						$('#all-produtos').append(html);
						$('#nomeCategoria2').html(
							'<h1 class="color-verde-claro text-center show-for-large titilliumsemibold font52">PRODUTOS</h1>\n\
							<h1 class="color-verde-claro text-center hide-for-large titilliumsemibold font42">PRODUTOS</h1>\n\
							<h4 class="text-center color-laranja titilliumsemibold">Todos as categorias</h4>\n\
						');
						$('#navegacaoProduto').html('<h6 class="color-verde-claro titillium_regular font12"><a href="index.php" class="hover-brancons">Página Inicial</a>/ <a href="produtos.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">Produtos</strong></a>/ <a href="produtos.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">Todas as categorias</strong></a> </h6>');
					}

				}

				if($('#verificaProduto').attr('value') != '1'){
					$('#produtosIndex').html('<p class="alert alert-warning alerts" style="text-align:center">Não há nenhum produto cadastrado! :(</p>');
				}

				if($('#verificaProduto').attr('value') != '1'){
					$('#all-produtos').html('<p class="alert alert-warning alerts" style="text-align:center">Não há nenhum produto cadastrado! :(</p>');
				}

				$('#produtosIndex').html(htmlTeste);			
					
					showSlides(1);

					setInterval(function(e){
						slideAtual++;

						var slides = document.getElementsByClassName("mySlides");

						if(slideAtual > slides.length)
							slideAtual = 1;

						currentSlide(slideAtual);
				}, 3000);
			}
		}
		
		$('.loading').hide();
		
		arrayObjects['Requisicoes'].ajax('adm/mvc/controller/ProdutoController.php', 'post', 'json', dados, callback);		

	},

	listaProdutoIndividual: function(){

		var id = window.location.href.substr(window.location.href.lastIndexOf("=")+1);
		var slide = '';
		var thumb = '';

		var dados = {
			'action' : 'returnInfoId',
			'id'     : id
		}

		var dados2 = {
			'action' : 'returnImg',
			'id'     : id
		}

		var callback = function(json){
			
			console.log(json);
			
			var countOpcionais = 0;

			for(var i in json){
				
				$('#navegacaoProduto').html('<h6 class="color-verde-claro titillium_regular font12"><a href="index.php" class="hover-brancons">Página Inicial</a>/ <a href="produtos.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">Produtos</strong></a>/ <a href="produto-interno.php?imak='+json[i].id+'" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">'+json[i].name+'</strong></a> </h6>');
				$('#nome-produto').text(json[i].name);
				$('#descricao').text(json[i].descricao);

				// VIDEO PRODUTO
				if(json[i].video == null){	
					$('#video-produto').html('');
				}else{
					$('#video-produto').html(
						'<div class="column large-12">\n\
							<h4 class="color-verde-claro text-center show-for-large titillium_bdbold">Vídeo deste produto</h4>\n\
							<h3 class="color-verde-claro text-center hide-for-large titillium_bdbold">Vídeo deste produto</h3>\n\
							<br><br>\n\
							<div>\n\
								<div class="video text-center show-for-large">\n\
									<iframe width="515" height="315" src="'+json[i].video+'" frameborder="0" allowfullscreen></iframe>\n\
								</div>\n\
								<div class="video text-center hide-for-large">\n\
									<iframe width="100%" height="315" src="'+json[i].video+'" frameborder="0" allowfullscreen></iframe>\n\
								</div>\n\
							</div>\n\
						</div>\n\
					');
				}

				console.log(json[i].opcionais);

				if(json[i].opcionais == null){

					$('#opcionais').html('');

				}else{
						
					opcionais = json[i].opcionais.split(",");
					
					for(var a = 0; a < opcionais.length; a++){

						$('#opcionais').append(
							'<p class="color-cinza-forte titillium_regular text-center">'+opcionais[a]+'</p>\n\
						');
						// countOpcionais++;
					}	
				}

				if(json[i]['img-especificacoes'] == null){	
					$('#img-especificacoes').html('');
				}else{
					$('#img-especificacoes').html(
						'<div class="large-12 column">\n\
							<h4 class="color-verde-claro text-center show-for-large titillium_bdbold">Especificações Técnicas</h4>\n\
							<h3 class="color-verde-claro text-center hide-for-large titillium_bdbold">Especificações Técnicas</h3>\n\
							<div class="img-especificacoes">\n\
								<img class="float-center" src="adm/assets/upload/'+json[i]['img-especificacoes']+'">\n\
							</div>\n\
						</div>'
					);
				}
			}
			$('.loading').hide();
			$('#conteudo').show();
		}

		arrayObjects['Requisicoes'].ajax('adm/mvc/controller/ProdutoController.php', 'post', 'json', dados, callback);


		var callback2 = function(data){
			
			console.log(data);
			
			for(var i in data){
				slide += '\n\
					<div class="sp-slide">\n\
						<img class="sp-image" src="adm/assets/upload/'+data[i].img+'" />\n\
					</div>';

				
				thumb += '<img class="sp-thumbnail" src="adm/assets/upload/'+data[i].img+'" />';
				
				$('#upload').append('<img src="adm/assets/upload/'+data[i].img+'">');
			}

			$('#banner').html('\n\
				<div class="sp-slides">\n\
					'+slide+'\n\
				</div>\n\
				<div class="sp-thumbnails">\n\
					'+thumb+'\n\
				</div>\n\
			');

			$('#banner').sliderPro({
				width: 950,
				height: 750,
				// orientation: 'orizontal',
				loop: false,
				arrows: true,
				buttons: false,
				thumbnailsPosition: 'bottom',
				thumbnailWidth: 100,
				breakpoints: {
					800: {
						thumbnailsPosition: 'bottom',
						thumbnailWidth: 100,
						thumbnailHeight: 100
					}
				}
			});

			// $('#upload').fotorama();
			$('.loading').hide();
			$('#conteudo').show();
		}

		arrayObjects['Requisicoes'].ajax('adm/mvc/controller/ProdutoController.php', 'post', 'json', dados2, callback2);


	},

	listarNoticia: function(){

		var dados = null;

		var callback = function(json){

			console.log(json);

			var countNoticias = 0,
				data = '',
				html = '',
				idDestaque = false;

			if(json.length > 0){

				for(var i in json){

					var dataPub = json[i].data;
					dataPub = dataPub.split('-');
					dataPub = dataPub[2] + '/' + dataPub[1] + '/' + dataPub[0];

					html = ''+
							'<div id="'+json[i].id+'" class="column margin-noticias">\n\
							<input type="hidden" id="verificaNoticia" value="1">\n\
								<div class="box-shadow-noticias-pagina float-center branco-noticia">\n\
									<div class="img-noticia">\n\
										<a href="noticias-interno.php?imak='+json[i].id+'"><div style="background:url(adm/assets/upload/'+json[i].img+') no-repeat center; background-size: cover;" class="img-noticia float-center"></div></a>\n\
									</div>\n\
									<div class="box-noticia">\n\
										<p class="font12 color-laranja titillium_regular">'+dataPub+'</p>\n\
										<h4 class="titillium_bdbold"><a class="color-verde-claro" href="noticias-interno.php?imak='+json[i].id+'">'+json[i].titulo+'</a></h4>\n\
										<p class="color-cinza-forte titillium_regular font14">'+json[i].subtitulo+'</p>\n\
									</div>\n\
									<br>\n\
									<a href="noticias-interno.php?imak='+json[i].id+'"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>\n\
									<br><br>\n\
								</div>\n\
							</div>\n\
							';


					// NOTICIA DESTAQUE DESKTOP
					if(json[i].status == 1 && $('#noticiaDestaqueDesktop')[0] && countNoticias < 2 ){

						idDestaque = json[i].id;

						$('#noticiaDestaqueDesktop').append(
							'<div id="'+json[i].id+'" class="large-5 columns">\n\
							<input type="hidden" id="verificaNoticiaDestaque" value="1">\n\
								<div class="img-noticia-destaque">\n\
									<a href="noticias-interno.php?imak='+json[i].id+'"><div style="background:url(adm/assets/upload/'+json[i].img+') no-repeat center; background-size: cover;" class="img-noticia-destaque float-center"></div></a>\n\
								</div>\n\
							</div>\n\
							<div class="large-7 columns">\n\
								<div class="box-noticia">\n\
									<h6 class="color-laranja titillium_regular font14"><i>'+dataPub+'</i></h6>\n\
									<h4 class="titillium_bdbold"><a class="color-verde-claro" href="noticias-interno.php?imak='+json[i].id+'">'+json[i].titulo+'</a></h4>\n\
									<p class="color-cinza-forte titillium_regular font14">'+json[i].subtitulo+'</p>\n\
									<br>\n\
									<a class="show-for-large" href="noticias-interno.php?imak='+json[i].id+'"><div class="veja-mais-noticias">VEJA MAIS</div></a>\n\
									<a class="hide-for-large" href="noticias-interno.php?imak='+json[i].id+'"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>\n\
								</div><br><br>\n\
							</div>\n\
						');
						countNoticias++;
					}

					// NOTICIA DESTAQUE MOBILE
					if(json[i].status == 1 && $('#noticiaDestaqueMobile')[0] && countNoticias < 2){

						idDestaque = json[i].id;

						$('#noticiaDestaqueMobile').append(
							'<div id="'+json[i].id+'" class="column">\n\
								<input type="hidden" id="verificaNoticiaDestaque" value="1">\n\
								<a href="noticias-interno.php?imak='+json[i].id+'"><img class="float-center" src="adm/assets/upload/'+json[i].img+'"></a>\n\
								<br>\n\
								<div class="box-noticia float-center">\n\
									<h6 class="color-laranja titillium_regular font14 text-center"><i>'+dataPub+'</i></h6>\n\
									<h1 class="titillium_bdbold text-center"><a class="color-verde-claro" href="noticias-interno.php?imak='+json[i].id+'">'+json[i].titulo+'</a></h1>\n\
									<p class="color-cinza-forte titillium_regular font16">'+json[i].subtitulo+'</p>\n\
									<br>\n\
									<a class="show-for-large" href="noticias-interno.php?imak='+json[i].id+'"><div class="veja-mais-noticias">VEJA MAIS</div></a>\n\
									<a class="hide-for-large" href="noticias-interno.php?imak='+json[i].id+'"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>\n\
									<br><br><br>\n\
								</div>\n\
							</div>\n\
						');
						countNoticias ++;
					}


					// NOTICIA INDEX
					if(json[i].status == 1 && $('#noticiaIndex')[0] && countNoticias < 3){
						$('#noticiaIndex').append(html);
						countNoticias ++;
					}

					// PAG NOTICIA
					if(json[i].status == 1 && $('#all-noticias')[0] && idDestaque != json[i].id){
						// console.log($('#all-noticias')[0]);
						$('#all-noticias').append(html);
					}
				}	
			}

			if($('#verificaNoticia').attr('value') != '1'){
				$('#noticiaIndex').html('<p class="alert alert-warning" style="text-align:center">Não há nenhuma noticia cadastrada! :(</p>');
			}

			if($('#verificaNoticiaDestaque').attr('value') != '1'){
				$('#noticiaDestaqueDesktop').html('<p class="alert alert-warning" style="text-align:center">Não há nenhuma noticia cadastrada! :(</p>');
			}

			$('.loading').hide();
			$('#conteudo').show();

		}

		arrayObjects['Requisicoes'].ajax('adm/mvc/controller/NoticiaController.php', 'post', 'json', dados, callback);
	},

	listarNoticiaIndividual: function(){

		var id = window.location.href.substr(window.location.href.lastIndexOf("=")+1);

		var dados = {
			'action' : 'returnInfoId',
			'id'     : id
		}

		var dados2 = {
			'action' : 'returnImg',
			'id'	 : id
		}
		
		console.log(dados2);
		// console.log(id);

		var callback = function(json){

			// console.log(json);

			if(json.length > 0){

				for(var i in json){

					$('#navegacaoNoticia').html('<h6 class="color-verde-claro titillium_regular font12"><a href="index.php" class="hover-brancons">Página Inicial</a>/ <a href="noticias.php" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">Notícias</strong></a>/ <a href="noticias-interno.php?imak='+json[i].id+'" class="hover-brancons"><strong class="color-verde-escuro titillium_regular font12">'+json[i].titulo+'</strong></a> </h6>');

					$('#noticiaIndividual').html(
						'<br>\n\
						<div id="'+json[i].id+'" class="row">\n\
							<div class="column large-12">\n\
								<h3 class="color-laranja titillium_bdbold show-for-large">'+json[i].titulo+'</h3>\n\
								<h1 class="color-laranja titillium_bdbold hide-for-large">'+json[i].titulo+'</h1>\n\
								<br>\n\
								<h5 class="color-verde-escuro titillium_regular">'+json[i].subtitulo+'</h5>\n\
							</div>\n\
						</div>\n\
						<br>\n\
						<div class="row">\n\
							<div class="column large-12">\n\
								<p class="titillium_regular color-cinza-forte">'+json[i].descricao+'</p>\n\
							</div>\n\
						</div>\n\
						<br><br><br>\n\
					');	

					if(json[i].video == null){						
						$('#videoNoticia').html('');
					}else{
						$('#videoNoticia').html(
							'<br><br><br>\n\
							<div class="row">\n\
								<div class="column large-12">\n\
									<div class="video text-center show-for-large">\n\
										<iframe width="515" height="315" src="'+json[i].video+'" frameborder="0" allowfullscreen></iframe>\n\
									</div>\n\
									<div class="video text-center hide-for-large">\n\
										<iframe width="100%" height="315" src="'+json[i].video+'" frameborder="0" allowfullscreen></iframe>\n\
									</div>\n\
								</div>\n\
							</div>'
						);
					}	
				}

			}
		}

		arrayObjects['Requisicoes'].ajax('adm/mvc/controller/NoticiaController.php', 'post', 'json', dados, callback);

		var callback2 = function(data){
			
			console.log(data);

			
			for(var i in data){	
				
				$('#imagensNoticia').append(
					'<div class="column large-12">\n\
						<div class="">\n\
							<img class="float-center" src="adm/assets/upload/'+data[i].img+'">\n\
						</div>\n\
						<br>\n\
					</div>\n\
				');
			}

			$('.loading').hide();
			$('#conteudo').show();
		}

		arrayObjects['Requisicoes'].ajax('adm/mvc/controller/NoticiaController.php', 'post', 'json', dados2, callback2);

	},

	contato: function(idForm){

		if(!arrayObjects['Validacoes'].camposEmBranco(idForm)){
			return;
		}

		if(!arrayObjects['Validacoes'].email('email')) {
			return
		}

		$('.btn-ok').html('<img style="width: 30px" src="adm/assets/img/loading-branco.svg" />');

		var dados = $('#' + idForm).serialize();

		var callback = function(json){
			$('.btn-ok').html('ENVIAR');
			arrayObjects['Validacoes'].alerts('alert alert-success', 'Mensagem enviada com sucesso! Agradecemos o contato! :)');
			$('#nome').val('');
			$('#telefone').val('');
			$('#email').val('');
			$('#msg').val('');
		}

		arrayObjects['Requisicoes'].ajax('adm/mvc/controller/ContatoController.php', 'post', null, dados, callback);

	}


}