function Categorias(){

	var aux = null;

}

Categorias.prototype = {

	add: function(dados){

		var bool = false,
			nome = $('#nome-categoria').val();

		$('td').each(function(index, el){
			if($(this).attr('id') == 'td-nome-categoria'){
				if($(this).text().toUpperCase() == $('#nome-categoria').val().toUpperCase()){
					arrayObjects['Validacoes'].alerts('alert alert-danger', 'Já existe uma categoria com esse nome. Verifique!');
					bool = true;
				}
			}
		});

		if(bool == true){
			return;
		}

		showHide('bg-add', 'hide');

		var callback = function(json){

			$('#alert-return-table').empty();
			
			arrayObjects['Validacoes'].alerts('alert alert-success', 'Categoria cadastrada com sucesso! :)');

			$('tbody').prepend(''+
			'<tr id="'+json+'">'+
				'<td id="td-nome-categoria" class="">'+nome+'</td>'+
				'<td class=""><a class="btn btn-default" href="#" role="button" onClick="load(\'Categorias\', \'update\', this)">'+
				'<span class="glyphicon glyphicon-edit color-azul-c" aria-hidden="true">'+
				'</span> Editar</a>'+
				'<a class="btn btn-default" href="#" role="button" onClick="showDialog(\'modalRemover\', \'show\', '+json+')">'+
				'<span class="glyphicon glyphicon-trash color-vermelho" aria-hidden="true"></span> Excluir</a></td>'+
			'</tr>'+
			'');

			$('#action').val('add');

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/CategoriaController.php', 'post', null, dados, callback);

	},

	update: function(dados){

		var bool = false;

		$('td').each(function(index, el){
			if($(this).attr('id') == 'td-nome-categoria'){
				if($(this).text().toUpperCase() == $('#nome-categoria').val().toUpperCase()){
					if(aux.toUpperCase() != $(this).text().toUpperCase()){
						arrayObjects['Validacoes'].alerts('alert alert-danger', 'Já existe uma categoria com esse nome. Verifique!');
						bool = true;
					}
				}
			}
		});

		if(bool == true){
			return;
		}

		var callback = function(json){
			
			arrayObjects['Validacoes'].alerts('alert alert-success', 'Categoria alterada com sucesso! :)');

			$('tr').each(function(index, el) {
				if($(this).attr('id') == $('#id').val()){
					$(this).find('#td-nome-categoria').text($('#nome-categoria').val());
				}
			});

			showHide('bg-add', 'hide');

			$('#action').val('add');

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/CategoriaController.php', 'post', null, dados, callback);

	},

	listar: function(){

		var dados = null;

		var callback = function(json){
			
			if(json.length == 0){

				$('#alert-return-table').html('<p class="alert alert-info" style="text-align: center">Não existe nenhuma categoria cadastrada! :(</p>');

			} else {

				for(var i in json){

					if(parseInt(json[i].id) > 0){
						$('tbody').prepend(''+
						'<tr id="'+json[i].id+'">'+
							'<td id="td-nome-categoria" class="">'+json[i].name+'</td>'+
							'<td class=""><a class="btn btn-default" href="#" role="button" onClick="load(\'Categorias\', \'update\', this)">'+
							'<span class="glyphicon glyphicon-edit color-azul-c" aria-hidden="true">'+
							'</span> Editar</a>'+
							'<a class="btn btn-default" href="#" role="button" onClick="showDialog(\'modalRemover\', \'show\', '+json[i].id+')">'+
							'<span class="glyphicon glyphicon-trash color-vermelho" aria-hidden="true"></span> Excluir</a></td>'+
						'</tr>'+
						'');
					} else {
						$('tbody').prepend(''+
						'<tr id="'+json[i].id+'">'+
							'<td id="td-nome-categoria" class="">'+json[i].name+'</td>'+
							'<td class=""><a class="btn btn-default" href="#" role="button">'+
							'<span class="glyphicon glyphicon-edit color-azul-c" aria-hidden="true">'+
							'</span> Editar</a>'+
							'<a class="btn btn-default" href="#" role="button">'+
							'<span class="glyphicon glyphicon-trash color-vermelho" aria-hidden="true"></span> Excluir</a></td>'+
						'</tr>'+
						'');
					}
				}

			}

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/CategoriaController.php', 'post', 'json', dados, callback);

	},

	showUpdate: function(value){

		$('#nome-categoria').val($(value).parent().parent().find('#td-nome-categoria').text());
		$('.btn-ok').attr("onClick", "update('Categorias', 'Categorias')");
		$('#action').val('update');
		$('#id').val($(value).parent().parent().attr('id'));
		aux = $(value).parent().parent().find('#td-nome-categoria').text();

		showHide('bg-add', 'show');

	},

	remove: function(id){

		var dados = {
			'id' 	 : id,
			'action' : 'remove'
		}

		var callback = function(json){

			showDialog('modalRemover', 'hide');
			$('#id').val('');

			if(json == 'true'){
				arrayObjects['Validacoes'].alerts('alert alert-success', 'Categoria removida com sucesso! :)');
				$('#' + id).remove();
			} else {
				arrayObjects['Validacoes'].alerts('alert alert-danger', 'Essa categoria possui vínculos com outros registros. Verifique! :(');
			}

		}

		arrayObjects['Requisicoes'].ajax('mvc/controller/CategoriaController.php', 'post', null, dados, callback);

	}

}