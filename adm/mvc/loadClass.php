<?php

function __autoload($class) {

	if(file_exists($class . '.class.php')){
		require $class . '.class.php';
	} 

	if(file_exists('../model/' . $class . '.class.php')){
		require '../model/' . $class . '.class.php';
	} 


	if(file_exists('../system/' . $class . '.class.php')){
		require '../system/' . $class . '.class.php';
	} 

}