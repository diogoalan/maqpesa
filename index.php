<?php require('topo-index.php'); ?>

<div class="row text-center loading">
   	<img src="assets/img/loading-verde.svg" />
</div>


<div id="conteudo">

<!-- SECTION PRODUTOS -->
<section class="float-center">
	<div class="bg-produtos">	
		<br><br><br>
		<div class="row">
			<div class="column">	
				<h1 class="color-laranja text-center show-for-large titilliumsemibold">LINHA MAQPESA</h1>
				<h1 class="color-laranja text-center hide-for-large titilliumsemibold font42">LINHA MAQPESA</h1>
				<p class="color-cinza-forte text-center titillium_regular">Uma nova marca para sua produtividade crescer </p>
			</div>
		</div>
		
		<br><br><br><br>

			<!-- LISTA PRODUTOS -->
			<div class="slideshow-container">
			  	<div id="produtosIndex">

					<!-- <div class="row small-up-1 medium-up-2 large-up-3 text-center mySlides fade"> -->
	
							<!-- <div class="column margin-produtos">
								<div class="border-produtos float-center">
									<a href="produto-interno.php">			   
									   <img src="assets/img/rapsco1-500x370.jpg" class="img-produto float-center" alt="">
									</a>   
									<br>
									<h5 class="color-laranja text-center titillium_bdbold"><a class="color-laranja" href="produto-interno.php">RT - Raspo Transportador</a></h5>
									<p class="titillium_bdbold font16"><a class="veja-mais" href="produto-interno.php"><i>VEJA MAIS</i></a></p>
								</div>
							</div>

							<div class="column margin-produtos">
								<div class="border-produtos float-center">
									<a href="produto-interno.php">			   
									   <img src="assets/img/rapsco1-500x370.jpg" class="img-produto float-center" alt="">
									</a>   
									<br>
									<h5 class="color-laranja text-center titillium_bdbold"><a class="color-laranja" href="produto-interno.php">RT - Raspo Transportador</a></h5>
									<p class="titillium_bdbold font16"><a class="veja-mais" href="produto-interno.php"><i>VEJA MAIS</i></a></p>
								</div>
							</div>

							<div class="column margin-produtos">
								<div class="border-produtos float-center">
									<a href="produto-interno.php">			   
									   <img src="assets/img/rapsco1-500x370.jpg" class="img-produto float-center" alt="">
									</a>   
									<br>
									<h5 class="color-laranja text-center titillium_bdbold"><a class="color-laranja" href="produto-interno.php">RT - Raspo Transportador</a></h5>
									<p class="titillium_bdbold font16"><a class="veja-mais" href="produto-interno.php"><i>VEJA MAIS</i></a></p>
								</div>
							</div> -->
					
					<!-- </div> -->
				</div>

				<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
				<a class="next" onclick="plusSlides(1)">&#10095;</a>
			</div>

			<br>

			<div id="botoesSlaid" style="text-align:center">
<!-- 			  <span class="dot" onclick="currentSlide(1)"></span> 
			  <span class="dot" onclick="currentSlide(2)"></span> 
			  <span class="dot" onclick="currentSlide(3)"></span> --> 
			</div>


<!-- 				<div class="column margin-produtos">
					<div class="border-produtos float-center">
						<a href="produto-interno.php">			   
						   <img src="assets/img/rapsco1-500x370.jpg" class="img-produto float-center" alt="">
						</a>   
						   <br>

						   <h5 class="color-laranja text-center titillium_bdbold"><a class="color-laranja" href="produto-interno.php">RT - Raspo Transportador</a></h5>
						
					
						<p class="titillium_bdbold font16"><a class="veja-mais" href="produto-interno.php"><i>VEJA MAIS</i></a></p>

					</div>
				</div>

				<div class="column margin-produtos">
					<div class="border-produtos float-center">
						<a href="produto-interno.php">			   
						   <img src="assets/img/rapsco1-500x370.jpg" class="img-produto float-center" alt="">
						</a>   
						   <br>

						   <h5 class="color-laranja text-center titillium_bdbold"><a class="color-laranja" href="produto-interno.php">RT - Raspo Transportador</a></h5>
						
					
						<p class="titillium_bdbold font16"><a class="veja-mais" href="produto-interno.php"><i>VEJA MAIS</i></a></p>

					</div>
				</div>

				<div class="column margin-produtos">
					<div class="border-produtos float-center">
						<a href="produto-interno.php">			   
						   <img src="assets/img/rapsco1-500x370.jpg" class="img-produto float-center" alt="">
						</a>   
						   <br>

						   <h5 class="color-laranja text-center titillium_bdbold"><a class="color-laranja" href="produto-interno.php">RT - Raspo Transportador</a></h5>
						
					
						<p class="titillium_bdbold font16"><a class="veja-mais" href="produto-interno.php"><i>VEJA MAIS</i></a></p>

					</div>
				</div> -->

			<br><br><br>
	</div>
</section>



<!-- SECTION BANNER-2 -->
<section class="banner2">
	
	<div class="container-full img-banner2 show-for-large">
		<br><br><br><br><br><br>
		<div class="row">
			<h2 class="color-laranja text-center show-for-large titillium_bdbold">CONHEÇA NOSSOS PRODUTOS DA <br>LINHA DE MÁQUINAS RODOVIÁRIAS</h2>

			<h2 class="color-laranja text-center hide-for-large titillium_bdbold">CONHEÇA NOSSOS PRODUTOS DA <br> LINHA DE MÁQUINAS RODOVIÁRIAS</h2>
			<p class="color-branco text-center titillium_regular">Todas as condições para financiamento para projetos da sua cidade. <br>
			Navegue pelos nossos produtos clicando a baixo
			</p>

			<br><br>

			<a href="produtos.php?imak=1"><div class="btn-veja-mais float-center titillium_bdbold">VEJA MAIS</div></a>

		</div>
		<br><Br><br><br><Br><br>
	</div>

	<div class="container-full img-banner2-responsivo hide-for-large">
		<br><br><br><br><br><br><br>
			<div class="row">
				<!--<h2 class="color-laranja text-center show-for-large titillium_bdbold">CONHEÇA NOSSOS PRODUTOS DA <br>LINHA DE MÁQUINAS RODOVIÁRIAS</h2>-->

				<h2 class="color-laranja text-center hide-for-large titillium_bdbold">CONHEÇA NOSSOS PRODUTOS DA <br> LINHA DE MÁQUINAS RODOVIÁRIAS</h2>
				<p class="color-branco text-center titillium_regular hide-for-large">Todas as condições para financiamento para projetos da sua cidade.<br>
				Navegue pelos nossos produtos clicando a baixo
				</p>

				<br><br>

				<a href="produtos.php"><div class="btn-veja-mais float-center titillium_bdbold">VEJA MAIS</div></a>
			</div>
		<br><Br><br><br><Br><br>
	</div>

</section>





<!-- SECTION ULTIMAS NOTICIAS -->
<section class="ultimas-noticias">
	
	<div class="container-full bg-laranja">
		<br><Br>
		
		<div class="row">
			<h2 class="color-branco text-center show-for-large titillium_bdbold">ÚLTIMAS NOTÍCIAS</h2>
			<h1 class="color-branco text-center hide-for-large titillium_bdbold">ÚLTIMAS NOTÍCIAS</h1>
			<p class="text-center titillium_regular color-branco">Navegue também pelas nossas notícias, e fique por dentro das novidades </p>
		</div>

		<br><br>

		<!-- LISTA NOTICIAS -->
		<div id="noticiaIndex" class="row small-up-1 medium-up-2 large-up-3">
			
<!-- 			<div class="column margin-noticias">
				<div class="box-shadow-noticias-pagina float-center">
					<div class="img-noticia">
						<a href="noticias-interno.php"><img src="assets/img/img-noticias-pagina-inicial.png" class="float-center" alt=""></a>
					</div>

					<div class="box-noticia">	
						<p class="font12 color-laranja titillium_regular">20/12/1994</p>

						<h6 class="titillium_bdbold"><a class="color-verde-claro" href="#"> Navegue também pelas nossas notícias, e fique por</a></h6>

						<p class="color-cinza-forte titillium_regular font12">Navegue também pelas nossas notícias, e fique por dentro das novidadesNavegue também pelas nossas notícias notícias</p>

						<br>

						<a href="noticias-interno.php"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>

						<br>

					</div>
				</div>
			</div>

			<div class="column margin-noticias">
				<div class="box-shadow-noticias-pagina float-center">
					<div class="img-noticia">
						<a href="noticias-interno.php"><img src="assets/img/img-noticias-pagina-inicial.png" class="float-center" alt=""></a>
					</div>

					<div class="box-noticia">	
						<p class="font12 color-laranja titillium_regular">20/12/1994</p>

						<h6 class="titillium_bdbold"><a class="color-verde-claro" href="#"> Navegue também pelas nossas notícias, e fique por</a></h6>

						<p class="color-cinza-forte titillium_regular font12">Navegue também pelas nossas notícias, e fique por dentro das novidadesNavegue também pelas nossas notícias notícias</p>

						<br>

						<a href="noticias-interno.php"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>

						<br>

					</div>
				</div>
			</div>

			<div class="column margin-noticias">
				<div class="box-shadow-noticias-pagina float-center">
					<div class="img-noticia">
						<a href="noticias-interno.php"><img src="assets/img/img-noticias-pagina-inicial.png" class="float-center" alt=""></a>
					</div>

					<div class="box-noticia">	
						<p class="font12 color-laranja titillium_regular">20/12/1994</p>

						<h6 class="titillium_bdbold"><a class="color-verde-claro" href="#"> Navegue também pelas nossas notícias, e fique por</a></h6>

						<p class="color-cinza-forte titillium_regular font12">Navegue também pelas nossas notícias, e fique por dentro das novidadesNavegue também pelas nossas notícias notícias</p>

						<br>

						<a href="noticias-interno.php"><div class="veja-mais-noticias float-center">VEJA MAIS</div></a>

						<br>

					</div>
				</div>
			</div> -->
			
		</div>

		<br><br><br>
	</div>
</section>


<?php require('rodape.php'); ?>


</div>