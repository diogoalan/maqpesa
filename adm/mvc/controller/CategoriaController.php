<?php

error_reporting(E_ERROR);

require '../loadClass.php';

function add(){

	$categoriaModel = new Categorias();
	$categoriaModel->setNome($_POST['nome-categoria']);

	$categoria = new CategoriaDao();
	$categoria->insertCategoria($categoriaModel);
	$aux = $categoria->lastId();

	foreach($aux as $c){
		$aux = $c['categoria_id'];
	}

	echo $aux;
}

function update(){

	$categoriaModel = new Categorias();
	$categoriaModel->setNome($_POST['nome-categoria']);
	$categoriaModel->setId($_POST['id']);

	$categoria = new CategoriaDao();
	$categoria->updateCategoria($categoriaModel);

	echo 'true';

}

function remove(){

	$categoriaModel = new Categorias();
	$categoriaModel->setId($_POST['id']);

	$categoria = new CategoriaDao();
	// $aux = count($categoria->verificaExistenciaSubcategoria($categoriaModel));
	$aux = $aux + count($categoria->verificaExistenciaProdutos($categoriaModel));

	if($aux == 0){
		$aux = $categoria->removeCategoria($categoriaModel);
		echo 'true';
	} else {
		echo 'false';
	}

}

function listar(){

	$categorias = new CategoriaDao();
	$categorias = $categorias->selectAllCategoria();

	$return = array();
	$array = array();

	foreach ($categorias as $c) {
		$array['id'] = $c['categoria_id'];
		$array['name'] = $c['categoria_nome'];
		array_push($return, $array);
	}

	$json = json_encode($return);
	
	echo $json;

}

switch ($_POST['action']) {
	case 'add':
		add();
	break;

	case 'update':
		update();
	break;

	case 'remove':
		remove();
	break;

	default:
		listar();
	break;
}